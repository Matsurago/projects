// Algorithm to form a magic square:
// Put 1 in the middle of row 0
// Put next number in a cell with offset (-1, +1)
// - if outside borders, wrap around
// - if already placed in that cell, put in (0, +1) instead
#include <stdio.h>

void clear_matrix(int n, int[n][n]);  // C99: VLA
void print_matrix(int n, int[n][n]);

void magic_square(int n, int[n][n]);


int main(void) {
    int n;
    printf("magic square size (odd number): ");
    scanf("%d", &n);

    int s[n][n];
    clear_matrix(n, s);
    magic_square(n, s);
    print_matrix(n, s);

    return 0;
}

void magic_square(int n, int a[n][n]) {
    int i = 0, j = n / 2;  // first cell to fill
    a[i][j] = 1;
    
    int k = 2;  // numbers to fill
    int next_i, next_j;
    
    while (k <= n * n) {
        next_i = (i - 1 < 0) ? (n - 1) : (i - 1);
        next_j = (j + 1) % n;
      
        if (a[next_i][next_j] != 0) {
            next_i = (i + 1) % n;
            next_j = j;
        }
        
        i = next_i;
        j = next_j;
      
        a[i][j] = k++;
    }
}

void clear_matrix(int n, int a[n][n]) {
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j) 
            a[i][j] = 0;
}

void print_matrix(int n, int a[n][n]) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            printf("%3d ", a[i][j]);
        }
        printf("\n");
    }
}

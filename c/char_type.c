#include <stdio.h>
#include <ctype.h>

#define TEST(f, c) printf("  %c  ", f((*c)) ? 'x' : ' ')


int main(void) {
    printf("     alnum     cntrl     graph     print     space     xdigit\n");
    printf("          alpha     digit     lower     punct     upper      \n");

    for (char* p = "azAZ0 !\t"; *p != '\0'; ++p) {
        if (iscntrl(*p)) {
            printf("\\x%02x:", *p);
        } else {
            printf("   %c:", *p);
        }

        TEST(isalnum, p);
        TEST(isalpha, p);
        TEST(iscntrl, p);
        TEST(isdigit, p);
        TEST(isgraph, p);
        TEST(islower, p);
        TEST(isprint, p);
        TEST(ispunct, p);
        TEST(isspace, p);
        TEST(isupper, p);
        TEST(isxdigit, p);

        printf("\n");
    }

    return 0;
}
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define NUM_RANKS 13
#define NUM_SUITS 4
#define HAND_SIZE 5


void read_cards(int num_in_rank[], int num_in_suit[]);

void analyze_hand(int num_in_rank[], int num_in_suit[], 
    bool* straight, bool* flush, bool* four, bool* three,
    bool* royal_flush, int* pairs);

void print_result(bool straight, bool flush, bool four, bool three, 
    bool royal_flush, int pairs);


int main(void) {
    int num_in_rank[NUM_RANKS];
    int num_in_suit[NUM_SUITS];
    
    bool straight = false;
    bool flush = false;
    bool four = false;
    bool three = false;
    bool royal_flush = false;
    int pairs = 0;
    
    read_cards(num_in_rank, num_in_suit);
    
    analyze_hand(num_in_rank, num_in_suit, 
        &straight, &flush, &four, &three, &royal_flush, &pairs);
    
    print_result(straight, flush, four, three, royal_flush, pairs);
}

void analyze_hand(int num_in_rank[], int num_in_suit[],
    bool* straight, bool* flush, bool* four, bool* three,
    bool* royal_flush, int* pairs) {
            
    // is flush?
    for (int i = 0; i < NUM_SUITS; ++i) 
        if (num_in_suit[i] == HAND_SIZE)
            *flush = true;
    
    // is royal flush?
    if (flush) {
        int n = 0;
        for (int i = NUM_RANKS - HAND_SIZE; i < NUM_RANKS; ++i)
            if (num_in_rank[i] == 1)
                ++n;
        
        if (n == HAND_SIZE) {
            *royal_flush = true;
            return;
        }
    }
    
    // is straight?
    int num_consec = 0;
    int i = 0;
    while (num_in_rank[i] == 0)
        ++i;  // first card in hand with the lowest rank
        
    for (; num_in_rank[i] > 0 && i < NUM_RANKS; ++i) 
        ++num_consec;
        
    if (num_consec == HAND_SIZE) {
        *straight = true;
        return;
    }
    
    // is 4-of-a-kind, 3-of-a-kind, or pair?
    for (int i = 0; i < NUM_RANKS; ++i) {
        if (num_in_rank[i] == 4) *four = true;
        if (num_in_rank[i] == 3) *three = true;
        if (num_in_rank[i] == 2) (*pairs)++;
    }
}

void read_cards(int num_in_rank[], int num_in_suit[]) {
    bool card_exists[NUM_RANKS][NUM_SUITS] = {false};
    
    for (int i = 0; i < NUM_RANKS; ++i)
        num_in_rank[i] = 0;
    
    for (int j = 0; j < NUM_SUITS; ++j)
        num_in_suit[j] = 0;
    
    int cards_read = 0;
    while (cards_read < HAND_SIZE) {
        int rank, suit;
        bool bad_card = false;
        
        char rank_ch = getchar();
        switch (rank_ch) {
            case '2': rank = 0; break;
            case '3': rank = 1; break;
            case '4': rank = 2; break;
            case '5': rank = 3; break;
            case '6': rank = 4; break;
            case '7': rank = 5; break;
            case '8': rank = 6; break;
            case '9': rank = 7; break;
            case 't': case 'T': rank = 8; break;
            case 'j': case 'J': rank = 9; break;
            case 'q': case 'Q': rank = 10; break;
            case 'k': case 'K': rank = 11; break;
            case 'a': case 'A': rank = 12; break;
            default:  bad_card = true;
        }
        
        char suit_ch = getchar();
        switch (suit_ch) {
            case 'c': case 'C': suit = 0; break;
            case 'd': case 'D': suit = 1; break;
            case 'h': case 'H': suit = 2; break;
            case 's': case 'S': suit = 3; break;
            default:  bad_card = true;
        }
        
        char ch;
        while ((ch = getchar()) != '\n')
            if (ch != ' ')
                bad_card = true;
                
        if (bad_card) {
            printf("Bad card; ignored.\n");
        } else if (card_exists[rank][suit]) {
            printf("Duplicate card; ignored.\n");
        } else {
            ++num_in_rank[rank];
            ++num_in_suit[suit];
            card_exists[rank][suit] = true;
            ++cards_read;
        }
    }
}

void print_result(bool straight, bool flush, bool four, bool three, 
    bool royal_flush, int pairs) {
    
    if (royal_flush)
        printf("Royal flush");
    else if (straight && flush)
        printf("Straight flush");
    else if (four)
        printf("Four of a kind");
    else if (three && pairs == 1)
        printf("Full house");
    else if (flush)
        printf("Flush");
    else if (straight)
        printf("Straight");
    else if (pairs == 2)
        printf("Two pairs");
    else if (pairs == 1)
        printf("Pair");
    else 
        printf("High card");
    
    printf("\n");
}

#include <stdio.h>
#include <stddef.h>

struct my_s {
    char c;
    short h;
    long long l;
};

int main(void) {
    printf("offset of c: %llu\n", offsetof(struct my_s, c));
    printf("offset of h: %llu\n", offsetof(struct my_s, h));
    printf("offset of l: %llu\n", offsetof(struct my_s, l));

    return 0;
}

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(void) {
    int n;
    printf("number of items to generate: ");
    scanf("%d", &n);

    // C99: variable-length arrays
    int a[n];

    srand((unsigned int) time(NULL));

    for (int i = 0; i < n; ++i) {
        a[i] = rand() % 100;
    }

    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");

}



#include <stdio.h>
#include <stdbool.h>

bool is_eps(double x, double e) {
    return x < e && x > -e;
}

double newton_sqrt(double x) {
    double a = x / 2;

    while (! is_eps(x - a * a, 1e-5))
        a = (a + x / a) / 2;

    return a;
}

int main(void) {
    double x;
    printf("input number: ");
    scanf("%lf", &x);

    double res = newton_sqrt(x);
    printf("sqrt(%g) = %g\n", x, res);
}


// Allows to see memory locations this problem has access to.
#include <stdio.h>
#include <ctype.h>

typedef unsigned int addr_t;
typedef unsigned char byte;

int main(void) {
    addr_t addr;
    int n;

    // give clue to a user which addresses are accessible
    printf("Address of main function: %x\n", (addr_t) main);
    printf("Address of addr variable: %x\n", (addr_t) &addr);

    printf("\nEnter an address (hex): ");
    scanf("%x", &addr);
    printf("Enter number of bytes to view: ");
    scanf("%d", &n);

    printf("\n");
    printf(" Address              Bytes              Characters\n"); 
    printf(" -------  -----------------------------  ----------\n");

    byte* ptr = (byte*) addr;

    for (; n > 0; n-= 10, ptr += 10) {
        printf("%8X  ", (addr_t) ptr);

        int i = 0;
        for (; i < 10 && i < n; ++i) {
            printf("%.2X ", *(ptr + i));
        }

        for (; i < 10; ++i) {
            printf("   ");
        }
        printf(" ");

        for (i = 0; i < 10 && i < n; ++i) {
            byte c = *(ptr + i);
            if (!isprint(c)) {
                c = '.';
            }
            printf("%c", c);
        }

        printf("\n");
    }

    return 0;
}

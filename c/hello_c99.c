#include <stdio.h>
// C99: bool type
#include <stdbool.h>

typedef struct person_t {  // struct tag
    char name[16];
    int age;
} person_t;  // typedef: can use person_t without 'struct' keyword

void print_person(const person_t* p) {
    printf("%s (%d years old)\n", p->name, p->age);
}

// C99: variable array size hints
void print_array(int n, int a[n]) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

// C99: one-line comments
int main(void) {
    bool val = true;  // C99
    if (val)
        printf("hello C.\n");

    // C99: can declare variables anywhere in a function
    // before: only at the beginning, before any statement
    int day = 1, month = 3, year = 2020;
    // use %.2d to add a leading zero
    printf("date: %.2d/%.2d/%d\n", day, month, year);

    // C99: long long type
    unsigned long long int v = -1;
    printf("max long: %llu\n", v);

    // C99: designated array initializers
    // length is deduced from the largest index
    int a[] = {[5] = 7, [9] = 3};  // others will be zero
    print_array(10, a);

    // C99: designated struct field initializers
    person_t p1 = {.age = 33, .name = "Vasiliy"};
    print_person(&p1);

    // C99: struct literals
    p1 = (person_t) {.name = "Rose", .age = 27};
    print_person(&p1);

#ifdef DEBUG
    puts("(debug) program exit.");
#endif
    return 0;
}


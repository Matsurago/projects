// https://dmoj.ca/problem/cco07p2
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define RAYS 6

bool are_same(int x[], int y[]);
int hash(int x[], int m);


// snowflake hash
int hash(int x[], int m) {
    int sum = 0;
    for (int i = 0; i < RAYS; ++i)
        sum += x[i];
    
    return sum % m;
}

// same snowflakes?
bool are_same(int x[], int y[]) {
    // rotate left
    for (int offset = 0; offset < RAYS; ++offset) {
		bool same = true;
		
        for (int j = offset, k = 0; k < RAYS; ++j, ++k) {
            if (x[k] != y[j % RAYS]) {
				same = false;
				break;
			}
        }
        
        if (same) return true;
    }
    
    // rotate right
    for (int offset = 0; offset < RAYS; ++offset) {
		bool same = true;
		
        for (int j = offset, k = 0; k < RAYS; ++j, ++k) {
            if (x[k] != y[(2 * RAYS - j) % RAYS]) {
				same = false;
				break;
			}
        }
        
        if (same) return true;
    }
    
    return false;
}

// hashmap bucket linked list node
typedef struct snowflake_node {
    int snowflake[RAYS];
    struct snowflake_node* next;
} snowflake_node;


int main(void) {
    int n;
    scanf("%d", &n);
    
    // make a hashmap with N buckets
    snowflake_node* snowflakes[n];

    // zero init
    for (int i = 0; i < n; ++i) {
        snowflakes[i] = NULL;
    }
    
    // read snowflakes
    for (int i = 0; i < n; ++i) {
        snowflake_node* node = malloc(sizeof(snowflake_node));
        if (node == NULL) {
            fprintf(stderr, "cannot allocate memory\n");
            exit(EXIT_FAILURE);
        }

		for (int j = 0; j < RAYS; ++j) {
			scanf("%d", &node->snowflake[j]);
        }

        // add to hashmap (to front of bucket list)
        int h = hash(node->snowflake, n);

        // !!
        node->next = snowflakes[h];
        snowflakes[h] = node;
    }

    // find duplicates
    for (int i = 0; i < n; ++i) {
        for (snowflake_node* p = snowflakes[i]; p; p = p->next) {
            for (snowflake_node* q = p->next; q; q = q->next) {
                if (are_same(p->snowflake, q->snowflake)) {
				    printf("Twin snowflakes found.\n");
				    return 0;
			    }
            }
        }
    }

    // free
    for (int i = 0; i < n; ++i) {
        snowflake_node* p = snowflakes[i];
        while (p) {
            snowflake_node* next = p->next;
            free(p);
            p = next;
        }
    }

    printf("No two snowflakes are alike.\n");
    return 0;
}

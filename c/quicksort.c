#include <stdio.h>

#define N 12

void quicksort(int n, int[n]);
void quicksort_step(int[], int, int);


int main(void) {    
    int a[N] = {7, 8, 3, 2, 1, 4, 12, 5, 11, 6, 10, 9};
    quicksort(N, a);
    
    for (int i = 0; i < N; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void quicksort(int n, int a[n]) {
    quicksort_step(a, 0, n - 1);
}

// Quicksort algorithm
void quicksort_step(int a[], int low, int high) {
    if (high <= low) return;
    
    int old_low = low, old_high = high;
    
    int pivot = a[low];
    int hole = low;
    
    while (low < high) {
        while (high > low && a[high] >= pivot)
            --high;
        
        if (high == low) break;
            
        a[hole] = a[high];  // first item on the right side less than pivot
        ++low;
        hole = high;
        
        while (low < high && a[low] <= pivot)
            ++low;
            
        if (high == low) break;
        
        a[hole] = a[low];  // first item on the left side greater than pivot
        --high;
        hole = low;
    }
    
    a[low] = pivot;  // here, low == high
    quicksort_step(a, old_low, low - 1);
    quicksort_step(a, low + 1, old_high);
}

#include <stdio.h>
#include <math.h>

void tabulate(double (*f)(double), double start, double end,
              double increment);

int main(void) {
    double start, end, increment;

    printf("Interval start: ");
    scanf("%lf", &start);
    printf("Interval end: ");
    scanf("%lf", &end);
    printf("Increment: ");
    scanf("%lf", &increment);
    
    printf("\n      x        cos(x)"
           "\n   -------    -------\n");
    tabulate(cos, start, end, increment);
    
    printf("\n      x        sin(x)"
           "\n   -------    -------\n");
    tabulate(sin, start, end, increment);
    
    printf("\n      x        tan(x)"
           "\n   -------    -------\n");
    tabulate(tan, start, end, increment);
    
    return 0;    
}

// accepts a pointer to a function
void tabulate(double (*f)(double), double start, double end,
              double increment) {
    int n = ceil((end - start) / increment);
    for (int i = 0; i <= n; ++i) {
        double x = start + i * increment;
        printf("%10.5f %10.5f\n", x, f(x));
    } 
}


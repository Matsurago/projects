// compute consequent squares
// without using multiplication
#include <stdio.h>

int main(void) {
    int N = 10;

    for (int i = 1, square = 1, odd = 3; i <= N; i += 1, odd += 2) {
        printf("%d\t%d\n", i, square);
        square += odd;
    }

    return 0;
}


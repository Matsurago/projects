; hello.asm
section .data
	msg  	db  	"hello, world",10 	; 10 = line feed
	.len 	equ 	$ - msg

section .bss

section .text
	global main
main:
	mov rax, 0x2000004  	; = write
	mov rdi, 1  		; 1 = to stdout
	mov rsi, msg  		; string to display
	mov rdx, msg.len 	; length of the string without \0
	syscall
	mov rax, 0x2000001 	; = exit
	mov rdi, 0 		; 0 = success exit code
	syscall

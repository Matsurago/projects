with Ada.Text_IO;

package body Simple_IO is
   procedure Get(F: out Float) is
      use Ada.Text_IO;
   begin
      F := Float'Value (Get_Line);
   end Get;

   procedure Put(F: in Float) is
   begin
      Ada.Text_IO.Put (Float'Image (F));
   end Put;
   
   procedure Put(S: in String) is
   begin
      Ada.Text_IO.Put (S);
   end Put;
   
   procedure New_Line(N: in Integer := 1) is
      use Ada.Text_IO;
   begin
      Put_Line ("");
   end New_Line;
end Simple_IO;


   

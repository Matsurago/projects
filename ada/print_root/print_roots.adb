with Simple_IO, Simple_Maths;

procedure Print_Roots is
   use Simple_IO;
   use Simple_Maths;
   X: Float;
begin
   Put("Roots of various numbers");
   New_Line(2);
   
   loop
      Get(X);
      exit when X = 0.0;
      
      Put("Root of "); Put(X); Put(" is ");
      if X < 0.0 then
	 Put("not calculable");
      else 
	 Put(Sqrt(X));
      end if;
	
      New_Line;
   end loop;
   
   New_Line;
   Put("Program finished");
   New_Line;
   
end Print_Roots;


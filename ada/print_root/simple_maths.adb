package body Simple_Maths is 
   function Sqrt(F: Float) return Float is
      X, R, EPS: Float;
   begin
      X := F;
      loop
	 R := (X * X - F) / (2.0 * X);
	 EPS := Fabs (F - R * R);
	 exit when EPS < 0.1;
	 X := R;
      end loop;
      return R;
   end Sqrt;
   
   function Fabs(F: Float) return Float is
      R: Float;
   begin
      if F < 0.0 then
	 R := -1.0 * F;
      else
	 R := F;
      end if;
      return R;
   end Fabs;
	 
end Simple_Maths;


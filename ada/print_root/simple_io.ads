package Simple_IO is
   procedure Get(F: out Float);
   procedure Put(F: in Float);
   procedure Put(S: in String);
   procedure New_Line(N: in Integer := 1);
end Simple_IO;
   

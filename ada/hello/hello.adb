with Ada.Text_IO;

procedure Hello is
   use Ada.Text_IO;
   A, B, C: Integer;
begin
   A := Integer'Value (Get_Line);
   B := Integer'Value (Get_Line);
   C := A + B;
   
   if C = 0 then
      Put_Line ("RESULT IS 0");
   elsif C > 0 then
      Put_Line ("POSITIVE RESULT :" & Integer'Image (C));
   else
      Put_Line ("NEGATIVE RESULT :" & Integer'Image (C));
   end if;
end Hello;

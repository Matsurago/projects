package learning.server;

import static learning.Utils.print;

public class RunServer {
    public static void main(String[] args) {
        if (args.length != 1) {
            print("Usage: java learning.server.RunServer <port>");
            return;
        }

        int port;
        try {
            port = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            print("Incorrect port number: %s", args[0]);
            return;
        }

        Server server = new Server(port);
        server.start();

        print("\nExiting.");
    }
}

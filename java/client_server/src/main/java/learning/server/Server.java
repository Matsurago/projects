package learning.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import static learning.Utils.error;
import static learning.Utils.print;

class Server {
    private final int port;

    Server(int port) {
        this.port = port;
    }

    void start() {
        print("Starting server at port %d...", port);

        try (ServerSocket serverSocket = new ServerSocket(port)) {
            print("Server started successfully. Awaiting for clients to connect.");

            //noinspection InfiniteLoopStatement
            while (true) {
                try (Socket clientSocket = serverSocket.accept()) {
                    print("Client connected (%s:%d).", clientSocket.getInetAddress().getHostAddress(), clientSocket.getPort());
                    serveClient(clientSocket);
                    print("Client disconnected.");
                } catch (IOException e) {
                    error(e,"Client disconnected");
                }
            }
        } catch (Exception e) {
            error(e, "Unable to start the server.");
        }
    }

    private void serveClient(Socket clientSocket) {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)) {

            String request, response;
            while ((request = in.readLine()) != null) {
                response = processClientRequest(request);
                out.println(response);
            }

        } catch (IOException e) {
            error(e, "Encountered a problem while communicating with a client");
        }
    }

    private String processClientRequest(String clientInput) {
        print("Client sent text: '%s'", clientInput);
        return String.format("Hello, %s. How are you?", clientInput);
    }
}

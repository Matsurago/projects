package learning;

public final class Utils {
    private Utils() {}

    public static void print(String fmt, Object ... args) {
        if (args.length == 0)
            System.out.println(fmt);
        else
            System.out.printf(fmt + "%n", args);
    }

    public static void error(Throwable e, String message, Object ... args) {
        print("[ERROR] " + message + " (" + e.getMessage() + ")", args);
    }

    public static void text(String message) {
        System.out.print(message);
    }
}

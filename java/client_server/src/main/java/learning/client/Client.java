package learning.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static learning.Utils.*;

class Client {
    private final String host;
    private final int port;

    Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    void connect() {
        print("Connecting to %s:%d...", host, port);

        try (Socket socket = new Socket(host, port)) {
            print("Connected.");

            try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                 BufferedReader consoleInput = new BufferedReader(new InputStreamReader(System.in))) {

                text("Your name: ");
                String userInput;
                while ((userInput = consoleInput.readLine()) != null) {
                    print("Sending data...");
                    out.println(userInput);

                    String response = in.readLine();
                    print(response);

                    text("Your name: ");
                }
            }
        } catch (Exception e) {
            error(e, "Unable to connect to %s:%d", host, port);
        }
    }
}

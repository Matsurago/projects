package learning.client;

import static learning.Utils.print;

public class RunClient {
    public static void main(String[] args) {
        if (args.length != 2) {
            print("Usage: java learning.client.RunClient <host> <port>");
            return;
        }

        String host = args[0];
        int port;
        try {
            port = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            print("Incorrect port number: %s", args[1]);
            return;
        }

        Client client = new Client(host, port);
        client.connect();

        print("\nExiting.");
    }
}

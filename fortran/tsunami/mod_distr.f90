module mod_distr
  use iso_fortran_env, only: int32, real32
  implicit none
contains
  pure subroutine set_gaussian(x, i_center, decay)
    real(real32), intent(in out) :: x(:)
    real(real32), intent(in) :: i_center
    real(real32), intent(in) :: decay

    integer(int32) :: i
    
    do concurrent (i = 1:size(x))
      x(i) = exp(-decay * (i - i_center) ** 2)
    end do

  end subroutine set_gaussian  
end module mod_distr

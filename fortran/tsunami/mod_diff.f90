module mod_diff
  use iso_fortran_env, only: int32, real32
  implicit none
contains
  ! simple finite difference that can be used for forward movement
  pure function diff_upwind(x) result(dx)
    real(real32), intent(in) :: x(:)
    real(real32) :: dx(size(x))
    integer(int32) :: n

    n = size(x)
    
    dx(1) = x(1) - x(n)  ! border condition: loops over the border
    dx(2:n) = x(2:n) - x(1:n-1)  ! dx(i) = x(i) - x(i-1)
  end function diff_upwind
  
  ! centered finite difference for realistic equations
  pure function diff_centered(x) result(dx)
    real(real32), intent(in) :: x(:)
    real(real32) :: dx(size(x))
    integer(int32) :: n

    n = size(x)
    
    dx(1) = 0.5 * (x(2) - x(n))  ! border conditions
    dx(n) = 0.5 * (x(1) - x(n-1))
    dx(2:n-1) = 0.5 * (x(3:n) - x(1:n-2))  ! dx(i) = (x(i+1) - x(i-1)) / 2
  end function diff_centered

end module mod_diff

program tsunami
  use mod_diff, only: diff => diff_centered
  use mod_distr
  implicit none
  
  integer(int32) :: t
  integer, parameter :: x_max = 100  ! grid size
  integer, parameter :: t_max = 5000  ! time steps

  real, parameter :: dt = 0.02  ! time step, in seconds (s)
  real, parameter :: dx = 1.  ! grid step, in meters (m)
  real, parameter :: g = 9.8  ! gravity, m/s^2
  real, parameter :: h_mean = 10  ! mean unperturbed water height, m

  ! static array of real numbers of length x_max
  real :: h(x_max)  ! water height in each point of grid
  real :: u(x_max)  ! water velocity in each point of grid

  if (x_max <= 0) stop 'grid size must be positive'
  if (dt <= 0) stop 'time step must be positive'
  if (dx <= 0) stop 'grid spacing must be positive'

  ! initial water height distribution (Gaussian)
  call set_gaussian(h, 25.0, 0.02)  ! center = 25, decay = 0.02
  u = 0
  print *, 0, h
  
  ! main simulation
  do t = 1, t_max
     ! main equation (next state)
     ! operates on the whole array at once
     u = u - (u * diff(u) + g * diff(h)) / dx * dt
     h = h - diff(u * (h_mean + h)) / dx * dt

     print *, t, h
  end do

end program tsunami

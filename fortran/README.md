# Fortran
The current version is 2018 (ISO standard).
The most common free compiler is `gfortran`, which is a part of `gcc`.
Parallel computations in modern fortran are done with coarrays,
which relies on `openmpi` for underlying implementation.
In other words, both `opencoarrays` and `openmpi` need to be installed.

Commercial compilers from Intel and Cray support coarrays out-of-the-box.

MPI stands for Message Passing Interface, a low-level specification
for passing of data between arbitrary remote processes. It can be used
for distributed memory systems (CPUs communicating over a network), unlike
OpenMP, which is a specification for shared memory systems only (such as
multicore CPUs on a single computer).

However, both OpenMP and MPI are still very low-level, and using coarrays 
is a preferred way to write parallel programs.

## Verify installation
```
# openmpi
mpif90

# coarrays
caf source.f90 -o app
cafrun -n 2 app
```

## Compilation
```
# compile and link
gfortran source.f90 -o app

# compile, and then link (when a program comprises multiple files)
gfortran -c source.f90
gfortran source.o -o app
```

module mod_array
  implicit none
contains
  
  pure function reverse(a)
    real, intent(in) :: a(:)
    real :: reverse(size(a))
    
    reverse = a(size(a):1:-1)
  end function reverse
  
  pure real function average(a)
    real, intent(in) :: a(:)
    
    average = sum(a) / size(a)
  end function average
  
  pure real function std_dev(a)
    real, intent(in) :: a(:)
    
    std_dev = sqrt(average((a - average(a)) ** 2))
  end function std_dev
  
  ! moving simple average
  ! each element is the average of previous N elements
  ! res(i) = average(a(i-N:i))
  pure function moving_average(a, window) result(res)
    real, intent(in) :: a(:)
    integer, intent(in) :: window
    real :: res(size(a))
    
    integer :: i, i_start
    
    do i = 1, size(a)
      i_start = max(i - window, 1)
      res(i) = average(a(i_start:i))
    end do
  end function moving_average
  
  pure function moving_std_dev(a, window) result(res)
    real, intent(in) :: a(:)
    integer, intent(in) :: window
    real :: res(size(a))
    
    integer :: i, i_start
    
    do i = 1, size(a)
      i_start = max(i - window, 1)
      res(i) = std_dev(a(i_start:i))
    end do
  end function moving_std_dev
  
  pure function cross_pos(a, window) result(res)
    ! Return indexes where the value of 'a' crosses its 
    ! moving average (with window 'window') from negative to positive
    real, intent(in) :: a(:)
    integer, intent(in) :: window
    integer, allocatable :: res(:)
    
    real, allocatable :: moving_avg(:)
    logical, allocatable :: greater(:), smaller(:)
    integer :: i
    
    res = [(i, i = 2, size(a))]  ! initially, all indexes but the first
    
    moving_avg = moving_average(a, window)
    greater = a > moving_avg
    smaller = a < moving_avg
  
    ! filters an array based on a condition: a(i+1) > avg and a(i) < avg
    res = pack(res, greater(2:) .and. smaller(:size(a)-1))
  end function cross_pos
  
  pure function cross_neg(a, window) result(res)
    ! same as cross_pos, but from positive to negative
    real, intent(in) :: a(:)
    integer, intent(in) :: window
    integer, allocatable :: res(:)
    
    real, allocatable :: moving_avg(:)
    logical, allocatable :: greater(:), smaller(:)
    integer :: i
    
    res = [(i, i = 2, size(a))]
    
    moving_avg = moving_average(a, window)
    greater = a > moving_avg
    smaller = a < moving_avg
  
    res = pack(res, smaller(2:) .and. greater(:size(a)-1))
  end function cross_neg
  
end module mod_array

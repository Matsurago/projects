program stocks
  use mod_io
  use mod_array
  implicit none
  
  ! allocatable = size unknown at compile time
  character(len=4), allocatable :: symbols(:)  
  character(len=:), allocatable :: timestamp(:)
  real, allocatable :: price_open(:), price_close(:), &
    price_high(:), price_low(:), &
    price_adj_close(:), volume(:)

  integer :: i
  character(len=:), allocatable :: path
  real :: gain
  integer, allocatable :: buy(:), sell(:)
  
  ! array constructor (all elements have len 4)
  symbols = ['AAPL', 'AMZN', 'NVDA']

  do i = 1, size(symbols)    
    path = './data/' // trim(symbols(i)) // '.csv'
    call read_stock(path, timestamp, price_open, price_high, price_low, &
      price_close, price_adj_close, volume)
      
    if (i == 1) then
      print *, timestamp(size(timestamp)), ' to ', timestamp(1)
      print *, 'Symbol, Price gain, Price gain (%), Volatility (%)'
      print *, '--------------------------------------------------'
    end if
    
    ! reverse the lists, as in the input file data are from new to old
    timestamp = timestamp(size(timestamp):1:-1)
    price_adj_close = reverse(price_adj_close)
    
    ! calculate stock price gain
    gain = price_adj_close(size(price_adj_close)) - price_adj_close(1)
    
    print *, symbols(i), &
      ! relative stock price gain in percents
      gain, nint(gain/price_adj_close(1) * 100), &
      ! relative stock volatility (change in price comparing to average price)
      nint(std_dev(price_adj_close) / average(price_adj_close) * 100)
    
    ! write stock volatility stats
    path = './output/' // trim(symbols(i)) // '_volatility.txt'
    call write_stock(path, &
      timestamp, &
      price_adj_close, &
      moving_average(price_adj_close, 30), &
      moving_std_dev(price_adj_close, 30))
      
    ! calculate and write the best time to buy and sell stocks (looks like bad advice)
    buy = cross_pos(price_adj_close, 30)
    sell = cross_neg(price_adj_close, 30)
    
    path = './output/' // trim(symbols(i)) // '_crossover.txt'
    call write_crossover(path, timestamp, buy, sell)
  end do

end program stocks

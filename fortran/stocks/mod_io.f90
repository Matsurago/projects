module mod_io
  use mod_alloc, only: realloc
  implicit none
contains
  ! reads a CSV file containing stock data
  subroutine read_stock(path, timestamp, price_open, price_high, price_low, &
    price_close, price_adj_close, volume)
    
    character(len=*), intent(in) :: path  ! assumed length of char array
    character(len=:), allocatable, intent(in out) :: timestamp(:)
    real, allocatable, intent(in out) :: price_open(:), price_close(:), &
      price_high(:), price_low(:), &
      price_adj_close(:), volume(:)
      
    integer :: file_handle
    integer :: i, n
    
    n = num_records(path) - 1
    
    if (allocated(timestamp)) deallocate(timestamp)
    allocate(character(len=10) :: timestamp(n))
    
    call realloc(price_open, n)
    call realloc(price_high, n)
    call realloc(price_low, n)    
    call realloc(price_close, n)
    call realloc(price_adj_close, n)
    call realloc(volume, n)
    
    open(newunit=file_handle, file=path)
    read(file_handle, fmt=*, end=1)  ! skips reading the header
         ! jumps to label '1' if exception
    
    do i = 1, n
      read(file_handle, fmt=*, end=1) timestamp(i), &
        price_open(i), price_high(i), price_low(i), &
        price_close(i), price_adj_close(i), volume(i)
    end do
    
    close(file_handle)
    return
    
    1 print *, 'Error reading file: ', path  ! label '1'
    close(file_handle)
  end subroutine read_stock

  ! determines how many lines in a text file
  integer function num_records(path)
    character(*), intent(in) :: path
    integer :: file_handle
    
    num_records = 0
    
    open(newunit=file_handle, file=path)
    do
      read(file_handle, fmt=*, end=1)
      num_records = num_records + 1
    end do
    
    1 continue
    close(unit=file_handle)
  end function num_records

  subroutine write_stock(path, timestamp, price, moving_average, moving_std)
    character(len=*), intent(in) :: path
    character(len=:), allocatable, intent(in) :: timestamp(:)
    real, intent(in) :: price(:), moving_average(:), moving_std(:)
  
    integer :: file_handle
    integer :: i
    
    open(newunit=file_handle, file=path)
    do i = 1, size(timestamp)
      write(file_handle, fmt=*) timestamp(i), price(i), &
        moving_average(i), moving_std(i)
    end do
    close(file_handle)
  end subroutine write_stock
  
  subroutine write_crossover(path, timestamp, buy, sell)
    character(len=*), intent(in) :: path
    character(len=:), allocatable, intent(in) :: timestamp(:)
    integer, allocatable :: buy(:)
    integer, allocatable :: sell(:)
    
    integer :: file_handle
    integer :: i
  
    open(newunit=file_handle, file=path)
    do i = 1, size(buy)
      write(file_handle, fmt=*) 'Buy ', timestamp(buy(i))
    end do
    do i = 1, size(sell)
      write(file_handle, fmt=*) 'Sell ', timestamp(sell(i))
    end do
    close(file_handle)
  end subroutine write_crossover

end module mod_io

module mod_alloc
  implicit none
contains
  subroutine realloc(a, n)
    real, allocatable, intent(in out) :: a(:)
    integer, intent(in) :: n
    integer :: stats
    character(len=100) :: err_msg
    
    if (allocated(a)) call dealloc(a)
    
    allocate(a(n), stat=stats, errmsg=err_msg)
    if (stats > 0) error stop err_msg
  end subroutine realloc

  subroutine dealloc(a)
    real, allocatable, intent(in out) :: a(:)
    integer :: stats
    character(len=100) :: err_msg
    
    if (.not. allocated(a)) return
    
    deallocate(a, stat=stats, errmsg=err_msg)
    if (stats > 0) error stop err_msg
  end subroutine dealloc

end module mod_alloc

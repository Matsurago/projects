program cold_front
  implicit none

  ! these variables are visible to the function
  real, parameter :: t1_ = 12., t2_ = 24.
  real, parameter :: c_ = 20.
  real, parameter :: distance = 960
  
  integer :: h

  do h = 6, 48, 6
     print *, 'Temperature after ', h, ' hours is ', &
          calc_temp(t1_, t2_, c_, distance, real(h)), ' degrees.'
  end do

contains
  ! elemental allows passing scalars & arrays
  ! elemental implies pure
  elemental real function calc_temp(t1, t2, c, dx, dt) result(res)
    real, intent(in) :: t1, t2, c, dx, dt

    res = t2 - c * (t2 - t1) / dx * dt
  end function calc_temp
  
end program cold_front

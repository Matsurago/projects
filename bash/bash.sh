#!/bin/sh

# find all open ports and who is listening (as root to see app names)
netstat -tulpn

# list contents of a zip archive
unzip -l

# useful aliases
alias now="date +'%F-%H%M%S'"

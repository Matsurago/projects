# for permutations() and combinations(), install:
# install.packages("gtools")
library(gtools)

# 7-digit all phone numbers (containing digits from 0 to 9)
allPhoneNumbers <- permutations(10, 7, v = 0:9)


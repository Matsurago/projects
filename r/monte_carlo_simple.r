# an urn with read and blue beads
beadTypes <- c("red", "blue")
numToPick <- c(2, 3)
beads <- rep(beadTypes, times = numToPick)

# take a bead from the urn
bead <- sample(beads, 1)

# take 10000 beads from the urn (with putting them back)
numSimulations <- 10000
outcomes <- replicate(numSimulations, sample(beads, 1))

# another way to do it:
outcomes <- sample(beads, numSimulations, replace = TRUE)

# report the results
outcomeTable <- table(outcomes)
proportions <- prop.table(outcomeTable)  # contains 'probabilities' to get each type of a bead

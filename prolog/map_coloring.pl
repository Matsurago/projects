country(X) :- member(X, [belgium, denmark, france, 
                         germany, netherlands, luxemburg]).

color(X) :- member(X, [black, yellow, red, blue]).

borders(belgium, france).
borders(belgium, germany).
borders(belgium, netherlands).
borders(belgium, luxemburg).
borders(denmark, germany).
borders(france, germany).
borders(france, luxemburg).
borders(germany, netherlands).
borders(germany, luxemburg).

adjacent(X, Y) :- borders(X, Y); borders(Y, X).

valid_coloring(_, []).

valid_coloring(Country1/Color1, [Country2/Color2 | Others]) :-
    country(Country2), color(Color2),
    (
        (not(adjacent(Country1, Country2)));
        (adjacent(Country1, Country2), Color1 \= Color2)
    ),
    valid_coloring(Country1/Color1, Others).

solution([]).

solution([Country/Color | Others]) :- 
    country(Country), color(Color),
    valid_coloring(Country/Color, Others),
    solution(Others).

template([belgium/_, denmark/_, france/_, germany/_,
          netherlands/_, luxemburg/_]).

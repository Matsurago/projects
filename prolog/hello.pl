action(doom).
action(quake).
action(farcry).

rpg(witcher).
rpg(skyrim).

strategy(civilization).
strategy(heroes).

game(Thing) :- rpg(Thing).
game(Thing) :- action(Thing).
game(Thing) :- strategy(Thing).

prefers(alice, witcher).
prefers(bob, doom).

row(X) :- member(X, [1, 2, 3, 4, 5, 6, 7, 8]).
coord(X, Y) :- row(X), row(Y).

solution([]).

solution([C | Others]) :-
    solution(Others),
    noattack(C, Others).

noattack(_, []).

noattack(X1/Y1, [X2/Y2 | Others]) :-
    coord(X1, Y1), coord(X2, Y2),
    Y1 =\= Y2,  % not same row
    Y2-Y1 =\= X2-X1,  % not same upper diagonal
    Y2-Y1 =\= X1-X2,  % not same lower diagonal
    noattack(X1/Y1, Others).

template([1/_, 2/_, 3/_, 4/_, 5/_, 6/_, 7/_, 8/_]).

% query:
% ?- template(X), solution(X).

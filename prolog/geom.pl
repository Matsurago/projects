point(1, 1).
point(2, 3).
seg(point(1, 1), point(2, 3)).
triangle(point(4, 2), point(6, 4), point(7, 1)).

% which segment is vertical
vertical(seg(point(X, _), point(X, _))).

% which segment is horizontal
horizontal(seg(point(_, Y), point(_, Y))).

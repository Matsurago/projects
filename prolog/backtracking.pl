% with CUT it is like if/elif/else
f(X, 0) :- X < 3, !.
f(X, 2) :- X < 6, !.
f(_, 4).

% max example without CUT (inefficient)
max_ineff(X, Y, X) :- X >= Y.
max_ineff(X, Y, Y) :- Y > X.

% max example with CUT
max(X, Y, X) :- X >= Y, !.
max(_, Y, Y).

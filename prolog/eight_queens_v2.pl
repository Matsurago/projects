% This is less efficient than the solution in eight_queens.pl

% query:
% ?- solution(X).

solution(X) :-
    permutation([1, 2, 3, 4, 5, 6, 7, 8], X),
    safe(X).

safe([]).

safe([Q | Others]) :- 
    safe(Others),
    noattack(Q, Others, 1).

noattack(_, [], _).

noattack(Y1, [Y2 | Ys], XDist) :-
    Y2-Y1 =\= XDist,
    Y1-Y2 =\= XDist,
    noattack(Y1, Ys, XDist + 1).

% facts
parent(pamela, bob).
parent(tom, bob).
parent(tom, liz).

parent(bob, anna).
parent(bob, patrick).

parent(patrick, jim).

female(pamela).
female(liz).
female(anna).
male(tom).
male(bob).
male(jim).

% rules
offspring(Y, X) :- parent(X, Y).
grandparent(X, Y) :- parent(X, Z), parent(Z, Y).
mother(X, Y) :- parent(X, Y), female(X).
sister(X, Y) :- parent(Z, X), parent(Z, Y), female(X).

happy(X) :- parent(X, _).
hastwochildren(X) :- parent(X, Y), sister(Z, Y), parent(X, Z).
grandchild(X, Y) :- parent(Z, X), parent(Y, Z).
aunt(X, Y) :- parent(Z, Y), sister(X, Z).

predecessor(X, Y) :- parent(X, Y); parent(X, Z), predecessor(Z, Y).

relatives(X, Y) :- 
    predecessor(X, Y);
    predecessor(Y, X);
    predecessor(Z, X), predecessor(Z, Y);
    predecessor(X, Z), predecessor(Y, Z).

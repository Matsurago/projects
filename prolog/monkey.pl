% world state:
% monkey_xpos, monkey_ypos, box_pos, has_banana

% initial world state (use it for query):
% state(atdoor, onfloor, atwindow, hasnot).

% winning state
% state(_, _, _, has).

% can the monkey get the banana?
canget(state(_, _, _, has)).
canget(State1) :- 
    move(State1, _, State2), 
    canget(State2).

% state transitions:
% - grasp banana: if (middle, onbox, middle, hasnot)
% - climb box
% - push box
% - walk around
%
% move(StateBefore, Move, StateAfter).

% grasp move
move(state(middle, onbox, middle, hasnot),
     grasp,
     state(middle, onbox, middle, has)).

% climb the box move
move(state(BoxPos, onfloor, BoxPos, Has),
     climb,
     state(BoxPos, onbox, BoxPos, Has)).

% push box move
move(state(BoxPos1, onfloor, BoxPos1, Has),
     push(BoxPos1, BoxPos2),
     state(BoxPos2, onfloor, BoxPos2, Has)).

% walk around move
move(state(MonkeyPos1, onfloor, BoxPos, Has),
     walk(MonkeyPos1, MonkeyPos2),
     state(MonkeyPos2, onfloor, BoxPos, Has)).

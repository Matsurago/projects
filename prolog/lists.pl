% membership (std f: member)
my_member(X, [X | _]) :- !.  % find only first occurence
my_member(X, [_ | Tail]) :- my_member(X, Tail).

% concatenation (std f: append)
conc([], L, L).
conc([X | L1], L2, [X | L3]) :- conc(L1, L2, L3).

% add to a list without duplication
add(X, L, L) :- my_member(X, L), !.
add(X, L, [X | L]).

% membership via conc
my_member_v2(X, L) :- conc(_, [X | _], L).

% delete
del(X, [X | Tail], Tail).
del(X, [Y | Tail1], [Y | Tail2]) :- del(X, Tail1, Tail2).

% insert
insert(X, List, BiggerList) :- del(X, BiggerList, List).

% sublist
sublist(S, L) :- conc(_, L2, L), conc(S, _, L2).

% permutation
permutation([], []).
permutation([X | L], P) :-
    permutation(L, L1), insert(X, L1, P).

% ex.
evenlength([]).
evenlength([_ | [_ | Tail]]) :- evenlength(Tail).

oddlength([_ | Tail]) :- evenlength(Tail).

reverse([], []).
reverse([X | Tail], R) :- reverse(Tail, T), conc(T, [X], R).

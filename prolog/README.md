# Prolog

## Set Up

Use SWI Prolog distribution:
https://www.swi-prolog.org/


## Console

"," means AND, and ";" means OR.

Use
```
[filename].
```
to load a database (note the dot).

## Program

Order of clauses in Prolog is crucial, because the topmost clause for the same goal becomes
a preffered move, and it will be tried first. If the move is not optimal, it can lead to
infinite repetition of pointless moves.

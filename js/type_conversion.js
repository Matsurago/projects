// JS has 7 primitive types:
//   boolean, number, string, null, undefined, symbol
// and one non-primitive type: object

// 1. using a constructor without new:
const x = Number("33.3");
const y = Number("3a");  // NaN in case of failure
console.log(`x = ${x}, y = ${y}`)

// 2. using conversion functions:
const a = parseInt("12 girls", 10);  // 12
const b = parseInt("ff", 16);        // 255
const c = parseFloat("19.2 F");           // 19.2
console.log(`a = ${a}, b = ${b}, c = ${c}`)

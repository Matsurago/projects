// classes replace the old Prototype-style objects
class Box {
    _value = null;

    constructor(value) {
        this._value = value;
    }

    // class function
    isEmpty() {
        return !this._value;
    }

    get value() {
        return this._value;
    }

    set value(v) {
        console.log("setting new value: " + v);
        this._value = v;
    }
}

let box1 = new Box();
let box2 = new Box(42);
console.log("box1 empty: " + box1.isEmpty());
console.log("box2 empty: " + box2.isEmpty());

box2.value = 100;  // invokes setter
console.log("box2: " + box2.value)

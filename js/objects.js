let obj = {};    // create empty object
obj.foo = 123;      // create new property 'foo' and assign a value to it
let x1 = obj.foo;      // get the value of the property
let x2 = obj['foo'];   // same
let x3 = obj['fo' + 'o'];      // we can compute the property name using the square brackets
obj.bar = function () { /*...*/ };    // property can be a function (which is also an object)

// If the name of a property is not a valid identifier,
// it must be enclosed in quotes:
let obj2 = {
  name: 'Jane',
  'pet name': 'Wolf'
};

// The 'delete' operator removes a property:
delete obj.foo;

// The 'in' operator checks whether a property exists
if ('bar' in obj)     // true
  console.log(`${obj} has property 'bar'`)

// Note: objects are always compared by reference:
console.log('{} === {}: ' + ({} === {}));   // false

// The instanceof operator:
console.log({} instanceof Object);  // true
console.log([] instanceof Array);   // true
console.log([] instanceof Object);  // true, Array is a subconstructor of Object

// combine objects
const id = { id: 100 };
const username = { username: 'user', alias: 'mouse123' };
const user = { ...id, ...username };
console.log(user);  // { id: 100, username: 'user', alias: 'mouse123' }

// Math functions are in the Math object:
Math.abs(-1);
Math.pow(7);
Math.max(1, 3);
Math.round(8.12);
Math.cos(0.78);
Math.floor(8.79);   // 8
Math.random();        // between 0 and 1

// Eval parses its argument in *statement* context.

// {foo: 123} is a block with a label (i.e. statement)
let x = eval("{ foo: 123 }")
console.log(x);  // 123

// ({foo: 123}) is an object literal (i.e. expression)
let y = eval("({foo: 123})")
console.log(y);  // { foo: 123 }

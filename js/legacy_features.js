// To enable strict mode, type this at the first line after <script>
// or in the first line of a function:
'use strict';

// Var variables and functions are hoisted, i.e.
// moved to the top of their current scope.
function foo() {
  bar();    // OK, bar is hoisted
  function bar() {
    // ...
  }
}

// But var assignments are not hoisted.
function foo() {
  bar();    // Not OK, bar is still undefined
  var bar = function() {
    // ...
  };
}

// NOTE: The scope of a variable is always the whole function.
function foo() {
  var x = -512;
  if (x < 0) {
    var tmp = -x;
  }
  console.log(tmp);   // 512
}

// Sometimes IIFE ("iffy", immediately invoked function expression)
// is used to limit the scope of a variable, preventing it to become global.
(function() {
  var tmp = 10;   // not a global variable
}());

// All actual arguments passed to a function are stored
// in the special variable 'arguments'. 
// If a function is called with fewer arguments than expected,
// the missing parameters become 'undefined'.

// Old style classes:
// a constructor of Point
function Point(x, y) {
  this.x = x;
  this.y = y;
}

// member methods of Point
Point.prototype.distance = function() {
  return Math.sqrt(this.x * this.x + this.y * this.y);
}

// it can be used in the following way:
let p = new Point(3, 7);
console.log("distance: " + p.distance());
console.log(p instanceof Point);   // true

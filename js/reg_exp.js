// Regexp literals are enclosed in forward slashes:
// For example: /[A-Za-z0-9]+/
/^a+b+$/.test('aaab');         // test for a match (here: true)
/a(b+)a/.exec('_abbba_aba');   // test and capture groups (here: ['abbba', 'bbb'])

// replace all occurrences of regexp /.../ in the 'string'
'string'.replace(/.../g, '[$1]');

// Logical operators AND, OR return the operand that
// determined the outcome:
function foo(obj) {
    let options = obj || { val: "default" };
    console.log(options);
}

foo();

// JS has only one numeric type: 'number', which is IEEE-764 floating point

// Although 'number' is a primitive type, methods can be called on it, such as .toFixed()
// In this case it's temporary converted to the wrapper Number type

// set number precision
const a = 10.928376;
const b = a.toFixed(3);
console.log(`b = ${b}`);  // 10.928

// division errors result in Infinity
console.log( 3/0 );
// too big numbers result in Infinity
console.log( Math.pow(2, 1024) );

// conversion errors result in NaN
console.log( Number("1a") );

// 0, -0, NaN, null, undefined are all false:
let c = 0, d = -0, e = Number("x"), f = null, u;
if (c || d || e || f || u)
    console.log("this is never printed");

// special numbers
console.log( Number.EPSILON );  // the smallest value that can be added to 1 to get a distinct number
console.log( Number.MAX_SAFE_INTEGER );
console.log( Number.MAX_VALUE );

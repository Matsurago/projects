// Symbols are unique identifiers.
// The description parameter can be used for debug purposes.
const RED = Symbol("color");
const ORANGE = Symbol("color");
console.log(RED == ORANGE);   // false: symbols are unique

// symbols are one of the 7 primitive JS types
console.log(typeof RED);  // 'symbol'

// All strings in JS are unicode.

// 'string' is a primitive type, but it has a corresponding String wrapper type.
// The wrapper type is applied when you call methods on strings, such as toUpperCase()
// The wrapper object is discarded after the call.
const a = "hello world";
a.length;
a.toUpperCase();
a[1];            // get a char from the string ('e')
a.slice(1, 4);   // substring 'ell'
a.slice(-3);     // substring 'rld'
a.indexOf('w');  // find the position of 'w'

// empty string is false:
const s = '';
if (s)
    console.log('this is never printed')

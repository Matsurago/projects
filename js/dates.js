const now = new Date();  // current date/time
const school_day = new Date(2015, 9, 1);  // specific 
const dt = new Date(2015, 9, 1, 19, 0);   // 2015/9/1 19:00
dt.getFullYear();  // 2015
dt.getDate();      // 1 (1st) <-- actually the day in month!
dt.getDay();       // 0 = Sun, 1 = Mon, ...  <-- day of the week
dt.getTime();      // number of ms since Jan 1, 1970 UTC

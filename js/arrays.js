// Array size is not fixed.
// Each individual element can be of any type.
const a = [1, 2, "three", null];
const b = [
    [1, 2, 3],
    [4, 5, 6]
];

const len = a.length;
const lastElement = a[a.length - 1];
const concatStr = a.toString();   // "1,2,three,null"
const concat2 = a.join(", ");     // "1, 2, three, null"

a[7] = "test"; // resizes the array and adds an element
               // all new elements in between have value "undefined" 
a.push(3); // adds an element to the tail of the array
const last = a.pop();   // removes (and returns) the last element
const first = a.shift();   // removes (and returns) the first element
a.unshift("x");   // adds an element to the beginning of the array
const largeArr = a.concat([1, 2, 3]);  // adds multiple elements to the tail
const largeArr2 = a.concat(1, 2, 3);   // same effect

// slice(a, b): returns *new* subarray [a, b)
const c = [1, 2, 3, 4, 5];
c.slice(3);      // [4, 5]
c.slice(2, 4);   // [3, 4]
c.slice(-2);     // [4, 5]  two last elements
c.slice(1, -2);  // [2, 3]  i.e. elements with indexes [1, length - 2)
c.slice(-2, -1); // [4]

// fill: initializes or rewrites elements of the array
const arr = new Array(5).fill(1);   // array of ones (5 elements)
arr.fill("a", 1);  // sets all elements to "a" starting from the first

// sort:
let names = [{name: 'Francis'}, {name: 'Julia'}, {name: 'Ivan'}];
names.sort();  // default sort
names.sort((a, b) => a.name > b.name);  // can specify the comparing function 

/* search:
   - indexOf();
   - lastIndexOf();
   - find();  // same as findIndex, but returns the element itself (or null if not found)
   - findIndex();  // allows to specify WHERE condition as a function
*/
names.findIndex(obj => obj.name === "Francis");

/* other useful functions:
   - splice(index, num_to_remove, new_elements...); // removes and adds elements at any position 
   - copyWithin(to_where, from_where); // ES6: copies and pastes elements in the same array
   - reverse();
*/

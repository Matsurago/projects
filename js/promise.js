// create a promise
const wait = ms => new Promise(
    (resolve, reject) => setTimeout(resolve, ms)
);

// pipeline pattern
wait(300)
    .then(() => 20)
    .then(x => x + 1)
    .then(x => x * 2)
    .then(x => console.log(x));

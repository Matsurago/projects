// we use the fact that we can define properties on function objects
const fibo = function() {
    const f = function(n) {
        if (n <= 0) 
            throw new Error(`fibo: arg must be positive but was '${n}'`);
        
        if (fibo.cache[n])
            return fibo.cache[n];

        let res = (n == 1 || n == 2) 
            ? 1
            : fibo(n - 1) + fibo(n - 2);

        fibo.cache[n] = res;
        return res;
    }
    f.cache = {};
    return f;
}();

console.log(fibo(50));

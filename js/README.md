# JavaScript Code Snippets

## Dev Tools
- Node (npm): Run JS outside the browser
- Gulp; Grunt: A build tool (run once: `npm install -g gulp`)
- Babel: Transcompiler (ES6 → ES5)
- ESLint: Static analysis (`npm install -g eslint`)

## Create a New JS Project
1. `git init`
2. Add `.gitignore` (node_modules, *.log, *.tmp)
3. Init the project and add dependencies:
```
npm init  // generates package.json that manages dependencies
npm install --save package@version  // to add a runtime dependency
npm install --save-dev package@version  // to add a compile dependency
npm install  // recreates node_modules based on package.json
```
4. Install gulp:
```
npm install --save-dev gulp
add gulpfile.js
```
5. Make directories for:
   - server-side code (`es6`; Node.js)
   - client-side code (`public/es6`; browser)
   - transcompiled server/client code (`dist`, `public/dist`)
6. Install babel:
```
npm install --save-dev babel-preset-es2015
npm install --save-dev gulp-babel
add .babelrc
```
7. `eslint --init` (generates .eslintrc)

## Generate SRI (Subresource Integrity) Hashes
```
#!/bin/sh
input='arrays.js'
openssl dgst -sha384 -binary $input | openssl base64 -A
echo ''
```

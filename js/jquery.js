// Calling a function when DOM is fully loaded:
$(document).ready(function() { /*...*/ });
$(function() { /*...*/ });  // same

// Getting elements:
const paragraphs = $("p");  // all <p>
const len = paragraphs.length;   // number of <p>s returned
const third_p = $("p").eq(2);  // get third <p>
const every_second = $("p:odd");  // same as $("p").filter(":odd");
$("p").find(".code");  // finds the descendant elements matching criteria

// Creating new elements:
const newParagraph = $("<p>New one</p>");

// Manipulating elements:
const ps = $("p");
ps.text("some text");  // sets text for all paragraphs at once
ps.html("<b>Some</b> text"); // sets inner HTML
ps.remove();  // delete all paragraphs
ps.append("<i>hmm!</i>"); // appends HTML to all paragraphs, same as $("<br>").appendTo("p");
ps.after("<br>"); // append after, same as $("<br>").insertAfter("p");
ps.before("<hr>"); // append before, same as $("<br>").insertBefore("p");
ps.addClass("my"); // adds CSS class
ps.removeClass("my");
ps.toggleClass("my");  // adds or removes a class!
ps.css("color", "red");  // manipulate CSS

// Calls can be chained:
$("p").after("<hr>").not(".highlight").css("margin-left", "20px");

// Ajax: handling success and failure:
$.get("http://host/uri").then(function() { /*...*/ }, function() { /*...*/ }); 

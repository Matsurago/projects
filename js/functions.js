// Objects can have functions as properties, in which case 
// they are called "methods". The keyword "this" can be used
// to access object's fields inside the function: 
const thomas = {
  name: "Thomas",
  greeting: function() {
    return `Hi, I am ${this.name}`;
  }
};

// From ES6, there's a shorthand for declaring methods:
const obj = {
  name: "Thomas",
  greeting() {
    return `Hi, I am ${this.name}`;
  }
}

// Function is a subtype of object.
// Thus, like objects, functions can have properties.
// Also see examples/fibo.js
function foo() {
  return 42;
}
foo.bar = "some value";  // define 'bar' property

// Functions have some properties (methods) by default:
foo.call(obj, 10, 30); // executes the function on a given object with given arguments
foo.apply(obj, [10, 30]); // same, but arguments are supplied in the array
foo.bind(obj); // permanently(!) asscociates the function with the object "obj"
               // another bind() will not change the ownership(!)

// objects can be updated inside a function:
const empty = {};
function g() {
  this.value = "123";
}
g.call(empty);  // object 'empty' gets new property!
console.log(`empty.value = ${empty.value}`)

// functions are first-class citizens:
let my = foo;  // can be assigned to another variable
my();          // calls the test() function

// functions can have default arguments (from ES6):
function foo(a, b = "user", c = 3) {
  // ...
}

// spread operator collects additional arguments:
function bar(val, ...rest) {
  console.log(`val = ${val}, rest = ${rest}`)
}
bar(3, 4, 5); // 'val' gets 3, and 'rest' gets [4, 5]

// spread operator can also be used on arguments themselves
let nums = [1, 2, 3, 4]
console.log("max: " + Math.max(...nums))

// Lambda expressions:
// - can omit "return" and curly braces if the body is a single expression;
// - can omit parenthesis around parameters, if it takes only one parameter
// - can have default argument values
const f1 = () => "hello!";
const f2 = name => `Hello, ${name}`;
const f3 = (a, b) => a + b;
const f4 = (...xs) => xs.reduce((x, a) => x + a, 0)

// must enclose an object in parenthesis when returned from a lambda:
const f5 = () => ({
  name: 'user',
  password: '123',
})

// currying
const f6 = x => y => x + y;
// application
let a = f6(2)(3);  // a = 5
// partial application
const h = f6(2);
let b = h(3);        // b = 5

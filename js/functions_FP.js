/* FP array functions:
   - some();  // same as find, but returns true if there is an element that matches the criteria
   - every(); // returns true, if every element in the array matches the criteria
   - map((element, index) => ...);
     map(element => ...);
     map(funOfOneArg);
   - filter();
   - reduce();
*/

// reduce example
const f = x => x + 1;
const g = x => x * 2;
const h = x => y => x * y;
// this is fold left
const v = [f, g, h(3)].reduce((x, f) => f(x), 20);
console.log(v);  // 126

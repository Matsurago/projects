use v5.12;
use warnings;

sub say_hello {
    state $n = 0;  # preserves values between calls
    $n += 1;
    say "hello, world! (x$n)";
    7;  # last value is the return value
}

sub my_min {
    # @_ stores the list of arguments
    my ($x, $y) = @_;
    if ($x <= $y) {
        $x
    } else {
        $y
    }
}

sub better_min {
    my ($min, @rest) = @_;
    for (@rest) {
        if ($_ < $min) {
            $min = $_;
        }
    }
    $min
}

&say_hello;          # call a function
my $x = &say_hello;  # save the return value
say "x = $x";

my $m = &my_min(9, 7);
say "m = $m";

$m = &better_min(19, 9, 5, 10, 100, 4, 7, 10);
say "m = $m";

# can can call a function declared above without & and (), but
# the name MUST NOT clash with an existing Perl function
# and the definition must come before invocation
$m = better_min 7, 12, 3, 10;
say "m = $m";

# anonymous functions
my $_inc = sub {
    $x = shift;
    ++$x;
};
# call an anonymous function
say "inc 6 = ", $_inc->(6);

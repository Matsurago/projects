use v5.12;
use warnings;

# 1. Array constructing
# qw: all words are single-quoted (qq for double-quoted)
my @colors = qw( red blue orange green yellow );
say "@colors";  # need quotes to separate elements with spaces
# x: makes n copies of a given element
my @zeroes = (0) x 10;
say "@zeroes";  # ten zeroes
# ranges (only counts up):
my @my_range = (1..3, 7..9);
say "@my_range";
my @alphabet = ('A' .. 'Z');
say "@alphabet";

# 2. Array access
say "$colors[0]";        # first element, use $
say "@colors[1..3]";     # second to forth elements (inclusive)
say "@colors[1,3]";      # second and forth elements
say "$colors[$#colors]"; # last element
say "$colors[-1]";       # last element

# destructuring
my ($w1, $w2, @wrest) = @colors;
say "w1 = $w1, w2 = $w2, rest = @wrest";

# 3. Array copy
# lists are copied on assignment
my @list1 = (1, 2, 3);
my @list2 = @list1;
$list1[0] = 4;
say "list 1: @list1,  list 2: @list2\n";

# 4. Array read-only functions
# array size: do not use length() function, it's for strings only
say "length = " . scalar @colors;
# join
say "join: " . join ",", @colors;
# filter
my @clr = grep length($_) >= 5, @colors;
say "colors longer than 5 symbols: @clr";
# map & filter: the order is from tne bottom
my @clr_upper =
    map { uc }
    grep { index($_, 'o') != -1 }
    @colors;
say "colors with 'o': @clr_upper";
# sum; can also 'use List::Util qw(sum)'
say 'sum = ', List::Util::sum @my_range;

# 5. Array modification functions
# add:
my @numbers = (1, 2, 3);
push(@numbers, 4, 5);  # add to the end
unshift(@numbers, 0);  # add to the beginning
say "numbers: ", "@numbers";

# remove:
my $last = pop(@numbers);     # remove the last element
my $first = shift(@numbers);  # remove the first element
say "numbers: @numbers; first = $first, last = $last";

# insert in the middle:
@numbers = (@numbers[0..1], 7, 8, @numbers[2..$#numbers]);
say "@numbers";

# splice (2 args): remove everything starting with index 3
my @letters = qw( a b c d e f g );
my @removed_elements = splice(@letters, 3);
say "removed elements: @removed_elements,  array: @letters";

# splice (3 args): remove 2 elements starting with index 3
@letters = qw( a b c d e f g );
@removed_elements = splice(@letters, 3, 2);
say "removed elements: @removed_elements,  array: @letters";

# splice (4 args): replace 2 elements starting with index 3 with (x, y, z)
@letters = qw( a b c d e f g );
@removed_elements = splice(@letters, 3, 2, qw( x y z ));
say "removed elements: @removed_elements,  array: @letters";

# splice (4 args): insert elements (x, y, z) at index 3
@letters = qw( a b c d e f g );
splice(@letters, 3, 0, qw( x y z ));  # use zero as number of elements to remove
say "array: @letters";

# reverse
my @reversed_range = reverse @my_range;
say "reversed: @reversed_range";
# partial reverse
@my_range[1..4] = reverse @my_range[1..4];
say "reversed (1..4): @my_range";

# 5. Loops
# foreach
@numbers = 1..5;
for my $num (@numbers) {
    $num *= 2;  # can modify the list itself
}
say "doubled numbers: @numbers";

# can omit the loop variable, now it is $_
for (@numbers[0..2]) {
    say $_;
}

# can even omit the $_, as print/say by default operates on it
for (@numbers[0..1]) {
    say;
}

# each (requires 5.12)
# note: never use each more than once simultaneously
# it operates on the same pointer to the internal structure!
while ((my $index, my $value) = each @numbers) {
    # also don't end the loop prematurely
    say "index = $index, value = $value";
    # better not use each() at all
}

# 6. Sort
# sort in dictionary order
# WARNING: numbers are treated as strings!
@numbers = (1, 2, 10, 9, 100, 7, 17);
my @s_numbers = sort @numbers;
my @rs_numbers = reverse sort @numbers;
say "\nsorted: @s_numbers";
say "reverse sorted: @rs_numbers";

say "-" x 10;

# sort in numerical order
@s_numbers = sort {$a <=> $b} @numbers;
@rs_numbers = sort {$b <=> $a} @numbers;  # reversed!
say "sorted: @s_numbers";
say "reverse sorted: @rs_numbers";

# sort in dictionary order, case-insensitive
my @words = qw( Cat alpaca camel Dog );
my @s_words = sort @words;
say "@s_words";  # Cat, Dog, alpaca, camel
@s_words = sort {(lc $a) cmp (lc $b)} @words;
say "@s_words";  # alpaca, camel, Cat, Dog

# X. Pitfalls !!
# $len gets size of array ('array' in scalar context)
my $len = @words;
say "len: $len";
# $lst gets last element ('list' in scalar context)
my $lst = ('cat', 'mouse', 'dog');
say "last: $lst";
# $fst gets first element ('list' in forced list context)
my ($fst) = ('cat', 'mouse', 'dog');  # parentheses around $fst are important and change meaning
say "first: $fst";

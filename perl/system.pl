use strict;
use warnings;
use v5.10;

# print program arguments
say "args: @ARGV";

# print env variables
for my $key (keys %ENV) {
    say "$key -> $ENV{$key}"
}

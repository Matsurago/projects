#!/usr/bin/perl
use v5.12;

# Perl has special rules for string to number implicit conversions
say "  123abc + 100 = ",   "  123abc" + 100;   # 223   (abc is ignored)
say "  abc123 + 100 = ",   "  abc123" + 100;   # 100   (not a number = 0)
say "9.99.99  + 10  = ",   "9.99.99"  + 10;    # 19.99 (at most one . is allowed)

# int truncation can be surprising
say "4.39 * 100 = ", int(4.39 * 100);  # prints 438

# () wrongly interpreted as a function call
print "(3 + 5) * 2 = ";
print (3 + 5) * 2;       # WARNING: it prints 8
print "\n";

say "0777" + 1;  # automatic conversion treats all numbers as 10-based (not octal)

# undef variables have default values
my $sum += 7;            # gets 0 in numeric context
say $sum;
my $world .= "hello";    # gets an empty string in string context
say $world;

# array used in scalar context evaluates to number of elements in it!
my @words = qw( cat dog mouse );
my $n = @words;
say "n = $n";

# the result is different whether an operator is used in list or scalar context
my @first = reverse qw( cat dog mouse );
my $second = reverse qw( cat dog mouse );
say "first = @first, second = $second";

say scalar @first;  # can force scalar context

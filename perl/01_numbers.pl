use v5.12;
use warnings;

# division: float-based, as in Python
say "2 / 5 = ", 2 / 5;

# exponentiation: as in Python
say "2^5 = ", 2 ** 5;

my $num = 45.23000;        # assignment makes a string
say "45.23000 -> ", $num;  # 45.23; zeroes are truncated

say "10_000_000 -> ", 10_000_000;  # can use _ to separate digits
say "1.23e+4 -> ", 1.23e+4;        # scientific notation

say "0xff -> ", 0xff;              # hex literal
say "077 -> ", 077;                # oct literal
say "0b1001 -> ", 0b1001;          # binary literal

# explicit conversions
say "ff = ", hex("ff");
say "077 = ", oct("077");

# math functions
say "int(12.35) = ", int(12.35);   # 12; same as ceiling
say "abs(-100) = ", abs(-100);
say "sqrt(1.44) = ", sqrt(1.44);
say "sin(3.14) = ", sin(3.14);

# random float
srand(1000);  # set seed
say "rand(6) = ", rand(6);

# bitwise operators
$num = 42;
say "$num is even" if ($num & 1) == 0;
say "$num is odd" if ($num & 1) == 1;

# BigInt
use Math::BigInt;

my $res = Math::BigInt->new(1);
my $n = 30;
for (2..$n) {
    $res *= $_;
}
say "factorial($n) = $res";

## Pitfalls ##
say (1 + 2) * 3;  # prints 3, interpreted as (say(1+2)) * 3;

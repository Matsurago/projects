use v5.12;
use warnings;

# Perl hashmap
my %nums = (
    'hundred' => 100,
    'twelve'  => 12,
    'twenty'  => 20,
);

# another way to create a hash
my %chars;
@chars{'a'..'z'} = 1..26;

# read
say "n = ", $nums{'twenty'};
# read multiple
say "nums = ", "@nums{'twelve', 'twenty'}";
# read or default
say '%nums{\'two\'} = ' . ($nums{'two'} // -1);

# length
say "length = " . scalar keys %nums;  # do not use length() function, it's for strings

# add
$nums{'thirteen'} = 13;
# add multiple
%nums = (%nums, 'three' => 3, 'seven' => 7);
# add if not defined
$nums{'thirteen'} //= 130;  # 'thirteen' is already a key, so skip this
$nums{'three'} //= 3;

# iteration over keys
for my $key (keys %nums) {
    say "$key -> $nums{$key}";
}

# reverse a hash (all values must be unique):
my %words = reverse %nums;

# another way to iterate (note: never use each more than once simultaneously)
# not recommended
while (my ($k, $v) = each (%words)) {
    say "$k: $v";
}

# values
my @vals = values %nums;
say "@vals";

# check if key exists
if (exists $nums{'thirteen'}) {
    say '\'thirteen\' is in the map';
}

# delete a key/value pair from a hash
delete $nums{'thirteen'};
# delete multiple values at once
delete @nums{'seven', 'twenty'};

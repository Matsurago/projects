use v5.12;
use warnings;

# partial pattern match on default variable
# // is the same as m//
# can't omit m if a different delimiter is used, e.g. m{} or m()
$_ = "abcdef";
if (/cde/) {
    say "'$_' matches 'cde'";
}

# trick: match any same two chars
$_ = "cdde";
if (/(.)\1/) {  # \1 references 1st capture group
    say "'$_' matches '(.)\\1'. The matched part is '$1'.";  # use 1st capture group outside the pattern
    # NOTE: the values of $1, $2, ... are preserved until the next *successful* match
}

# use =~ to match on a string other than $_

# anchors:
# \A - beginning of string, ^ - beginning of line
# \Z - end of string, possibly followed by \n
# \z - end of string, $ - end of line
my $str = "test.txt\nimage.png";
if ($str =~ /\.png\z/) {
    say "'$str' matches '\\.png\\z'";
}

# use anchors for full match
my $email = 'oleg7737@mail.com';
if ($email =~ /\A\w+@\w+\.\w{2,3}\z/) {
    say "'$email' matches email pattern."
}

# match modifiers
# /i    case-insensitive
# /s    allow . to match newlines
# /x    whitespace and comments inside the pattern become insignificant (readability option)
# /u    \w matches Unicode
# /a    \w matches ASCII
# /aai  ASCII-based case only (ignore UTF-8 upper/lower case)
# /n    disable capture groups: '()' only used for grouping (since v5.22)
# /p    enable ${^PREMATCH}, ${^MATCH}, and ${^POSTMATCH} variables
if ('YES' =~ /yes/i) {
    say 'matched.';
}

# /m    multiline match: '/foo$/m' matches foo at the end of any line ('/^foo/m' at start of any line)
if ("fun foo\ndeclared here" =~ /foo$/m) {
    say 'multiline matched.'
}

# capture group modifiers
# (?:)       non-capturing group
# (?<name>)  named capture: use '\g{name}' to refer to a named group inside the pattern
if ('Vasya_Repin' =~ /(?<first>\w+) (?:\s+|_) (?<last>\w+)/x) {
    say "first name: $+{first}, last name: $+{last}";
}

# match in list context: can directly assign groups to variables without $1, $2, ..
$_ = '99-1111-2222';
my ($t_prefix, $t1, $t2) = /(\d+)-(\d+)-(\d+)/;
say "$t_prefix $t1 $t2";

# replacement (using default variable)
$_ = 'the personal number is 99-879992';
s/\d/\*/g;  # /g = replace all
say $_;

# replacement with group references
$_ = 'John payed 78 dollars.';
if (s/(\d+) dollars/$1 euros/) {  # true if substitution successful
    say $_;
}

# replacement (regular variable)
# destructive: need to make a copy
my $filename = 'readme.txt';
(my $updated_filename = $filename) =~ s/\.txt\z/\.md/;
print $updated_filename;

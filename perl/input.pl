#!/usr/bin/perl
use warnings;
use v5.12;

print "Input your name: ";
chomp(my $name = <STDIN>);  # reads input and removes \n at the end

print "Hello, ${name}!\n";

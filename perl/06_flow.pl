use v5.12;
use warnings;

# check whether a variable is defined
my $total;
if (defined($total)) {
    say "total = $total";
} else {
    say "\$total is undefined.";
}
# keyword 'unless': same as 'if', but condition is negated

# undefine a variable
undef($total);

# falsy values: undef, 0, '', '0'
my $amount = '0';
if ($amount) {
    say "this is not printed :(";
}

# swap
(my $x, my $y) = (2, 4);   # list assignment
($x, $y) = ($y, $x);
say "x = $x, y = $y";

# append to a string
my $s = "hello";
$s .= " world";
say $s;

# print uses the favourite variable ($_) by default
$_ = "something";
say;  # prints $_

# for loop
my @nums = 1..3;
for (my $i = 0; $i <= $#nums; $i++) {
    print "$nums[$i] ";
}
print "\n", "-" x 10, "\n";

# foreach loop (keywords 'for' and 'foreach' are interchangeable)
for my $num (@nums) {
    print "$num ";
}
print "\n";

# while loop: see 03_arrays.pl
# until loop: same as 'while', but condition is negated
# last: same as 'break' in C
# next: same as 'continue' in C
# continue: block that always executes in each iteration of the loop (ignoring last/next)

# foreach loop as statement modifier
say "number: $_" for 1..3;

# switch
my $number = 7;
given ($number) {
    when ($_ < 0) { say 'negative' }  # use 'continue;' to fall through
    when ($_ > 0) { say 'positive' }
    default { say 'zero' }
}

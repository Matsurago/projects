#!/usr/bin/perl
use warnings;
use v5.12;

# usage
# input_files.pl filename
# input_files.pl filename1 filename2 .. filenameN
# files to process are read from @ARGV (empty = STDIN)
while (<>) {  # use <<>> for v5.22 and above
    print $_;
}

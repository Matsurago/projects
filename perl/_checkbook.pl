#!/usr/bin/perl
use warnings;
use v5.12;

# Checkbook Application Exercise in Perl, v1.0
#
# The app:
# - handles deposits, withdrawals, checks writing;
# - looks up checks by check number or date written;
# - prints a statement

print "Welcome to checkbook 1.1\n";
print "Please enter your name: ";

chomp(my $name = <STDIN>);

print "Menu:\n";
print "1. Enter a deposit\n";
print "2. Enter a withdrawal\n";
print "3. Enter a check\n";
print "4. Lookup a check by #\n";
print "5. Lookup a check by date\n";
print "6. Print a statement\n";
print "7. Exit program\n\n";
print "Please enter your menu option: ";

chomp(my $choice = <STDIN>);

my @book = ("DEP:12/12/1999:Beginning Balance:100");

# Perl Snippets

## Useful commands

Run a Perl one-liner from a command line, with post 5.10 features enabled:
```
perl -E "say 'hello'"
perl -E "while(<>){say uc $_}"
```

Run a Perl one-liner with no new features:
```
perl -e "print 'hello\n'"
```

Run a script with warnings enabled:
```
perl -w script.pl
```

Run a script with detailed diagnostics enabled:
```
perl -Mdiagnostics script.pl
```

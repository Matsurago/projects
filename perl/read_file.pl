use v5.14;  # for file handle methods
use warnings;

# open for appending: >>
# open for writing: >
# open for reading: <
if (! open MY_FILE, '>>:encoding(UTF-8)', 'output.txt') {
    die "Error: $!";  # $! holds the system error message
    # can 'use autodie' to automatically exit the app when system error occurs
}

# can 'select MY_FILE' to make it default for print/printf
print MY_FILE "output line 1\n";
printf MY_FILE "%s %g\n", "test", 14.78;
# need 'select STDOUT' afterwards to reset
close MY_FILE;

# reading a file: same as with STDIN:
# use 'while (<MY_FILE>)'

# another (recommended) way: avoid using <> and bare names
open my $f_handle, '<', '06_flow.pl';
$f_handle->autoflush(1);  # set flush (requires 'use IO::Handle' in old Perl)

chomp(my $line = readline($f_handle));
say "read line: '$line'";

# reading the entire file
my $file_contents = do {
    local $/;  # set line separator to undef
    readline($f_handle);
};
say "\n*** rest of file ***\n", $file_contents;

# clean-up
close $f_handle;

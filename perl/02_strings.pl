use v5.12;
use warnings;

use utf8;                             # tell Perl that source code is in UTF-8
use open ':std', ':encoding(UTF-8)';  # tell Perl to actually encode text in UTF-8

# Unicode usage
say "⅚ ∞ 世界";                   # a unicode string
say "\x{2668} \N{HOT SPRINGS}";  # a unicode char by code point and by name
say "\101 \x41";                 # an ASCII char by ASCII ordinal (octal and hex)
say "\cC";                       # CTRL+C
say "ω = ", chr(0x03C9);         # explicit unicode code point to char conversion
say "ω = ", ord('ω');            # explicit unicode char to code point conversion

# Quotes
# single quote: escape chars and vars are not evaluated
my $s1 = 'test\n $v';
say $s1;  # just prints as is
# double quote: escape chars, vars evaluated
say "test: $s1";
# can replace single quote with q, double with qq
say q<Richard "Falcon" Wallis>;
say qq<test: $s1>;

# a multiline string (END_OF_S is a custom separator)
my $s2 = <<"END_OF_S";
Greetings, brothers!
Today we achieve our goal!
END_OF_S
print $s2;

# special escape chars
say "\Us.a.r.l.\E";  # uppercase until \E

# next alphanumeric seq - useful!
my $sid = 'zz';
$sid++;
say "next of 'zz' is '$sid'";  # 'aaa'

# string operations
say "concat: ", "hello" . "world";
say "repeat: ", "-" x 10;
say "to lower: ", lc("HeLLo");
say "to lower (1st char): ", lcfirst("Hello");
say "to upper: ", uc("hello");
say "to upper (1st char): ", ucfirst("hello");
say "len: ", length("感性に響く");  # needs 'use utf8' pragma

# split
# NOTE: use "split /pattern/, str, -1" to handle empty commas at the end
# NOTE: calling split without args is the same as "split /s+/, $_"
my @word_list = split /,/, 'cat,dog,hamster';
say "split: @word_list";

# substring
say "substring(1) = ", substr "hello", 1;  # ello
say "substring(1, 3) = ", substr "hello", 1, 3;  # ell

# replace
my $hello = "hello";
substr $hello, 1, 2, "a";  # modifies the variable
say "substring(1, 3, 2, 'a') = $hello";  # halo
# replace all
my $names = "Vasya,Oleg,Semen";
$names =~ tr/,/-/;
say "names: $names";

# string comparison
say "\ncomparing 'cat' to 'dog': ";
say "equal:            ", 'cat' eq 'dog';
say "not equal:        ", 'cat' ne 'dog';
say "less than:        ", 'cat' lt 'dog';
say "less or equal:    ", 'cat' le 'dog';
say "greater than:     ", 'cat' gt 'dog';
say "greater or equal: ", 'cat' ge 'dog';

# formatting with variable width/precision
# use * to supply these in parameters
printf "\nprice: %*.*f \$\n", 10, 2, 1.2345;

# search
say "index of 'cat' in 'minicats' = " . index 'minitcats', 'cat', 0;  # start pos is optional
say "rindex of 'i' in 'minicats' = " . rindex 'minitcats', 'i';

# pattern matching for reading fixed-size data
my $data = '20230401smith   12345007';
my ($emp_hired, $emp_name, $emp_num, $emp_dept) = unpack 'A8A8A5A3', $data;
say "hired: $emp_hired, name: $emp_name, id: $emp_num, dept: $emp_dept";

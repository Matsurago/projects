use v5.12;
use warnings;

while (<>) {
    chomp;
    # replace with the pattern to test
    if (/\$\d+(\.\d+)?/) {
        # $`: string before the match
        # $&: matched part
        # $': string after the match
        say "match: |$`<$&>$'|";
    } else {
        say "no match: |$_|";
    }
}

use v5.12;
use warnings;

sub _pow {
    my ($x, $n) = @_;
    my $res = 1;
    return $res if $n == 0;

    while ($n > 1) {
        if ($n % 2 == 0) {
            $n /= 2;
            $x *= $x;
        } else {
            $n -= 1;
            $res *= $x;
        }
    }
    $res * $x;
}

for my $i (0..10) {
    say "pow(5, $i) = " . _pow(5, $i);
}

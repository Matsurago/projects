#!/usr/bin/perl

@lines = `perldoc -u -f atan2`;

foreach (@lines) {
    # X<atan2> -> ATAN2
    s/\w<(.+?)>/\U$1/g;  # modifies a line in memory
    print;               # prints the modified line
}

use strict;
use warnings;
use v5.10;

# requires 3rd party library
use XML::LibXML::Reader;

my $reader = XML::LibXML::Reader->new(location => 'data/users.xml')
    or die 'cannot read users.xml';

while ($reader->read) {
    process_node($reader)
}

sub process_node {
    my $reader = shift;
    my $type = $reader->nodeType;

    if ($type == XML_READER_TYPE_ELEMENT) {
        say "node: ", $reader->name;
    } elsif ($type == XML_READER_TYPE_TEXT) {
        say "value: ", $reader->value;
    }
}

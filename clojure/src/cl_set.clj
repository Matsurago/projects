(def primes #{3 5 7 11})  ; a set
(def controls #{:up :down :left :right})  ; a set of keywords 

; supplying a duplicate value raises an exception!

(println
  (contains? primes 7)  ; true

  ; when set is used as a function, the value is returned
  (primes 7)  ; 7
  (primes 8)  ; nil
)

; add a value to the set (a new set is returned)
(def more-primes (conj primes 13))  ; no exception on duplicates

; remove a value from the set
(def less-primes (disj primes 7)) 

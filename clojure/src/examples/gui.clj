(ns gui
  (:import 
    (javax.swing JFrame)
    (java.awt Toolkit)
  ))

(defn screen-size []
  (let [screen (.getScreenSize (Toolkit/getDefaultToolkit))]
    [(.-width screen) (.-height screen)]))

(defn make-window [title]
  (let [[width height] (screen-size)]
    (doto (JFrame. title)
      (.pack)
      ; (.setDefaultCloseOperation JFrame/EXIT_ON_CLOSE)
      (.setSize (/ width 2) (/ height 2))
      (.setLocationRelativeTo nil)
      (.setVisible true))))

(def words '("white" "cat"))

(let [[fst-word snd-word] words]  ; destructuring
  (println 
    "first word:" fst-word
    "; second word:" snd-word
  )
)

; can be nested, can match partially
; use _ for unneded variables
(def bs [[70 13] [98 65 67]])
(let [[[_ x1-2] [x2-1]] bs] 
  (println x1-2 x2-1)  ; 13 98
)

; not limited to let
; destructuring in a function
(defn describe [[first-name last-name]]
  (println (str "First name: " first-name ", Last name: " last-name)))
(describe ["Alex" "Willington"])

; destructuring a String gives characters
(let [[f s] "Armenia"]
  (println (str f s))  ; Ar
)

; destructuring with maps
(def person {:name "Serj" :age 30})

(let [{n :name a :age} person]  ; keys come second, unlike map def 
  (println "Hi" n ". You are" a "years old."))

; can bind keywords with same-named variables
(let [{:keys [name age]} person] 
  (println "Hi" name ". You are" age "years old."))

; can supply the default arguments when destructured variables get nil
(defn print-person 
  [{:keys [name age work tel] 
    :or {name "Adam" age 18 work "Acme Inc." tel "07-777"}
    :as p}]  ; can have the entire map, too
  (println (str "name: " name ", age: " age ", work: " work ", tel: " tel)))

(defn head [sequence]
  ; pre- and postconditions
  {:pre [
    (not= sequence nil)
    (> (count sequence) 0)
  ]
  
  :post [
    (= % (nth sequence 0))  ; % means 'return value'
  ]}

  (first sequence)
)

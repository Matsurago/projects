;; TODO
(defn replace-all [string replacements]
    string
)

(defn escape-html [string]
    (replace-all string [
        ["&", "&amp;"]
        ["\"", "&quot;"]
        ["<", "&lt;"]
        [">", "&gt"]
    ])
)

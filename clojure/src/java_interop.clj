(ns java-interop-example
  (:import java.io.File))  

; to import multiple classes from the same package  
; (:import (java.io File InputStream)))

(def f (File. "cl_set.clj"))  ; create a Java object

(println
  (.exists f)  ; call a method on the object
  (File/separator)  ; access a static field
)

(File/createTempFile "prefix" ".txt")  ; call a static method

; note that a method can not be used as a function literal as is: 
(def files [(File. "1.clj") (File. "cl_set.clj")])
(println
  ; (map .exists files)  ; does not work!
  (map (memfn exists) files)
)

(import java.awt.Rectangle)
(import '(java.util Random Date))  ; import multiple classes

(def r (Rectangle. 0 0 20 30))  ; can also use fully-qualified class name

(println
  (.-width r)    ; access a field
  (.-height r)
)

; set a field
(set! (.-width r) 10)
(println (.-width r) (.-height r))

; invoke multiple methods on a Java object
(def my-map
  (doto (java.util.HashMap. )
    (.put "one" 1)
    (.put "two" 2)
    (.put "three" 3)
  ))
(println my-map)

; catch all exceptions from a Java method
(defn my-wrapper [f]
  (try
    (f)
    (catch ArithmeticException e (println "Arithmetic exception occurred."))
    (catch Exception e (println (str "Other exception occurred: " (.getMessage e))))
    (finally (println "finishing..."))
  ))

(my-wrapper #(throw (Exception. "some error")))
(my-wrapper #(/ 10 0))
(my-wrapper #(Integer/parseInt "a"))

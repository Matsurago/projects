(def values [7 6 3 5 9 1])
(def nums [1 2 3])

; map / filter / reduce
(println 
  (map #(* % 2) values)
  (map #(* %1 %2) values nums)  ; can multiple input seqs!

  (filter #(< % 5) values)
  (reduce * 1 values)
  (reduce * values)  ; will use the 1st element of a seq
)

; function composition
(def square-str 
  (comp #(str "val-" %) #(* % %)))

(println (map square-str values))

; for is similar to map (see list_comprehensions.clj):
(println 
  (for [v values] (* v 2))  ; returns a seq (14 12 6 10 18 2)
)

; reversing order of nested calls
(println
  ; this line...
  (apply + (filter #(> % 10) (map #(* % 2) values)))
  ; is equivalent to
  (->>
    values
    (map #(* % 2))
    (filter #(> % 10))
    (apply +)
  )
)

; complement gives a function that has the opposite truthy value
(def non-zero? (complement zero?))

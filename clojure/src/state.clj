(def counter (atom 0))

(defn update-counter []
  (swap! counter inc)  ; can also pass args: (swap! counter my-inc 10)

  (when (= 3 @counter) 
    (println "Counter was incremented three times.")))

(update-counter)
(update-counter)
(update-counter)

; manage multiple states at once: use ref
(def visitors (ref #{}))
(def counter (ref 0))

(defn new-visitor [name] 
  (dosync 
    (alter visitors #(conj % name))
    (alter counter inc)
  ))

(new-visitor "Vasya")
(new-visitor "Olya")
(println @visitors @counter)

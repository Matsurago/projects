; declare a new namespace and make this namespace default
(ns app)

; all new vars will be bound to this namespace
(def app-version "1.0.0")

; if an existing namespace is supplied, this namespace becomes
; default, and a new namespace is not created
(ns user)  ; back to the default namespace 

; to use a var from a different namespace, use fully-qualified name:
(println app/app-version)

; load a namespace from a library
(require 'clojure.data)
; load a namespace using an alias
(require '[clojure.data :as d])

; now can use functions from the namespace
(println 
  (clojure.data/diff [1 2 3] [3 2 1])
)

; when defining namespace in a .clj file, the file and directory names
; must match the namespace name:
; ./clojure/data_ext.clj -> clojure.data-ext   namespace

; define a namespace loading another namespace:
(ns app
  (:require 
    user           ; without an alias
    [user :as u]   ; using an alias
  )
)

; pull a var from other namespace directly
; very dangerous: DO NOT USE.
(require '[clojure.data :refer [diff]])

; can now use diff directly
(println (diff [1 2] [2 3]))

; get current namespace object
(println "Current namespace:" *ns*)

; find a namespace and all symbols in it
(binding [*print-length* 10]
  (println (ns-map (find-ns 'user)))  ; also (ns-map 'user)
)

; define a keyword in a namespace to avoid collisions
(println
  :user/some-keyword
  ::some-keyword  ; a shortcut for the current namespace
)

; stuff like prinln is defined in the ns clojure.core
; by default, the following is ran:
; (require '[clojure.core :refer :all])

; require a namespace forcing a reload (useful in development)
(require :reload 'clojure.data)
; to exclude some long-running fn from reload
; use defonce instead of defn 

; remove a var from a namespace
; useful when a function name is changed to remove the old name from ns
; useful to force reload of vars created with defonce 
(ns-unmap 'app 'app-version)


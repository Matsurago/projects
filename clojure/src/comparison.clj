(def r 71)

(println 
  (if (= (quot r 2) 0) 
    "even"
    "odd"
  )

  (= "value" "value")  ; same as equals() in Java
  (= 7 7 7 7)  ; any number of arguments
  (not= 7 7)   ; not equals

  ; 'and', 'or' are short-circuited
  (or true (= (/ 7 0) 1))

  ; nil is also evaluated as false (no other keyword or value is false)
  (if nil "value" "default-value")

  ; all other values evaluate to true
  (and true 100 200)      ; returns 200 (sic!)
  (or false nil "x" "y")  ; returns "x"
)

; can group multiple statements with 'do'
(println
  (if nil 
    "value"
    (do 
      (println "returning default value:")
      "default-value")))

; 'when' does not support else branch, but allows omitting do
; note: when the condition is false, returns nil
(when true 
  (println "first") (println "second") (println "third")
)

(when-not false (println "fourth"))

; 'cond' is like switch in Java
(def w 490)
(println 
  (str "Light " w "nm is:")
  (cond
    (< w 400) "ultraviolet"
    (and (>= w 400) (<= w 700)) "visible"
    (> w 700) "infrared"  ; :else "infrared"
  )
)
q
; 'case' is simplified 'cond', all case values must be constants
(def error-code 401)
(println
  (str "Error code " error-code ":")
  (case error-code
    400 "client error"
    401 "unauthorized"
    403 "forbidden"
    404 "not found"
    "unknown error"  ; otherwise (can omit)
  )
)

; check type
(println
  (number? 2021)  ; true
  (string? "test")
  (keyword? :t)
  (map? {:x 6 :y 8})
  (vector? [1 2 3])
)

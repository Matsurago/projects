(def values (take 100 (iterate inc 1N)))

(defn sum-values [cl] (apply + cl))
(defn product-values [cl] (apply * cl))

(let [
  result-1 (promise)
  result-2 (promise)
  ]

  (.start (Thread. #(deliver result-1 (sum-values values))))
  (.start (Thread. #(deliver result-2 (product-values values))))

  (println "Result 1 (sum) = " @result-1)  ; @ is same as deref
  (println "Result 2 (product) = " @result-2)
)

; the same can be done with future, but more easily
(def result-3 
  (future (apply - values)))

(println "Result 3 (minus) = " @result-3)

; it is better to provide a timeout and the default value
(def result-4
  (future (do
    (Thread/sleep 1000)
    (apply + values))))

(println "Result 4 = " (deref result-4 10 -1))

; recur: tail-recursive function optimization
; to avoid StackOverlowError
(defn print-seq [s]
  (when (seq s)  ; seq returns nil on empty collection
    (print (first s) "")
    (recur (rest s))))

; recur when we need to pass additional arguments (using multi-arity fn)
(defn sum-upto
  ([n] (sum-upto n 0 0))
  ([n i acc] 
    (if (> i n)
      acc
      (recur n (+ i 1) (+ acc i))  ; recur replaces sum-upto
    )))

; multi-arity function can be avoided with 'loop'
(defn sum-upto-v2 [n]
  (loop [n n i 0 acc 0]  ; n -> n, i -> 0, acc -> 0 
    (if (> i n)
      acc
      (recur n (+ i 1) (+ acc i))  ; recur replaces sum-upto
  )))

; create a lazy seq
; under the hood a lambda is created that delays the evaluation
(def lazy-nums (lazy-seq [1 2 3]))

; repeat, cycle, and iterate produce infinite lazy seq:
(println
  (take 5 (repeat 1))
  (take 5 (cycle [1 2 3]))
  (take 5 (iterate inc 1))
)

; lazy functions:
; take, map, interleave, for

; undo lazyness: force access each element
(doall lazy-nums)  ; returns the entire sequence (makes it eager)
(doseq [n lazy-nums]  ; same as doall, but doesn't hold entire seq 
  (println n))        ; replaces for (which is lazy)

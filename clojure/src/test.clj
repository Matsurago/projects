; run tests after loading this file
; (clojure.test/run-tests 'test-example)

; run tests in the current namespace
; (clojure.test/run-tests *ns*)  or just  (clojure.test/run-tests)

(ns test-example
  (:require [clojure.test :refer :all]))

(deftest test-first-nullability
  (is (not (nil? (first [9 8 7]))))
  (is (nil? (first [])))
)

(deftest test-first
  (is (= 9 (first [9 8 7]))))

; can organize tests into groups
(deftest test-rest
  (testing "Null and empty"
    (is (= '() (rest nil)))
    (is (= '() (rest [])))
    (is (= '() (rest [6])))
  )
  (testing "Normal case"
    (is (= '(7) (rest [8 7])))
    (is (= '(7 6) (rest [8 7 6])))
  )
  (testing "Returned type"
    (is (= clojure.lang.PersistentVector$ChunkedSeq (class (rest [7 8]))))
    (is (= clojure.lang.PersistentList (class (rest '(7 8)))))
    (is (= clojure.lang.APersistentMap$KeySeq (class (rest #{7 8}))))
    (is (= clojure.lang.PersistentArrayMap$Seq (class (rest {:f 7 :s 8}))))
  )
)

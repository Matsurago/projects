; throw an exception
(defn k-to-c [value]
  (when (< value 0) 
    (throw 
      (ex-info 
        "Value must be positive"
        {:value value})))

  (- value 273.15)       
)

(println 
  (k-to-c 320)

  (try  ; returns value from catch on exception (println -> nil)
    (k-to-c -10)

    (catch ArithmeticException e (println "Math issue."))
    (catch clojure.lang.ExceptionInfo e 
      (println "TODO: get exception message and data")))
)

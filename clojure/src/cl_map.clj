(def numbers {"one" 1 "two" 2 "three" 3})  ; a map
(def person {:name "Serj" :age 30})  ; a map that imitates object

; create a map using function 'hash-map'
(def other-numbers 
  (hash-map "four" 4 "five" 5)
)

(println
  (get numbers "two")  ; get a value from map
  (numbers "two")      ; same, using the map name as a function!
  (numbers "x")        ; nil for a non-existing value

  (person :name)       ; keywords are often used as keys
  (:name person)       ; same, using the keyword as a function!

  (contains? person :age)  ; check if the key exists
  (count person)       ; map size

  (seq person)  ; map to a sequence of key/value pairs
  (seq {})      ; seq on empty map (or other collection) gives nil!
)

; add a key/value pair to the map (returns a new map)
(println (assoc {} :key "value"))
; remove a key/value pair from the map (returns a new map)
(println (dissoc {:key "value"} :key))
; update a value in the map (returns a new map)
(println (update {:key "value"} :key "updated-value"))

; get a copy of the map adding new values
(def upd-person (assoc person 
  :tel "03-9999-999"
  :address "Rakshata Street 7-2"
))

(println (:address upd-person))

; remove a key/value pair from the map
; non-existing keys are ignored
(def upd-person-2 (dissoc person :age :middle-name))

(println
  (keys upd-person)  ; map keys (order is not guaranteed)
  (vals upd-person)  ; map values
)

; a nested map
(def my-book {
  :name "Carrots"
  :author "C.Rabbit"
  :sold {
    :where "Forst Depths"
    :copies 100
  }
})

; update a nested map
(def upd-book 
  (update-in my-book [:sold :copies] inc)
)

(def nums [1 2 3])  ; a vector
(def same-nums (vector 1 2 3))  ; same using function 'vector'
(def yet-same-nums (vec (range 1 4)))  ; vector from another collection
(def ints (vector-of :int 1 2 3))  ; vector of primitives (:int, :char, etc.)

(println
  (count nums)  ; =length
  (empty? nums) ; =null
  (first nums)  ; =head
  (rest nums)   ; =tail  (note: returns a sequence)
  (rest [])     ; empty sequence, no exception thrown
  (nth nums 2)  ; n-th item, =!!
  (nums 2)      ; same, can call a vector as a function (wow!)

  (conj nums 4) ; =++   [1 2 3 4]  may be fast if capacity allows
  (cons 4 nums) ; =:    (4 1 2 3)  slow
)

; update a value in the vector (returns a new vector)
(def upd-nums (assoc nums 1 5))  ; nums[1] = 5
(println (upd-nums 1))

; add a vector and another collection (returns a new vector)
; first arg must be a vector
(println (into same-nums '(4 5 6)))

; comparison pitfalls
; seqeuntial collections are compared by value:
(println 
  (= [1 2 3] '(1 2 3))  ; true
  (= [1 2 3] #{1 2 3})  ; false, set is not sequential
)

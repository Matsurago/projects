; declare and call an anonymous function
(println 
  ((fn [x] (* x x)) 3)  ; 
  (#(* %1 %1) 3)        ; same, short notation (%1 = first arg)
  (#(* % %) 3)          ; same, for functions with one argument only
)

; return a function (closure)
(defn less-than? [limit]
  (fn [x] (< x limit))
)

(def small-num? (less-than? 10))
(println
  (small-num? 5)
  (small-num? 10))

; apply a fn to the arguments
(def greeting (apply str ["hello" " " "user" "!"]))  

; partial application
(def add-two (partial + 2))

; 'complement' returns a fn that is a boolean negation of another fn
(def not-small-num? (complement small-num?))

; 'every-pred' returns a fn that and-combines predicates
(def rly-small? (every-pred small-num? (less-than? 5)))

(println
  greeting     ; hello user!
  (add-two 3)  ; 5
  (not-small-num? 7)  ; false
  (rly-small? 2)      ; true
)


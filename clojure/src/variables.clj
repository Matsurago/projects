; define a variable
(def greeting "hello")

; define a function
(defn say-hello [name]  ; use [] if it takes no parameters
  (println (str greeting " " name "!"))    
)

; call the defined function
(say-hello "Aervin")


; a function with multiple parameters
(defn avg [a b]  ; note: no comma
  (/ (+ a b) 2.0)
)

; call the defined function with multiple parameters
(println (avg 10 7))

; local name binding
(defn final-price [price discount min-price]
  (let [
    interm-price (* (- 1.0 discount) price)
    ;; can bind more variables in this block
  ] (if (< interm-price min-price) min-price interm-price))
)

; conditional (not nil) local name binding
(defn author-of [book]
  (if-let [author-name (:author book)]  ; also 'when-let'
    (.toUpperCase author-name)
    "Anonymous"
  )
)

(println 
  (author-of {:author "Samuel" :title "Flowing Tiger"})
  (author-of {:title "Lost Expedition Notes"})
)

; symbols (variable names) are first-class values
(def isbn-length 13)

(println 
  'isbn-length  ; print the symbol itself, not the associated value

  #'isbn-length ; print the binding (var, symbol/value association)
  (.get #'isbn-length)  ; get value from var
  (.-sym #'isbn-length) ; get symbol from var
)

; binding allows to rebind the value temporarily
; but this works only for dynamic vars
(def ^:dynamic *log-enabled* false)  ; make the var dynamic
                                     ; *..* is a convention
(binding [*log-enabled* true]
  (println (if *log-enabled* "log on" "log off"))
  ; set! can be used to change the value again inside the binding
  (set! *log-enabled* false)
  (println (if *log-enabled* "log on" "log off"))
)

; get a metadata from a var
(defn some-f []
  "Some function"
  (println "hello"))

(println (meta #'some-f))

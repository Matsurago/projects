; seq is a common interface for sequential containers
; most of Java and Clojure collections implement that

; other collections can be cast to seq
(println 
  (seq [1 2 3])
  (seq {:a 1 :b 2})
  (seq [])   ; seq on empty collection returns nil !
  (seq nil)  ; seq on nil also returns nil
)

(def values [3 1 2 5 4])  ; this is a vector...

; but all these functions return a seq 
(println 
  (rest values)    ; returns a seq, not a vector!
  (take 2 values)  ; also seq
  (map #(* 2 %) values)  ; also seq
)

; same is for sort and reverse, as well as for other functions below:
(println
  (sort values)    ; a seq, not a sorted vector
  (reverse values)
)

(println
  ; split a seq into N parts, skips last elements that don't fit
  (partition 2 values)  ; ((3 1) (2 5))
)

(println
  ; combine sequences
  (interleave '("a" "b") '(1 2))  ; (a 1 b 2)

  ; add a separator between items: (cat and mouse and hamster)
  (interpose "and" '("cat" "mouse" "hamster"))  
)

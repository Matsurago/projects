; lazy, produces a value
(def values
  (for [
      x [100 200 300]     ; for (var x : List.of(100, 200, 300))
      y [1 2 3 4 5 6 7]   ;   for (var y : List.of(1, 2, 3, 4, 5, 6, 7))
      :when (odd? y)
    ]
    (+ x y)))

(println values)

; eager, used for side-effects
(doseq [
    x [100 200 300]
    y [1 2 3 4 5 6 7]
    :when (odd? y)
  ]
  (print (+ x y) ""))

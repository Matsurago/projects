(println
  (/ 8 3 7)  ; get ratio (8/21)
  (quot 8 3)  ; =div (2)
  (/ 8.0 3.0)  ; 2.667
)

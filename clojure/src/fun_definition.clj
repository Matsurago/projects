; overloading (multi-arity function)
(defn greet
  ; a docstring comment
  "Greets a specific or generic user."

  ([] (greet "user"))  ; can call the second form of itself
  ([name] (println (str "hello, " name "!")))
)

; vararg function
(defn run [& args]  ; space between & and parameter name is required
  (println "args:" args))

(run "avg" 7 13)
(run)  ; args will be nil

; vararg function with the first parameter required
(defn run-a [name & args]
  (println "cmd name:" name "args:" args))

; a multimethod and its dispatch method (a polymorphic method)
(defn dispatch-data-type [data]
  (cond 
    (vector? data) :vector-data
    (string? data) :string-data
    (number? data) :number-data
  ))

(defmulti normalize-data dispatch-data-type)

(defmethod normalize-data :vector-data [data]
  data)

(defmethod normalize-data :number-data [data]
  [data])

(defmethod normalize-data :string-data [data]
  [(Integer/parseInt data)])

(defmethod normalize-data :default [data]
  (println "warning: unknown data format")
  []
)

; can now invoke it with different data formats
(println 
  (normalize-data [1 2 3])
  (normalize-data 4)
  (normalize-data "5")
  (normalize-data #{6 7 8})
)

; DANGER - a mutable collection
(def arr (into-array [1 2 3]))

; modifies collection in place
(aset arr 2 99)  ; arr[2] = 99
(println (seq arr))

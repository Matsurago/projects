(def sentence "A white cat sits on the bench.")

(println 
  (re-matches #".*cat.*" sentence)  ; check match

  (re-seq #"\w+" sentence)  ; match and split to (a lazy) seq
)

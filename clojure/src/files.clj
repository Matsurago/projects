(require '[clojure.java.io :as io])

; read a file line by line
(with-open [reader (io/reader "cl_list.clj")]
  (println (line-seq reader))
)

; read entire file into a String
(println
  (slurp "cl_lazy.clj")
)

; write a file
(spit "temp.txt" "line1\nline2")

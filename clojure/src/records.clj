; records correspond to classes in Java
; should be the first choice when modeling domain entities
; data exposed in public API should be modelled using maps instead

; define a record
(defrecord Phone [os version screen])

; create an instance (field order should match)
(def my-phone (->Phone "Android" "9.1" 5.5))

; create an instance using a map
(def my-ipad (map->Phone {:os "iOS" :version "14.0" :screen 11.0}))

; records are maps, so all usual functions work
(println (:os my-phone) (count my-ipad))  ; "Android" 3

; can implement type-based polymorphic behavior with protocols
(defprotocol Animal
  (greet [this message])  ; this must be the first parameter
)

(defrecord Cat [name tail-length]
  Animal  ; implements Protocol
  (greet [this message] (str message " I'm " name ". Meow!"))
)

(defrecord Dog [nickname paw-size]
  Animal
  (greet [this message] (str message " I'm " nickname ". Woof!"))
)

(println (greet (->Cat "Barsik" 10) "Hi!"))
(println (greet (->Dog "Spot" 3) "Hello!"))

; can add protocol to existing types
(defrecord PC [main-os version])

(defprotocol Device 
  (status [this]))

(extend-protocol Device
  Phone
    (status [this] (str (:os this) " " (:version this) " " (:screen this) "\""))
  PC
    (status [this] (str (:main-os this) " " (:version this)))
  ; or even for a non-record type!
  String
    (status [this] this)
)

(println (status my-ipad))
(println (status (->PC "Windows" "10")))
(println (status "An unknown device."))

; another way to add a protocol to existing type
(defrecord TV [is-smart?])
(extend-type TV
  Device
  (status [this] (if (:is-smart? this) "A smart TV." "A common TV without OS."))
)

(println (status (->TV true)))

(def nums '(1 2 3))  ; a list
(def same-nums (list 1 2 3))  ; same using function 'list'

; pitfall: quote prevents inner expr evaluation
(println 
  '(1 (+ 2 3))  ; outputs (1 (+ 2 3)), not (1 5)
  `(1 ~(+ 2 3)) ; syntax-quote` and unquote~ combination gives (1 5)
)

(println
  (count nums)  ; =length
  (first nums)  ; =head
  (rest nums)   ; =tail  (note: returns a sequence)
  (rest [1])    ; empty sequence (), no exception thrown
  (next [1])    ; same as rest, but returns nil instead of ()
  (nth nums 2)  ; n-th item, =!!

  (conj nums 4) ; =:    (4 1 2 3)  this is different from vector
  (cons 4 nums) ; =:    (4 1 2 3)
)

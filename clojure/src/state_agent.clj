; using atom and ref are dangerous when there are side effects
; because dosync and swap! may be called multiple times
; use agents instead
(def orders (agent {}))

(defn add-order [{:keys [id customer]}]
  (send orders (fn [orders-map]  ; send is async unlike atom/ref
    (Thread/sleep 500)
    (println "Notification sent.")
    (assoc orders-map id customer)
  )))

(add-order {:id 10 :customer "Acme Inc."})
(println @orders)  ; orders wont be updated yet

; when a error occurs the agent goes into error state
; immediately rejecting all calls to send.
; it should be restarted:
(when (agent-error orders)
  (restart-agent orders {} :clear-actions true))

; to properly end JVM when there are agents running
; can be the last command in -main
; (shutdown-agents)  

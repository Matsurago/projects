# Clojure

## Useful commands

Start the REPL:
```
lein repl
```

Create a new app:
```
lein new app appName
```

## Useful REPL commands
Load a file:
```
(load-file "filename.clj")
```

Show a docstring for a function:
```
(doc function-name)
```

Show the object type:
``` 
(class my-obj)
```

Use `*1` to get the last result from the REPL.
Second to last will be `*2`, and so on. The last exception is `*e`.

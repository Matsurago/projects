// a function definition
let greet name age =
    // string interpolation
    $"Hello, {name}. You are {age} years old."

// call the function
let alexGreeting = greet "Alex" 18

// a function definition with explicit type annotations
let describePerson (age: int) : string =
    if age < 1 then "toddler"
    elif age < 18 then "child"
    else "adult"

// a function definition with explicit type annotations for generic parameters
let combine (a: 'a) (b: 'a) (c: 'a) : ResizeArray<'a> =
    let res = ResizeArray()
    res.Add a
    res.Add b
    res.Add c
    res

// call the generic function
let nums = combine 1 2 3
let strs = combine "a" "b" "c"

// a const - never changes once assigned
let currentTime = System.DateTime.Now
// a function that returns new value each time it's called
let getCurrentTime () = System.DateTime.Now

// a higher-order function
let minBy_ (compare: ('a -> 'a -> int)) (a: 'a) (b: 'a): 'a =
    if compare a b <= 0 then a
    else b

// pass a lambda
minBy_ (fun a b -> if a < b then -1 elif a > b then 1 else 0) 23 32

// pipeline operator |> 
// a |> f = f a  (a becomes the LAST argument of f)
let square_ x = x * x
let sum_ x y = x + y
let print_ x = printfn $"result: {x}"

3 
    |> square_
    |> sum_ 5
    |> print_ 


// a recursive function
// use [<TailCall>] attribute for tail recursion
let rec factorial n =
    if n < 1 then 1
    else n * factorial (n - 1)

factorial 5

// function composition
let f x = x + 1
let g x = x * 2

let h = f >> g  // g (f x)
let i = f << g  // f (g x)

let res1 = h 5  // 12
let res2 = i 5  // 11

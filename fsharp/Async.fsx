open System.IO
open System.Threading.Tasks

// async functions in .NET have ~Async suffix.

// async basics
let writeToFileAsync filename text = task {
    // await, unit result
    do! File.AppendAllTextAsync(filename, text)
    
    // await, result 'a
    let! data = File.ReadAllTextAsync filename

    // 'return' lifts the result value onto the monad (here: task)
    return data.Length    
}

// read 3 files sequentially in async block
let t1 = task {
    let! a = File.ReadAllTextAsync "Functions.fsx"
    let! b = File.ReadAllTextAsync "Options.fsx"
    let! c = File.ReadAllTextAsync "Records.fsx"

    return $"{a} {b} {c}"
}

// read 3 files in parallel
let t2 = task {
    let! data = 
        ["Functions.fsx"; "Options.fsx"; "Records.fsx"]
        |> List.map File.ReadAllTextAsync 
        |> Task.WhenAll
    
    // concat text from 3 files
    return data
        |> Array.reduce (sprintf "%s %s")
}

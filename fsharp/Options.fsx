open System

// option is an or-type in F#
// usage as in other languages -> closer to Java

let loadTextFile (filename: string): string option =
    if IO.File.Exists filename 
    then Some (System.IO.File.ReadAllText filename)
    else None

let countWords (text: string): int =
    Text.RegularExpressions.Regex("\\s").Split text 
    |> Array.length

let countWordsInFile (filename: string): int option =
    loadTextFile filename
    |> Option.map countWords

let numWords = countWordsInFile "Basics.fsx"

// Option.bind is flatMap()
// unwraps string option option -> string option
Some "Collections.fsx"
    |> Option.bind loadTextFile


// Option.defaultValue is orElse()
let x = None |> Option.defaultValue 0

// Option.iter is forEach()
Some 42 
|> Option.iter (fun value -> printfn $"value is {value}")


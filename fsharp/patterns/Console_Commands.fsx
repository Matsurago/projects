open System

let inputs: string seq =
    seq {
        while true do
            Console.Write("Enter command: ")
            Console.ReadLine()
    }


let isNotExit cmd = cmd <> "exit"


inputs
    |> Seq.takeWhile isNotExit
    // ... more pipeline functions ...

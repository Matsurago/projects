open System

// Treat timer events as a sequence

let isEvenSecond (args: Timers.ElapsedEventArgs): bool =
    args.SignalTime.Second % 2 = 0

let printTime (args: Timers.ElapsedEventArgs): unit =
    printfn $"Event at {args.SignalTime}"

// raises event every second
let timer = new Timers.Timer(Interval = 1000, Enabled = true)

timer.Elapsed 
    |> Event.filter isEvenSecond
    |> Event.add printTime

timer.Start()

// raw string
let raw = @"c:\users\user"

// big integer
let amount = 123456789123456789123I

// hex, binary; can use _ to separate digits
let codes = 0xff, 0b1001_1110_1111

// a substring
let hello = "hello world!"
let world = hello[6..]

// string format specifiers
// NOTE: printfn outputs a new line, printf doesn't
printfn $"%.3f{System.Math.E}"  // or
printfn $"{299792.458:N3}"      // .NET style
printfn $"0x%08x{65532}"

// debug output
let nums = seq { 1, 2, 3 }
printfn $"%A{nums}"


// compile-time constant; can be used:
// a) in pattern matching without 'when'
// b) in attribute arguments [<Attr(arg, ..)>]
// c) type provider arguments
[<Literal>]
let thisScript = __SOURCE_DIRECTORY__ + "\\" + __SOURCE_FILE__


// nested scopes
let greetingText =
    // every level of indent creates a new scope
    let fullName =
        let fname = "Frank"
        let sname = "Shmidt"
        $"{fname} {sname}"  // every scope returns a value
    $"Greetings, {fullName}"


// pattern matching
let dept = "Accounting"
let building =
    match dept with
    | "Securtiy"
    | "Engineering" -> 
        "A"
    | "Accounting"
    | "Headquaters" 
    | "Library" ->
        "B"
    | "Storage Depot" -> "C"
    | _ -> "D"


// pattern matching on lists
type Request = { From: string; Urgent: bool }

let describeRequests (requests: Request list): string =
    match requests with
    | [] -> "No requests"
    | [ { Urgent = true } ] -> "single urgent request"
    | [ { Urgent = true }; { Urgent = true } ] -> "two urgent requests"
    | { Urgent = false } :: rest -> $"First request is normal priority, rest: {rest}"
    | _ :: { Urgent = true } :: _ -> "second request is urgent"
    | _ -> "other"

type Game = {
    HomeTeam: string
    HomeTeamGoals: int
    AwayTeam: string
    AwayTeamGoals: int
}

let makeGame ht htg at atg = 
    { HomeTeam = ht; HomeTeamGoals = htg
      AwayTeam = at; AwayTeamGoals = atg }

let isAwayTeamWin (game: Game): bool = 
    game.AwayTeamGoals > game.HomeTeamGoals

let games = [
    makeGame "Messiville" 1 "Ronaldo City" 2
    makeGame "Messiville" 1 "Bale Town" 3
    makeGame "Ronaldo City" 2 "Bale Town" 3
    makeGame "Bale Town" 2 "Messiville" 1
]

// which team won the most away games?
let wonAwayTheMost = 
    games 
    |> List.filter isAwayTeamWin
    |> List.countBy (fun game -> game.AwayTeam)
    |> List.maxBy (fun (_, count) -> count)

// how many games did Ronaldo City feature in?
let numRonaldoCityGames =
    games
    |> List.filter (fun game -> game.HomeTeam = "Ronaldo City" || game.AwayTeam = "Ronaldo City")
    |> List.length

// which team scored the most goals?
let mostGoalsTeam = 
    games
    // this is flatMap
    |> List.collect (fun game -> [game.HomeTeam, game.HomeTeamGoals; game.AwayTeam, game.AwayTeamGoals])
    |> List.groupBy (fun (team, _) -> team)
    |> List.map (fun (team, teamAndGoals) -> team, List.sumBy (fun (_, goals) -> goals) teamAndGoals)
    |> List.maxBy (fun (_, goals) -> goals)

// create a simple list
let numbers = [1; 2; 3]
let moreNumbers = [4 .. 10]  // using a range
let allNumbers = numbers @ moreNumbers  // append two lists
let upToFive = allNumbers[0 .. 4]  // a sublist

// array
let words = [| "cat"; "mouse"; "hamster" |]
// is mutable
words[2] <- words[2] + "!"

// Split() also returns an array
match "1000,username,rw".Split "," with
    | [| id; name; access |] -> Ok $"user: {name}, id: {id}, access: {access}"
    | _ -> Error "Wrong record format."

// sequence - lazy
// evaluated each time when it's used
let names = seq { 
    "Jader Tryce" 
    "William Borning"
    // can have conditional elements
    if System.DateTime.Now.Year = 2024 then "Mr. President!"
}

// hash map (aka dictionary) - mutable - from C#
let codes = readOnlyDict [404, "not found"; 403, "Access Denied"; 401, "Unauthorized"]
let errorDescription = codes[404]

// hash map - immutable
// each add operation returns a new Map
let moreCodes = Map [200, "OK"; 201, "Created"]
let moreCodes2 = moreCodes.Add(301, "Redirect")
let moreCodes3 = Map.add 400 "Client-Side Error" moreCodes2

// set
let xs = Set [1; 2; 3]
let ys = Set [3; 4; 5]
let diff = xs - ys
let union = xs + ys





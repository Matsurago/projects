type Address = {
    Country: string
    City: string
    AddressLine: string
}

type Person = {
    FirstName: string
    LastName: string
    Age: int
    Address: Address
}

let john = {
    FirstName = "John"
    LastName = "Smith"
    Age = 23
    Address = {
        Country = "Japan"
        City = "Chiba"
        AddressLine = "Nishi-Chiba 1A"
    }
}

// access record values
let home = john.Address.AddressLine

// copy and update a record
let johnNextYear = {
    john with
        LastName = "Smither"
        Age = 24
}

// anonymous record
let transfer = {|
    To = john
    Date = System.DateTime.Now
    Amount = 3200
|}

// can pattern match on record fields
type Customer = {
    LoyaltyYears: int
    HasOverdraft: bool
    Overdraft: int
}

let canTakeLoan (customer: Customer): bool =
    match customer with
    | { LoyaltyYears = 0} -> false
    | { LoyaltyYears = 1; HasOverdraft = true } when customer.Overdraft > 500 -> false
    | { LoyaltyYears = 1 | 2 } -> true  // can match against multiple values
    | _ -> true

// can avoid 'when' using this pattern:
let canTakeLoan2 (customer: Customer): bool =
    let hasLargeOverdraft = customer.Overdraft > 500
    match customer, hasLargeOverdraft with
    | { LoyaltyYears = 0 }, _ -> false
    | { LoyaltyYears = 1; HasOverdraft = true }, true -> false
    | _ -> true

// Or-types
type ContactMethod =
    | Email of address: string
    | Telephone of country: string * number: string
    | Post of 
        {|
            Line1: string
            Line2: string
            City: string
            Country: string
        |}

let johnsContact = Telephone ("JP", "080-0000-0000")

// pattern match for or-types
let contactMethodString method =
    match method with
    | Email address -> $"E-mail: {address}"
    | Telephone (country, number) -> $"Tel: {number} ({country})"
    | Post address -> $"{address.Line1} {address.Line2}"

// single-cased or-types - can be used as wrappers around primitive types
type CustomerId = CustomerId of int
// create an instance of a wrapper type
let olegId = CustomerId 1000
// quickly get a value from a wrapper
let (CustomerId olegIdValue) = olegId

// can also unwrap in function's signature:
let searchCustomer (CustomerId id) = 
    failwith $"cannot find customer with id = {id}"

// single-cased or-types can wrap a tuple;
// in this case, it's possible to give names to tuple elements
type UkDistance = UkDistance of miles: int * yards: int


// Pattern: enforcing invariant in a single-cased or-type
type ValidatedEmail =
    private | ValidatedEmail of string

    // get wrapped value
    member this.Value =
        match this with (ValidatedEmail email) -> email 

    // a "constructor" that enforces the invariant
    static member Create (email: string): Result<ValidatedEmail, string> =
        if email.Contains "@" then
            Ok (ValidatedEmail email)
        else 
            Error "Incorrect email format."

ValidatedEmail.Create "user@test.com"

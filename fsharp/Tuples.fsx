// tuple creation
let t = "Tokyo", 32, true  // type is string * int * bool

// tuple destrucruring
// can omit some 
let city, pos, _ = t

// can destructure tuples in function signatures
let foo (_, position, ans) =
    failwith "not implemented"

// NOTE: out parameters in C# become tuple results in F#
let parseOk, res = System.DateTime.TryParse "25 Dec 2020"
printfn $"parse succeeded: {parseOk}, result: {res}"

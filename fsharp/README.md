# F# Code Snippets

## Commands

New project (console template)
```
dotnet new console -lang F# -o ProjectName
```

List all templates
```
dotnet new -l
```

Run app
```
dotnet run
```

Run REPL
```
dotnet fsi
dotnet fsi FileName.fsx
```

Add reference to another project in the solution to use classes from there:
```
dotnet add ./ThisProject/ reference ./LibraryProject/
```

Add a dependency:
```
dotnet add package PackageName  // use latest
dotent add package PackageName --version Version
```

## Links
- [F# Software Foundation](https://fsharp.org/) (seems not to be well maintained)

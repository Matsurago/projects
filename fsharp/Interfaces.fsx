// usually only used for C# interop

type Greeter =
    abstract Greet: string -> string
    abstract BidFarewell: string -> string

// impl
type BasicGreeter () =
    interface Greeter with
        member _.Greet name = $"Hello, {name}!"
        member _.BidFarewell name = $"Best regards, {name}!"


// need to explicitly specify the type
let greeter: Greeter = BasicGreeter()
greeter.Greet "Andrew"


// create an instance of interface without defining a type
let anonymousGreeter = 
    { new Greeter with
        member _.Greet name = $"Isn't it {name}?!"
        member _.BidFarewell name = $"Bye-bye, {name}!"
    } 

anonymousGreeter.Greet "Vera"

open System.IO

let findScripts () =
    let fileInfo = FileInfo __SOURCE_FILE__
    let dirInfo = fileInfo.Directory
    
    dirInfo.GetFiles()
    |> Array.filter (fun file -> Path.GetExtension file.Name = ".fsx")
    |> Array.map (fun file -> file.Name)


let scripts = findScripts ()

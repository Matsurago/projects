open System

// shuffle an array
let shuffle (xs: 'a array): 'a array = 
    let rand = Random()
    xs |> Array.sortBy (fun x -> rand.Next())


let nums = [| 1 .. 12 |]
let res = shuffle nums

// it's F#'s Either

type ValidationError = 
    | MissingName
    | NameTooLong of int

let validateUsername (name: string): Result<string, ValidationError> =
    if name.Length = 0 then Error MissingName
    elif name.Length > 32 then Error (NameTooLong name.Length)
    else Ok name

validateUsername "vasyaHaveChoosenAveryLongAndAwesomeUsername"

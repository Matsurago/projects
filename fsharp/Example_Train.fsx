open System

type CarriageClass = 
    | First
    | Second

type BuffetType = {
    HotFood: bool
    ColdFood: bool
}

type CarriageType =
    | PassengerCarriage of CarriageClass
    | Buffet of BuffetType

type CarriageFeature =
    | Quiet
    | WiFi
    | Washroom

type Carriage = { 
    Position: int
    NumSeats: int
    Type: CarriageType
    Features: CarriageFeature Set
}

type Station = Station of string

type Stop = {
    Station: Station
    ArrivalTime: TimeOnly
}

type Train = {
    IdentificationCode: string
    Origin: Stop
    Destination: Stop
    IntermediateStops: Stop list
    DriverChangeStop: Station option
    Carriages: Carriage list
}


let totalNumSeats (train: Train) = 
    train.Carriages 
    |> List.sumBy (_.NumSeats)


let timeToTravel (origin: Station) (destination: Station) (train: Train)  =
    let stops = train.Origin :: train.IntermediateStops @ [train.Destination]

    let originSearch =
        stops 
        |> List.tryFind (fun stop -> stop.Station = origin)
    
    let destinationSearch = 
        stops
        |> List.tryFind (fun stop -> stop.Station = destination)

    match originSearch, destinationSearch with
    | Some org, Some dst -> 
        if org.ArrivalTime > dst.ArrivalTime 
        then Error "Stops must be in order of journey"
        else Ok (dst.ArrivalTime - org.ArrivalTime)
    | Some _, None -> Error "Destination stop is not found"
    | None, Some _ -> Error "Origin stop is not found"
    | None, None -> Error "Neither origin nor destination stops are found" 


let findCarriagesByFeature (feature: CarriageFeature) (train: Train): Carriage list =
    train.Carriages
    |> List.filter (fun carriage -> carriage.Features.Contains feature)


// usage example

let train = {
    IdentificationCode = "ABC123"

    Origin = {
        Station = Station "London St Pancras"
        ArrivalTime = TimeOnly(09, 00, 0)
    }
    Destination = {
        Station = Station "Paris Nord"
        ArrivalTime = TimeOnly(13, 15, 0)
    }
    IntermediateStops = [
        {
            Station = Station "Ashford"
            ArrivalTime = TimeOnly(10, 00, 0)
        }
        {
            Station = Station "Lille"
            ArrivalTime = TimeOnly(12, 00, 0)
        }        
    ]
    DriverChangeStop = Some (Station "Ashford")
    Carriages = [
        {
            Position = 1
            NumSeats = 45
            Type = PassengerCarriage First
            Features = Set [ WiFi; Quiet; Washroom ]
        }
        {
            Position = 2
            NumSeats = 65
            Type = PassengerCarriage Second
            Features = Set [ Washroom ]
        }
        {
            Position = 3
            NumSeats = 12
            Type = Buffet { HotFood = true; ColdFood = true }
            Features = Set [ WiFi ]
        }                
    ]
}

let s = totalNumSeats train
let t = train |> timeToTravel (Station "Ashford") (Station "Paris Nord")

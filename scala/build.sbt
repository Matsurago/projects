scalaVersion := "3.6.2"

name := "scala"
version := "0.1"

idePackagePrefix := Some("com.learning")

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.19" % Test

# Scala Code Snippets

## Manual Compilation
Have the `sbt` tool installed from [www.scala-sbt.org](https://www.scala-sbt.org/download/).
Confirm the version by running
```
sbt --version
```
If the version in the project is obsolete, update it by editing `project/build.properties`:
```
sbt.version = 1.10.7
```

To compile, test, and run the project:
```
sbt
> compile
> test
> run
```

package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite


class MaybeTest extends AnyFunSuite:

  test("map") {
    val x = Maybe(7)
    assert(x.map(_ + 1) == Maybe(8))

    val e: Maybe[Int] = Maybe()
    assert(e.map((x: Int) => x + 1) == e)
  }

  test("filter") {
    val x = Maybe(7)
    assert(x.filter(_ > 8) == Maybe())
    assert(x.filter(_ > 6) == Maybe(7))
  }

  test("flatMap") {
    val x = Maybe(7)
    assert(x.flatMap(x => Maybe(x + 1)) == Maybe(8))

    val e: Maybe[Int] = Maybe()
    assert(e.flatMap((x: Int) => Maybe(x + 1)) == e)
  }

  test("getOrElse") {
    assert(Maybe(8).getOrElse(5) == 8)
    assert(Maybe().getOrElse(5) == 5)
  }

  test("orElse") {
    assert(Maybe(8).orElse(Maybe(5)) == Maybe(8))
    assert(Maybe().orElse(Maybe(5)) == Maybe(5))
  }

  test("lift") {
    val x = Maybe(7)
    val f: Int => Int = _ + 1
    val g = Maybe.lift(f)

    assert(g(x) == Maybe(8))
  }

  test("lift2") {
    val x = Maybe(2)
    val y = Maybe(3)
    val f: (Int, Int) => Int = _ + _
    val g = Maybe.lift2(f)

    assert(g(x, y) == Maybe(5))
  }

  test("map2") {
    val e: Maybe[Int] = Maybe.Empty

    assert(Maybe.map2(Maybe(2), Maybe(3))(_ * _) == Maybe(6))
    assert(Maybe.map2(Maybe(2), e)(_ * _) == e)
    assert(Maybe.map2(e, Maybe(3))(_ * _) == e)
    assert(Maybe.map2(e, e)(_ * _) == e)
  }

  test("sequence") {
    val x = Node(Maybe("a"), Maybe("b"), Maybe("c"))
    assert(Maybe.sequence(x) == Maybe(Node("a", "b", "c")))

    val e = Node(Maybe("a"), Maybe("b"), Maybe(), Maybe("c"))
    assert(Maybe.sequence(e) == Maybe())
  }

  test("traverse") {
    val x = Node(1, 3, 5)
    val y = Node(1, 2, 3)
    val f: Int => Maybe[Int] = x =>
      if x % 2 == 1 then Maybe(x * 2)
      else Maybe()

    assert(Maybe.traverse(x)(f) == Maybe(Node(2, 6, 10)))
    assert(Maybe.traverse(y)(f) == Maybe())
  }

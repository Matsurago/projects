package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite

class AlgorithmsTest extends AnyFunSuite:

  test("countChange: small example") {
    assert(3 == countChange(4, List(1, 2)))
    assert(3 == countChange2(4, List(1, 2)))
  }

  test("countChange: sorted CHF") {
    assert(1022 == countChange(300, List(5, 10, 20, 50, 100, 200, 500)))
    assert(1022 == countChange2(300, List(5, 10, 20, 50, 100, 200, 500)))
  }

  test("countChange: no pennies") {
    assert(0 == countChange(301, List(5, 10, 20, 50, 100, 200, 500)))
    assert(0 == countChange2(301, List(5, 10, 20, 50, 100, 200, 500)))
  }

  test("countChange: unsorted CHF") {
    assert(1022 == countChange(300, List(500, 5, 50, 100, 20, 200, 10)))
    assert(1022 == countChange2(300, List(500, 5, 50, 100, 20, 200, 10)))
  }

package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite

class ParTest extends AnyFunSuite:
  test("parallel sum") {
    val xs = List(7, 5, 44, 12, 9, 0, 8, 4)
    assert(parSum(xs) == 89)
  }

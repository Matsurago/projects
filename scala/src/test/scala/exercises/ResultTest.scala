package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite

class ResultTest extends AnyFunSuite:

  test("map") {
    val x = Result.Right(7)
    assert(x.map(_ + 1) == Result.Right(8))

    val e = Result.Left("error")
    assert(e.map((x: Int) => x + 1) == e)
  }

  test("traverse") {
    val xs = Node("23", "47", "116")
    assert(Result.traverse(xs)(x => Result.catchNonFatal(x.toInt)) == Result.Right(Node(23, 47, 116)))

    val ys = Node("23", "47", "116", "an", "76", "bn")
    val res = Result.traverse(ys)(y => Result.catchNonFatal(y.toInt))
    res match
      case Result.Right(_) => fail("should not produce a result when there is an error in inputs")
      case Result.Left(e: NumberFormatException) => assert(e.getMessage == "For input string: \"an\"")
      case Result.Left(_) => fail("unexpected error occured")
  }

  test("sequence") {
    val xs = Node(Result.Right(1), Result.Right(2))
    assert(Result.sequence(xs) == Result.Right(Node(1, 2)))

    val ys = Node(Result.Right(1), Result.Right(2), Result.Left("error"))
    assert(Result.sequence(ys) == Result.Left("error"))
  }



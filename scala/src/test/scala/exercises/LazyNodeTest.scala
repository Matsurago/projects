package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite
import LazyNode.{cons, empty}

class LazyNodeTest extends AnyFunSuite:
  // helper func to make a calculation
  def calc(i: Int): Int =
    println(s"Calculating $i...")
    i

  // helper func to create a lazy node
  private def makeLazyNode(): LazyNode[Int] =
    val node = cons(calc(1), cons(calc(2), cons(calc(3), empty)))
    println("LazyNode created.")  // should be printed first
    node

  // tests

  test("toNode") {
    val xs = makeLazyNode()
    assert(xs.toNode == Node(1, 2, 3))
  }

  test("take") {
    val xs = cons(calc(1), cons(calc(2), cons(throw RuntimeException(), empty)))

    assert(xs.take(2).toNode == Node(1, 2))
    xs.take(3)  // no throw
    assertThrows[RuntimeException](xs.take(3).toNode)
  }

  test("drop") {
    val xs = cons(throw RuntimeException(), cons(calc(1), cons(calc(2), empty)))
    assert(xs.drop(2).toNode == Node(2))
  }

  test("takeWhile") {
    val xs = cons(calc(1), cons(calc(2), cons(calc(3), cons(throw RuntimeException(), empty))))

    assert(xs.takeWhile(_ < 3).toNode == Node(1, 2))
  }

  test("exists") {
    val xs = cons(calc(1), cons(calc(2), cons(calc(3), cons(throw RuntimeException(), empty))))

    assert(xs.exists(_ == 3))
    assertThrows[RuntimeException](xs.exists(_ == 4))
  }

  test("foldr") {
    val xs = cons(calc(1), cons(calc(2), cons(calc(3), cons(throw RuntimeException(), empty))))

    assert(xs.foldr(false)(_ == 3 || _))
    assertThrows[RuntimeException](xs.foldr(false)(_ == 4 || _))
  }

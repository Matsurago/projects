package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite
import Exercises.*

class ExercisesTest extends AnyFunSuite:
  test("fib") {
    assert(List.tabulate(10)(i => fib(i + 1)) === List(0, 1, 1, 2, 3, 5, 8, 13, 21, 34))
  }

  test("isSorted") {
    assert(isSorted(Array[Int](), _ < _))
    assert(isSorted(Array(7), _ < _))
    assert(isSorted(Array(7, 13), _ < _))
    assert(isSorted(Array(7, 13, 44), _ < _))

    assert(isSorted(Array(44, 13, 7), _ > _))
    assert(!isSorted(Array(7, 13, 44), _ > _))
  }

  test("curry") {
    def add(a: Int, b: Int): Int = a + b

    assert(curry(add)(3)(4) == 7)
  }

  test("uncurry") {
    def add(a: Int): Int => Int = b => a + b

    assert(uncurry(add)(3, 4) == 7)
  }

  test("compose") {
    def inc(a: Int) = a + 1
    def twice(a: Int) = a * 2

    assert(compose(inc, twice)(3) == 8)
  }

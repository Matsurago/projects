package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite

class NodeTest extends AnyFunSuite:

  test("make empty") {
    val e1 = Node.Nil
    val e2 = Node()

    assert(e1 == e2)
  }

  test("make") {
    // using definition
    val a = Node.Cons(1, Node.Cons(2, Node.Cons(3, Node.Nil)))
    // using apply in companion object
    val b = Node(1, 2, 3)

    assert(a == b)
  }

  test("access i-th element") {
    val a = Node(4, 5, 6)

    assert(a(0) == 4)  // using apply() method in the enum
    assert(a(1) == 5)
    assert(a(2) == 6)
  }

  test("update i-th element") {
    val a = Node(1, 2, 3)
    val b = a(2) = 7

    assert(b == Node(1, 2, 7))
  }

  test("rest") {
    val a = Node(1, 2, 3)
    assert(a.rest == Node.Cons(2, Node.Cons(3, Node.Nil)))

    val b = Node()
    assert(b.rest == Node.Nil)
  }

  test("setHead") {
    val a = Node(1, 2, 3)
    assert(a.setHead(4) == Node.Cons(4, Node.Cons(2, Node.Cons(3, Node.Nil))))

    val b = Node()
    assert(b.setHead(1) == Node.Cons(1, Node.Nil))
  }

  test("init") {
    val a = Node(1, 2, 3, 4, 5)
    assert(a.init == Node(1, 2, 3, 4))
  }

  test("size") {
    val nums = Node(1, 2, 3, 4)
    assert(nums.size == 4)

    val empty = Node()
    assert(empty.size == 0)
  }

  test("folds") {
    val nums = Node(7, 8, 9)

    assert(nums.foldl(0)(_ - _) == -24)
    assert(nums.foldr(0)(_ - _) == 8)
  }

  test("compose functions via fold") {
    val fs: Node[Int => Int] = Node(_ + 1, _ * 2, _ - 2)
    val id: Int => Int = x => x

    val f = fs.foldr(id)((a, f) => f.compose(a))
    assert(f(4) == 8)
  }

  test("foreach") {
    val nums = Node(1, 2, 3)
    var sum = 0

    nums.foreach(sum += _)
    assert(sum == 6)
  }

  test("toString") {
    val a = Node(1, 2, 3)
    assert(a.toString == "Node [1, 2, 3]")

    val e = Node()
    assert(e.toString == "Node []")
  }

  test("append") {
    val a = Node(1, 2, 3)
    val b = Node(4, 5)
    val c = Node()

    assert(a.append(b) == Node(1, 2, 3, 4, 5))
    assert(b.append(a) == Node(4, 5, 1, 2, 3))
    assert(a.append(c) == Node(1, 2, 3))
    assert(c.append(a) == Node(1, 2, 3))
  }

  test("drop") {
    val a = Node(7, 8, 9, 10)

    assert(a.drop(2) == Node(9, 10))
    assert(a.drop(0) == Node(7, 8, 9, 10))

    assert(a.dropWhile(_ < 9) == Node(9, 10))
    assert(a.dropWhile(_ == 2) == Node(7, 8, 9, 10))
  }

  test("map") {
    val a = Node(1, 2, 3)

    assert(a.map(_ + 1) == Node(2, 3, 4))
    assert(a.map(_.toString) == Node("1", "2", "3"))
  }

  test("flatMap") {
    val a = Node(1, 2, 3)

    assert(a.flatMap(x => Node(x, x * 2)) == Node(1, 2, 2, 4, 3, 6))
  }

  test("filter") {
    val a = Node(1, 2, 3, 4, 5, 6)

    assert(a.filter(_ % 2 != 0) == Node(1, 3, 5))
    assert(Node.filterViaFlatMap(a, _ % 2 != 0) == Node(1, 3, 5))
  }

  test("concat") {
    val a = Node(Node(1, 2), Node(3), Node(4, 5))

    assert(Node.concat(a) == Node(1, 2, 3, 4, 5))
  }

  test("reverse") {
    val nums = Node(1, 2, 3, 4, 5)
    assert(nums.reverse == Node(5, 4, 3, 2, 1))
  }

  test("zipWith") {
    val a = Node(1, 2, 3)
    val b = Node(4, 5, 6)

    assert(a.zipWith(b)(_ * _) == Node(4, 10, 18))
  }

  test("Exercise 3.8 - foldr") {
    val a = Node(1, 2, 3)

    // invoke foldr with acc = Nil and f = Cons
    val b = a.foldr(Node.Nil)(Node.Cons(_, _))

    assert(a == b)
  }

  test("product") {
    val a = Node(1.0, 2.0, 3.0)

    assert(Node.product(a) == 6.0)
  }

  test("hasSubsequence") {
    val a = Node(1, 2, 3, 4)

    assert(a.hasSubsequence(Node(1, 2)))
    assert(a.hasSubsequence(Node(2, 3)))
    assert(a.hasSubsequence(Node(4)))
    assert(a.hasSubsequence(Node.Nil))
    assert(! a.hasSubsequence(Node(3, 2)))
    assert(! a.hasSubsequence(Node(1, 3)))
    assert(! a.hasSubsequence(Node(2, 3, 4, 5)))
  }

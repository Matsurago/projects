package com.learning
package exercises

import org.scalatest.funsuite.AnyFunSuite
import Tree.*

class TreeTest extends AnyFunSuite:

  test("size: empty") {
    val t = Leaf
    assert(t.size == 0)
  }

  test("size: 1 node") {
    val v = Branch(1, Leaf, Leaf)
    assert(v.size == 1)
  }

  test("size: 2 nodes") {
    val v = Branch(1, Leaf, Branch(2, Leaf, Leaf))
    assert(v.size == 2)
  }

  test("size: 3 nodes") {
    //   2
    // 1   3
    val t = Branch(2, Branch(1, Leaf, Leaf), Branch(3, Leaf, Leaf))
    assert(t.size == 3)
  }

  test("size: 4 nodes") {
    //    4
    // 1     6
    //     5
    val u = Branch(4, Branch(1, Leaf, Leaf), Branch(6, Branch(5, Leaf, Leaf), Leaf))
    assert(u.size == 4)
  }

  test("fromList") {
    val tree: Tree[Int] = fromList(_ < _, 2, 1, 3)
    assert(tree == b(2, b(1), b(3)))
  }

  test("maximum") {
    val tree: Tree[Int] = fromList(_ < _, 2, 1, 3, 15, 7, 9, 0, 4, 6)
    assert(tree.maximumInt == 15)
  }

  test("depth: 0") {
    val tree = Leaf
    assert(tree.depth == 0)
  }

  test("depth: 1") {
    val tree = b(42)
    assert(tree.depth == 1)
  }

  test("depth: 2") {
    val tree = b(3, b(2), b(4))
    assert(tree.depth == 2)
  }

  test("depth: 6") {
    val tree: Tree[Int] = fromList(_ < _, 2, 1, 3, 15, 7, 9, 0, 4, 6, 2)
    assert(tree.depth == 6)
  }

  test("map: empty") {
    val tree: Tree[Int] = Leaf
    assert(tree.map(_ + 1) == tree)
  }

  test("map: int => int") {
    val tree: Tree[Int] = fromList(_ < _, 2, 1, 3, 4)
    val expected = b(3, b(2), b(4, Leaf, b(5)))
    assert(tree.map(_ + 1) == expected)
  }

  test("map: int => string") {
    val tree: Tree[Int] = fromList(_ < _, 2, 1, 3, 4)
    val expected = b("2!", b("1!"), b("3!", Leaf, b("4!")))
    assert(tree.map(_.toString + "!") == expected)
  }

  test("fold") {
    val tree: Tree[Int] = fromList(_ < _, 2, 1, 3, 4)
    assert(tree.fold(0)(_ + _ + _) == 10)
  }

  // helper methods
  private def b[T](value: T): Tree[T] =
    Branch(value, Leaf, Leaf)

  private def b[T](value: T, left: Tree[T], right: Tree[T]): Tree[T] =
    Branch(value, left, right)

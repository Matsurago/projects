package sc.study.basics

import scala.{List => SuperList}  // List is now known as SuperList


// inner packages!
package inner {

  class First
  class Second

  package tests {
    class FirstTestSuite {

      def test(): Unit = {
        val list = SuperList(1, 2, 3)
      }

    }
  }

}

package com.learning
package basics

import java.time.{LocalDate, LocalDateTime}
import scala.annotation.tailrec

object Functions:
  @main
  def main(): Unit =
    // can change the order of fn arguments
    val noon = makeDate(minute = 0, hour = 12)
    println(noon)

    println(inc(4))
    println(inc(4, 2))

    println("index = " + find(Array(1, 4, 7, 15), x => x == 7))

  private def makeDate(hour: Int, minute: Int): LocalDateTime =
    val now = LocalDate.now()
    LocalDateTime.of(now.getYear, now.getMonthValue, now.getDayOfMonth, hour, minute)

  // can specify default values
  def inc(x: Int, by: Int = 1): Int = x + by

  // a generic function
  def find[T](xs: Array[T], pred: T => Boolean): Int =
    @tailrec
    def go(i: Int): Int =
      if i >= xs.length then -1
      else if pred(xs(i)) then i
      else go(i + 1)

    go(0)

  // a custom string interpolator
  

abstract class Functions:
  // an abstract function (no impl), must have the return type specified
  def processEvent(id: Long): Unit

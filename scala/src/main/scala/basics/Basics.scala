package com.learning
package basics

import scala.annotation.tailrec
import scala.util.Random

/** Basic features of Scala language (doc comment) **/
object Basics:
  def main(args: Array[String]): Unit =
    val x = 10 // val is immutable
    var y = 10 // var allows mutation
    y = 12  // this expression results in Unit

    println("max = " + max(x, y))
    println("min = " + min(x, y))

    // conversions
    println("12".toInt)  // StringOps method
    println("char 99: '" + 99.toChar + "'")

    // for loop
    print("for loop: ")
    for i <- 0 until 4 do  // iterates over a Range(0,1,2,3); use 'to' to include 4
      print(s"$i ")
    println()

    // combine for and filtering
    for
      i <- 1 to 30
      if (i % 3) == 0 && i != 21
    do
      print(s"$i ")
    println()

    // nested loops
    for
      i <- 7 to 9
      j <- 7 to 9
    do
      println(s"$i x $j = ${i * j}")

    // produce a new collection using for
    val multiplesOfFour =  // immutable indexed sequence, in current version: Vector
      for
        i <- 1 to 100
        if i % 4 == 0
      yield i
    println("multiples of four = " + multiplesOfFour)

    // another example (scalar product)
    val v1 = List(1, 2, 3)
    val v2 = List(4, 5, 6)
    val scalarProduct = (for (x, y) <- v1.zip(v2) yield x * y).sum

    // a tuple
    val tuple = (27, "test")  // tuple can hold elements of different types
    println("The first element is %d, and the second is %s".format(tuple._1, tuple._2))

    // characters of a string
    val str = "The weather is cold"
    for c <- str do
      print(s"$c ")
    println()

    // menu using raw string
    println(
      """
        |Welcome to the Demo.
        |1. First Option
        |2. Second Option
        |3. Third Option
      """.stripMargin)

    val symbol = Symbol("SomeSymbol")  // class Symbol; used like undeclared identifiers in dynamically typed languages

    val name = "user"
    println(s"hello, $name!") // processed string literal
    println(s"ans = ${ 2 + 2 * 2 }")
    println(f"${math.Pi}%.7f") // printf-style formatting with f

    // any method can be used using infix notation, not just arithmetic operators like +
    val pos1 = "a string" `indexOf` 's'
    val pos2 = "a string" `indexOf` ('i', 2)   // parenthesis are required for more than 1 parameter

    // 4 prefix operators: +, -, !, ~
    // the method must be named unary_+ or unary_! and so on

    // can omit parenthesis for methods that take no parameters and have no side effects
    val s1 = "Some String"
    println(s1.toLowerCase)

    // in Scala, == invokes equals
    println("HELLO".toLowerCase == "hello")  // would be false in Java

    // operator precedence is determined by the first character in the method's name
    // +++ has lower precedence than ***
    // associativity is determined by the last character, if it's ':', it's right, otherwise left
    // a * b * c   is   (a * b) * c,   but  a ::: b ::: c   is   a ::: (b ::: c)

    // rich operators (defined in RichInt, StringOps classes)
    0 max 5  // 5
    0 min 5  // 0
    -2.7.abs     // 2.7
    -3.7.round   // -3L
    3 to 6   // Range(3,4,5,6)
    "word".capitalize  // Word
    "word" drop 2      // rd
    "a sentence".toList  // to a list of Char

    // Thread.`yield`()    // yield is a reserved word in Java, so we need to embrace it in `` to call this method

    // implicit methods allow automatic conversion between classes on the fly

    val num = (Math.random() * 10).toInt
    val mResult = num match
      case 1 => "One!"
      case 2 => "Two!"
      case _ => "Something else!"
    println(mResult)

    // equiv. to x => x > 2;  can omit a parameter if it appears only once.
    val anotherList = List(1, 2, 3, 4, 5)
    println(anotherList.filter(_ > 2))

    // partially applied functions use _ to denote all missing arguments
    val sum = (x: Int, y: Int, z: Int) => x + y + z
    val partialSum = sum(1, _: Int, _: Int)
    println(partialSum(7, 12))  // 20

    // can use just a function name, when the function is expected
    anotherList.foreach(print)
    println()

    // closures capture the variable itself
    var some = 2
    val addSome = (x: Int) => x + some
    println(addSome(5)) // 7
    some = 100
    println(addSome(5)) // 105

    // changes inside a closure affect variables
    var mySum = 0
    anotherList.foreach(mySum += _)
    println("sum = " + mySum)

    println(randomFilename())
    println(randomFilename())

    // exceptions
    var z: Int = 0
    try
      z = 100 / z
    catch
      case _: ArithmeticException => println("cannot divide by zero")
      case e: Exception => println(e.getMessage)

    println("2^50 = " + power(2, 50))
    printFunc("sqrt", sqrt, 3.781)
    printFunc("sqrtFixedPoint", sqrtFixedPoint, 3.781)
    printFunc("sqrt", sqrt, 0.1e-20)
    printFunc("sqrtFixedPoint", sqrtFixedPoint, 0.1e-20)
    printFunc("sqrt", sqrt, 1e+20)
    printFunc("sqrtFixedPoint", sqrtFixedPoint, 1e+20)
    printFunc("sqrt", sqrt, 1e+50)
    printFunc("sqrtFixedPoint", sqrtFixedPoint, 1e+50)


  // max function
  private def max(x: Int, y: Int): Int =
    if x > y then x
    else y  // no need for return statement

  private def min(x: Int, y: Int): Int =
    if x <= y then x
    else y

  private def randomFilename(): String =
    val n: Int = Random.between(1, 20)
    val rnd = BigInt.probablePrime(n * 8, Random)
    rnd.toString(36) + ".tmp"

  private def power(x: Long, n: Int): Long =
    if n > 0 then
      if n % 2 == 0 then
        val p = power(x, n / 2)
        p * p
      else
        x * power(x, n - 1)
    else if n == 0 then 1
    else 1 / power(x, -n)


  // Newton's method
  private def sqrt(x: Double): Double =
    @tailrec
    def loop(x0: Double): Double =
      if isClose(x, x0 * x0) then x0
      else
        val x1 = x0 / 2.0 + x / (2 * x0)
        loop(x1)

    loop(x / 2.0)  // initial guess


  // Fixed point: f(x) = x
  // x = sqrt(a) iff x * x = a
  //   hence x = a / x
  //   it means that x is a fixed point of f(x) = a / x
  private def sqrtFixedPoint(a: Double): Double =
    // for it to converge, we take an average of prev and current values
    findFixedPoint( averageDamp(x => a / x) )(a / 2)

  // Finds a fixed point of a function, i.e. x such that f(x) = x
  def findFixedPoint(f: Double => Double)(guess: Double = 1.0): Double =
    @tailrec
    def loop(guess: Double): Double =
      val next = f(guess)  // iteratively apply the function until it starts to give the same result
      if isClose(guess, next) then next
      else loop(next)  // may not converge for some functions

    loop(guess)

  // stabilizes the algorithm to find a fixed point, so it converges
  // done by averaging values of the previous and current iterations
  def averageDamp(f: Double => Double)(x: Double): Double =
    (x + f(x)) / 2.0

  def abs(x: Double): Double =
    if x < 0 then -x else x

  def isClose(a: Double, b: Double): Boolean =
    abs((a - b) / b) < 1e-7  // eps


  private def printFunc[A](name: String, f: A => A, arg: A): Unit =
    println(s"${name}(${arg}) = ${f(arg)}")

  def tokenize(s: String, sep: List[Char]): Seq[String] =
    val (res, acc) = s.foldLeft((List[String](), ""))((a, c) =>
      val (res, acc) = a
      if sep.contains(c) then
        if acc.nonEmpty then
          (c.toString :: acc :: res, "")
        else
          (c.toString :: res, "")
      else
        (res, acc + c)
    )

    (if acc.nonEmpty then acc :: res else res).reverse

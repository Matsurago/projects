package com.learning
package basics

import scala.annotation.tailrec

// class with a private field
class Wrapper(value: Int)

// class with a field with getter (and default value)
// this defines the primary constructor
class Stuff(val description: String = "Unknown"):
  // another field, not part of the constructor
  // both getter and setter are generated
  // needs to be initialized
  var count: Int = 0

  // this is executed in the body of the primary constructor
  println("Created a Stuff object.")

  // auxiliary constructor, must call the primary one
  def this(id: Int) =
    this(s"id: $id")

  // override keyword is required
  override def equals(obj: Any): Boolean =
    if (!obj.isInstanceOf[Stuff])
      false
    else
      this.description == obj.asInstanceOf[Stuff].description


object ClassesDemo:
  def main(args: Array[String]): Unit =
    val wrapper = new Wrapper(10)
    // wrapper.value is inaccessible

    val bookshelf = new Stuff("A bookshelf")
    println(bookshelf.description)

    val something = new Stuff
    println(something.description)

    val another = new Stuff(1234)
    another.count = 3  // uses a setter underneath
    println(another.description)

    println(bookshelf.count)
    println(another.count)

    println(something.equals(new Stuff))
    println(something.equals(3))

    val a = Rat(1, 2)
    val b = Rat(3, 4)

    val jsonData = JSON.Object(Map(
      "firstName" -> JSON.Str("John"),
      "lastName" -> JSON.Str("Smith"),
      "address" -> JSON.Object(Map(
        "street" -> JSON.Str("21 2nd Street"),
        "state" -> JSON.Str("NY"),
        "postalCode" -> JSON.Num(10021),
      )),
      "tel" -> JSON.Array(List(
        JSON.Object(Map(
          "type" -> JSON.Str("home"),
          "number" -> JSON.Str("212 555-1234")
        )),
        JSON.Object(Map(
          "type" -> JSON.Str("fax"),
          "number" -> JSON.Str("646 555-4567")
        )),
      ))
    ))
    println(show(jsonData))


// parameters to class become parameters to the primary constructor
class Rat(x: Int, y: Int):
  // invariant established by the primary constructor
  require(y > 0, "denominator must be positive")

  // private field, not accessible from outside
  private val g = gcd(x.abs, y)

  // public fields with getters
  val numerator: Int = x / g
  val denominator: Int = y / g

  // an auxiliary constructor
  def this(x: Int) = this(x, 1)

  @tailrec
  private def gcd(a: Int, b: Int): Int =
    if b == 0 then a
    else gcd(b, a % b)

  override def toString: String = s"${numerator}/${denominator}"
end Rat  // optional end class marker (useful for long classes)


// case class via abstract (alternative to enum)
abstract class JSON
object JSON:
  case class Array (items: List[JSON]) extends JSON
  case class Object (properties: Map[String, JSON]) extends JSON
  case class Num (value: Double) extends JSON
  case class Str (value: String) extends JSON
  case class Bool (value: Boolean) extends JSON
  case object Null extends JSON

def show(json: JSON): String = json match
  case JSON.Array(items) =>
    items.map(show).mkString("[", ", ", "]")
  case JSON.Object(properties) =>
    properties.map((key, value) => s"\"$key\": ${show(value)}")
      .mkString("{\n", ",\n", "\n}")
  case JSON.Num(value) => value.toString
  case JSON.Str(value) => s"\"$value\""
  case JSON.Bool(value) => value.toString
  case JSON.Null => "null"

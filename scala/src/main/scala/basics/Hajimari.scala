package sc.study.basics

object Hajimari {
  def main(args: Array[String]): Unit = {
    println("hello")

    val inc = (x: Int) => x + 1
    println(inc(3))

    def twice[A](f: A => A, a: A) = f(f(a))
    println(twice(inc, 7))

    var z = 13
    def mutate() = { z += 1; z }

    println(mutate())
    println(mutate())
    println("z = " + z)
  }
}

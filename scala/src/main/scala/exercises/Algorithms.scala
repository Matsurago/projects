package com.learning
package exercises

import scala.annotation.tailrec
import scala.collection.mutable

// How many ways can give a change using a specific set of coins?
// Method 1: memoization (DP)
def countChange(money: Int, coins: List[Int]): Int =
  def insertInSorted(xs: List[Int], value: Int): List[Int] = xs match
    case h :: t =>
      if value <= h then value :: h :: t
      else h :: insertInSorted(t, value)
    case Nil => value :: Nil

  val memo: mutable.Map[Int, Set[List[Int]]] = mutable.Map.empty
  for
    c <- coins
  do
    memo(c) = Set(List(c))

  for
    i <- coins.min until money
  do
    for
      c <- coins if i + c <= money && memo.contains(i)
    do
      val newPaths =  memo(i).map(insertInSorted(_, c))
      memo(i + c) = memo.getOrElse(i + c, Set()) ++ newPaths

  if memo.contains(money)
  then memo(money).size
  else 0


// How many ways can give a change using a specific set of coins?
// Method 2: recursion with immutable data structures
def countChange2(money: Int, coins: List[Int]): Int =
  // precondition: change is a list of (coin, amount) sorted by coin
  def addCoin(change: List[(Int, Int)], coin: Int): List[(Int, Int)] = change match
    case (c, amount) :: t =>
      if coin < c then (coin, 1) :: (c, amount) :: t  // this kind of coin is not yet in change
      else if coin == c then (c, amount + 1) :: t     // already have this kind of coin in change
      else (c, amount) :: addCoin(t, coin)
    case Nil => (coin, 1) :: Nil                      // this kind of coin is not yet in change

  // change is a list of (coin, amount)
  // the function computes the total amount of money in change
  def sum(change: List[(Int, Int)]): Int =
    change.map(_ * _).sum

  @tailrec
  def count(changeVariants: Set[List[(Int, Int)]]): Int =
    val updatedChangeVariants =
      changeVariants.flatMap(change => {
        val s = sum(change)
        val xs = coins
          .filter(s + _ <= money)
          .map(addCoin(change, _))
          .toSet
        if xs.size == coins.size then xs else xs + change
      })

    if updatedChangeVariants.size != changeVariants.size then count(updatedChangeVariants)
    else changeVariants.count(p => sum(p) == money)

  count(coins.map(c => List((c, 1))).toSet)

package com.learning
package exercises

import scala.annotation.tailrec

// Solved excersises from "Functional Programming in Scala, Second Edition"
object Exercises:
  // Exercise 2.1
  def fib(n: Int): Int =
    @tailrec
    def loop(n: Int, prev: Int, current: Int): Int =
      if n <= 1 then current
      else loop(n - 1, current, prev + current)

    if n <= 1 then 0
    else loop(n - 1, 0, 1)

  // Exercise 2.2 (generalized)
  def isSorted[T](a: Array[T], compare: (x: T, y: T) => Boolean): Boolean =
    @tailrec
    def go(i: Int): Boolean =
      if i >= a.length then true
      else if compare(a(i - 1), a(i)) then go(i + 1)
      else false

    go(1)

  // Exercise 2.3
  def curry[A, B, C](f: (A, B) => C): A => B => C =
    a => b => f(a, b)

  // Exercise 2.4
  def uncurry[A, B, C](f: A => B => C): (A, B) => C =
    (a, b) => f(a)(b)

  // Exercise 2.5 (this form is actually andThen rather than compose)
  def compose[A, B, C](f: A => B, g: B => C): A => C =
    a => g(f(a))

  // Exercise 4.2
  def variance(xs: Node[Double]): Maybe[Double] =
    def mean(xs: Node[Double]): Maybe[Double] =
      xs match
        case Node.Nil => Maybe.Empty
        case _ => Maybe.Just(xs.foldl(0.0)(_ + _))

    mean(xs).flatMap(
      m => mean(xs.map(x => Math.pow(x - m, 2)))
    )

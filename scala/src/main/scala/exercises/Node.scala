package com.learning
package exercises

import scala.annotation.tailrec

/** Functional, immutable linked list. */
enum Node[+A]:  // covariant
  case Nil
  case Cons(head: A, tail: Node[A])

  // implement 'apply' => can retrieve value with just parenthesis, as in arrays
  // gets value of the i-th element
  def apply(i: Int): A = Node.get(this, i)

  // implement 'update' => can set value the same way
  def update[B >: A](i: Int, value: B): Node[B] = Node.update(this, i, value)

  def rest: Node[A] = Node.rest(this)
  def init: Node[A] = Node.init(this)
  def setHead[B >: A](value: B): Node[B] = Node.setHead(this, value)

  def map[B](f: A => B): Node[B] = Node.map(this, f)
  def flatMap[B](f: A => Node[B]): Node[B] = Node.flatMap(this, f)
  def filter(f: A => Boolean): Node[A] = Node.filter(this, f)

  def foldr[B](acc: B)(f: (A, B) => B): B = Node.foldr(this, acc, f)
  def foldl[B](acc: B)(f: (B, A) => B): B = Node.foldl(this, acc, f)

  // omit parentheses for no-arg pure methods
  def size: Int = Node.foldr(this, 0, (_, a) => a + 1)

  def foreach(f: A => Unit): Unit = Node.foreach(this, f)

  def append[B >: A](node: Node[B]): Node[B] = Node.append(this, node)
  def drop(n: Int): Node[A] = Node.drop(this, n)
  def dropWhile(f: A => Boolean): Node[A] = Node.dropWhile(this, f)
  def zipWith[B, C](node: Node[B])(f: (A, B) => C): Node[C] = Node.zipWith(this, node, f)

  def reverse: Node[A] = Node.reverse(this)
  def hasSubsequence[B >: A](sub: Node[B]): Boolean = Node.hasSubsequence(this, sub)

  // if not provided, there is a default toString() implementation for enums
  override def toString: String =
    def go(node: Node[A]): String = node match
      case Nil => ""
      case Cons(x, Nil) => s"$x"
      case Cons(x, xs) => s"$x, ${go(xs)}"

    "Node [" + go(this) + "]"


object Node:
  // implement 'apply' in the companion object with variable number of arguments
  //   => can construct objects like arrays with ( )
  def apply[A](args: A*): Node[A] =
    if args.isEmpty then Nil
    else Cons(args.head, apply(args.tail*))

  def get[A](node: Node[A], index: Int): A =
    @tailrec
    def loop(node: Node[A], i: Int): A = node match
      case Nil => throw IndexOutOfBoundsException(index)
      case Cons(x, xs) =>
        if i == 0 then x
        else loop(xs, i - 1)

    loop(node, index)

  def update[A](node: Node[A], i: Int, newValue: A): Node[A] = node match
    case Nil => throw IndexOutOfBoundsException(i)
    case Cons(x, xs) =>
      if i == 0 then Cons(newValue, xs)
      else Cons(x, update(xs, i - 1, newValue))

  // Exercise 3.2 (tail)
  def rest[A](node: Node[A]): Node[A] = node match
    case Nil => Nil
    case Cons(_, xs) => xs

  // Exercise 3.3
  def setHead[A](node: Node[A], newValue: A): Node[A] = node match
    case Nil => Cons(newValue, Nil)
    case Cons(_, xs) => Cons(newValue, xs)

  // Exercise 3.6
  def init[A](node: Node[A]): Node[A] = node match
    case Nil => Nil
    case Cons(x, Cons(_, Nil)) => Cons(x, Nil)
    case Cons(x, xs) => Cons(x, init(xs))

  // we can think of foldr as a function that:
  //   replaces Nil  with acc
  //   replaces Cons with f
  def foldr[A, B](node: Node[A], acc: B, f: (A, B) => B): B = node match
    case Nil => acc
    case Cons(x, xs) => f(x, foldr(xs, acc, f))

  @tailrec
  def foldl[A, B](node: Node[A], acc: B, f: (B, A) => B): B = node match
    case Nil => acc
    case Cons(x, xs) => foldl(xs, f(acc, x), f)

  // Exercise 3.13: foldr via foldl
  // This is useful because foldl is implemented as a tail-recursive function (avoid stack overflows)
  def foldr2[A, B](node: Node[A], acc: B, f: (A, B) => B): B =
    foldl(reverse(node), acc, (a, x) => f(x, a))

  // foldr via foldl without double passing the list (note: not stack safe)
  def foldr3[A, B](node: Node[A], acc: B, f: (A, B) => B): B =
    val id = (b: B) => b
    val func = foldl(node, id, (g, a) => b => g(f(a, b)))  // stacks function compositions
    func(acc)

  @tailrec
  def foreach[A](node: Node[A], f: A => Unit): Unit = node match
    case Nil => ()
    case Cons(x, xs) => f(x); foreach(xs, f)

  def append[A](xs: Node[A], ys: Node[A]): Node[A] =
    foldr(xs, ys, Cons(_, _))

  // Exercise 3.15
  def concat[A](node: Node[Node[A]]): Node[A] =
    foldr(node, Nil: Node[A], append)

  // Exercise 3.4
  @tailrec
  def drop[A](node: Node[A], n: Int): Node[A] = node match
    case Nil => Nil
    case Cons(x, xs) =>
      if n <= 0 then node
      else drop(xs, n - 1)

  // Exercise 3.5
  @tailrec
  def dropWhile[A](node: Node[A], f: A => Boolean): Node[A] = node match
    case Cons(x, xs) if f(x) => dropWhile(xs, f)
    case _ => node

  // Exercise 3.16 - 3.18
  def map[A, B](node: Node[A], f: A => B): Node[B] =
    foldr(node, Nil: Node[B], (x, a) => Cons(f(x), a))

  // Exercise 3.19
  def filter[A](node: Node[A], f: A => Boolean): Node[A] =
    foldr(node, Nil: Node[A], (x, a) => if f(x) then Cons(x, a) else a)

  // Exercise 3.20
  def flatMap[A, B](node: Node[A], f: A => Node[B]): Node[B] =
    foldr(node, Nil: Node[B], (x, a) => append(f(x), a))

  // Exercise 3.12
  def reverse[A](node: Node[A]): Node[A] =
    foldl(node, Nil: Node[A], (acc, x) => Cons(x, acc))

  // Exercise 3.22 - 3.23
  def zipWith[A, B, C](xs: Node[A], ys: Node[B], f: (A, B) => C): Node[C] =
    (xs, ys) match
      case (_, Nil) => Nil
      case (Nil, _) => Nil
      case (Cons(x, xTail), Cons(y, yTail)) => Cons(f(x, y), zipWith(xTail, yTail, f))

  // Exercise 3.11
  def product(node: Node[Double]): Double =
    foldl(node, 1.0, _ * _)

  // Exercise 3.21
  def filterViaFlatMap[A](node: Node[A], f: A => Boolean): Node[A] =
    flatMap(node, x => if f(x) then Cons(x, Nil) else Nil)

  // Exercise 3.24
  @tailrec
  def hasSubsequence[A](node: Node[A], sub: Node[A]): Boolean = node match
    case Nil => hasPrefix(node, sub)
    case Cons(_, xs) =>
      if hasPrefix(node, sub) then true
      else hasSubsequence(xs, sub)

  @tailrec
  def hasPrefix[A](node: Node[A], prefix: Node[A]): Boolean = (node, prefix) match
    case (_, Nil) => true
    case (Cons(x, xs), Cons(y, ys)) if x == y => hasPrefix(xs, ys)
    case _ => false

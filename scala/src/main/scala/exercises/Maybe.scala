package com.learning
package exercises


enum Maybe[+A]:
  case Empty
  case Just(value: A)

  def map[B](f: A => B): Maybe[B] = this match
    case Empty => Empty
    case Just(x) => Just(f(x))

  def flatMap[B](f: A => Maybe[B]): Maybe[B] = this match
    case Empty => Empty
    case Just(x) => f(x)

  def getOrElse[B >: A](default: => B): B = this match
    case Empty => default
    case Just(x) => x

  def orElse[B >: A](default: => Maybe[B]): Maybe[B] = this match
    case Empty => default
    case _ => this

  def filter(f: A => Boolean): Maybe[A] = this match
    case Just(x) if f(x) => Just(x)
    case _ => Empty


object Maybe:
  def apply[A](values: A*): Maybe[A] =
    if values.isEmpty then Empty
    else Just(values.head)

  def lift[A, B](f: A => B): Maybe[A] => Maybe[B] =
    x => x.map(f)

  def lift2[A, B, C](f: (A, B) => C): (Maybe[A], Maybe[B]) => Maybe[C] =
    (x: Maybe[A], y: Maybe[B]) =>
      for
        a <- x
        b <- y
      yield f(a, b)

  // Exercise 4.3
  def map2[A, B, C](a: Maybe[A], b: Maybe[B])(f: (A, B) => C): Maybe[C] =
    lift2(f)(a, b)

  // Exercise 4.4
  def sequence[A](xs: Node[Maybe[A]]): Maybe[Node[A]] =
    traverse(xs)(x => x)

  // Exercise 4.5
  def traverse[A, B](xs: Node[A])(f: A => Maybe[B]): Maybe[Node[B]] =
    val acc: Maybe[Node[B]] = Just(Node.Nil)  // Just []
    val cons: (B, Node[B]) => Node[B] = Node.Cons(_, _)  // :

    xs.foldr(acc)((x, acc) => lift2(cons)(f(x), acc))

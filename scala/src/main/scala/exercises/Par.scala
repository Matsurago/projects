package com.learning
package exercises

class Par



def parSum(xs: List[Int]): Int =
  if xs.isEmpty then 0
  else if xs.size == 1 then xs.head
  else
    val (l, r) = xs.splitAt(xs.length / 2)
    parSum(l) + parSum(r)

package com.learning
package exercises

import scala.util.control.NonFatal

// Either implementation
// The name is Result to avoid conflicts with Either from the Scala standard library
enum Result[+E, +A]:
  case Left(error: E)
  case Right(value: A)

  def map[B](f: A => B): Result[E, B] = this match
    case Left(err) => Left(err)
    case Right(v) => Right(f(v))

  def flatMap[EE >: E, B](f: A => Result[EE, B]): Result[EE, B] = this match
    case Left(err) => Left(err)
    case Right(v) => f(v)

  def orElse[EE >: E, B >: A](b: => Result[EE, B]): Result[EE, B] = this match
    case Left(err) => b
    case Right(v) => Right(v)

object Result:
  def catchNonFatal[A](value: => A): Result[Throwable, A] =
    try
      Right(value)
    catch
      // NonFatal avoids catching JVM errors such as StackOverflow or OOM
      case NonFatal(e) => Left(e)

  // Exercise 4.6
  def map2[E, A, B, C](a: Result[E, A], b: Result[E, B])(f: (A, B) => C): Result[E, C] =
    lift2(f)(a, b)

  def lift2[E, A, B, C](f: (A, B) => C): (Result[E, A], Result[E, B]) => Result[E, C] =
    (a, b) => (a, b) match
      case (Right(a), Right(b)) => Right(f(a, b))
      case (Left(err), _) => Left(err)
      case (_, Left(err)) => Left(err)

  // Exercise 4.7
  def sequence[E, A](xs: Node[Result[E, A]]): Result[E, Node[A]] =
    traverse(xs)(x => x)

  // Applies a function that may fail to a list of values.
  // Returns a list of mapped values (if all successes), or the first encountered error.
  def traverse[E, A, B](xs: Node[A])(f: A => Result[E, B]): Result[E, Node[B]] =
    val z: Result[E, Node[B]] = Result.Right(Node.Nil)
    val g: (Result[E, B], Result[E, Node[B]]) => Result[E, Node[B]] = lift2(Node.Cons.apply)

    xs.foldr(z)((x, acc) => g(f(x), acc))


object ResultDemo:
  def main(args: Array[String]): Unit =
    // pure function
    def insuranceQuote(age: Int, salary: Int) =
      age * (100_000.0 / salary)

    // invoking pure function from the impure context
    def parseInsuranceRateQuote(age: String, salary: String): Result[Throwable, Double] =
      // this works, because for-yield is a syntactic sugar for map/flatMap
      for
        a <- Result.catchNonFatal(age.toInt)
        s <- Result.catchNonFatal(salary.toInt)
      yield
        insuranceQuote(a, s)

    println("Calculated Rate: " + parseInsuranceRateQuote("24", "150000"))

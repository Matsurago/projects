package com.learning
package exercises

import scala.annotation.tailrec

enum LazyNode[+A]:
  case Nil
  case Cons(head: () => A, tail: () => LazyNode[A])

  def toNode: Node[A] = LazyNode.toNode(this)

  def take(n: Int): LazyNode[A] = LazyNode.take(this, n)
  def takeWhile(f: A => Boolean): LazyNode[A] = LazyNode.takeWhile(this, f)
  def drop(n: Int): LazyNode[A] = LazyNode.drop(this, n)

  def exists(f: A => Boolean): Boolean = LazyNode.exists(this, f)
  def foldr[B](acc: => B)(f: (A, => B) => B): B = LazyNode.foldr(this, acc, f)

  def forAll(p: A => Boolean): Boolean =
    foldr(true)(p(_) && _)

//  def takeWhile2(p: A => Boolean): LazyNode[A] =
//    foldr(Nil)((x, acc) => if p(x) then Cons(x, () => acc) else Nil)

object LazyNode:
  // smart constructors

  def empty[A]: LazyNode[A] =
    Nil

  // here we can supply implicit thunks, like foo()
  def cons[A](h: => A, t: => LazyNode[A]): LazyNode[A] =
    // caches values on first evaluation
    // if 'h' and 't' were used directly, they will be recomputed on each access
    lazy val head = h
    lazy val tail = t
    Cons(() => head, () => tail)
  
  // WARNING: should supply explicit thunks like () => foo()
  // or it wont be lazy!
  def apply[A](values: A*): LazyNode[A] =
    if values.isEmpty then empty
    else cons(values.head, apply(values.tail*))

  // force evaluation of thunks
  def toNode[A](ln: LazyNode[A]): Node[A] =
    @tailrec
    def go(node: LazyNode[A], acc: Node[A]): Node[A] = node match
      case Nil => acc.reverse
      case Cons(h, t) => go(t(), Node.Cons(h(), acc))

    go(ln, Node.Nil)


  def take[A](node: LazyNode[A], n: Int): LazyNode[A] = node match
    case Cons(h, t) if n > 0 => cons(h(), take(t(), n - 1))
    case _ => empty

  def takeWhile[A](node: LazyNode[A], f: A => Boolean): LazyNode[A] = node match
    case Cons(h, t) if f(h()) => cons(h(), takeWhile(t(), f))
    case _ => empty

  @tailrec
  def drop[A](node: LazyNode[A], n: Int): LazyNode[A] = node match
    case Cons(_, t) if n > 0 => drop(t(), n - 1)
    case _ => node

  @tailrec
  def exists[A](node: LazyNode[A], f: A => Boolean): Boolean = node match
    case Cons(h, t) => f(h()) || exists(t(), f)  // non-strict ||
    case _ => false

  // non-strict parameters to support lazy evaluation
  def foldr[A, B](node: LazyNode[A], acc: => B, f: (A, => B) => B): B = node match
    case Cons(h, t) => f(h(), foldr(t(), acc, f))
    case _ => acc
    
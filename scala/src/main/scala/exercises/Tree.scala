package com.learning
package exercises

// Binary Search Tree
enum Tree[+A]:
  case Leaf
  case Branch(value: A, left: Tree[A], right: Tree[A])

  def insert[B >: A](compare: (B, B) => Boolean, value: B): Tree[B] = Tree.insert(this, compare, value)

  def size: Int = Tree.size(this)
  def depth: Int = Tree.depth(this)
  def maximum[B >: A](max: (B, B) => B): B = Tree.maximum(this, max)

  def fold[B](acc: B)(f: (A, B, B) => B): B = Tree.fold(this, acc, f)
  def map[B](f: A => B): Tree[B] = Tree.map(this, f)


object Tree:
  def insert[A](tree: Tree[A], compare: (A, A) => Boolean, newValue: A): Tree[A] =
    tree match
      case Leaf => Branch(newValue, Leaf, Leaf)
      case Branch(value, left, right) =>
        if compare(newValue, value)
        then Branch(value, insert(left, compare, newValue), right)
        else Branch(value, left, insert(right, compare, newValue))

  def fromList[A](compare: (A, A) => Boolean, values: A*): Tree[A] =
    values.foldLeft(Leaf: Tree[A])(insert(_, compare, _))

  def size[A](tree: Tree[A]): Int =
    fold(tree, 0, (_, l, r) => 1 + l + r)

  // Exercise 3.26
  def depth[A](tree: Tree[A]): Int =
    fold(tree, 0, (_, l, r) => 1 + Math.max(l, r))

  // Exercise 3.25
  def maximum[A](tree: Tree[A], max: (A, A) => A): A = tree match
    case Leaf => sys.error("maximum is called on an empty tree")
    case Branch(x, _, _) => fold(tree, x, (v, l, r) => max(v, max(l, r)))

  extension (tree: Tree[Int]) def maximumInt: Int =
    maximum(tree, (a, b) => if a >= b then a else b)

  // Exercise 3.27
  def map[A, B](tree: Tree[A], f: A => B): Tree[B] =
    fold(tree, Leaf: Tree[B], (x, left, right) => Branch(f(x), left, right))

  // Exercise 3.28
  // This is written to be similar to foldr in Node
  def fold[A, B](tree: Tree[A], acc: B, f: (A, B, B) => B): B = tree match
    case Leaf => acc
    case Branch(x, left, right) => f(x, fold(left, acc, f), fold(right, acc, f))

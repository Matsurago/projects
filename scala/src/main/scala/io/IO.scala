package sc.study.io

import java.io.{FileNotFoundException, FileReader, IOException}

import scala.io.Source

object IO {
  def main(args: Array[String]): Unit = {
    printFile("file.txt")
  }

  def printFile(filename: String): Unit = {
    def widthOfLength(s: String): Int = s.length.toString.length

    // use Source.fromFile(filename) to read from the filesystem
    val lines = Source.fromResource(filename).getLines().toList

    val longestLine = lines.reduceLeft(
      (a, b) => if (a.length > b.length) a else b)
    val maxWidth = widthOfLength(longestLine)

    for (line <- lines) {
      val numSpaces = maxWidth - widthOfLength(line)
      val padding = " " * numSpaces
      println(padding + line.length + " | " + line)
    }
  }

  // exception handling with pattern matching
  // Note: Scala does not require to declare 'throws' or catch checked exceptions!
  // also, try-catch block results in a value
  def openFile(filename: String): Unit = {
    try {
      val f = new FileReader(filename)
    } catch {
      case e: FileNotFoundException => System.out.println("File not found!")
      case e: IOException => System.out.println("IO Exception!")
    } finally {
      System.out.println("Exiting")
    }
  }
}

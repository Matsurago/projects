package com.learning
package collections

object MapDemo:
  def main(args: Array[String]): Unit =
    // immutable map
    // needs to be 'var' to allow reassignment
    var languages = Map("C++" -> 1983, "Java" -> 1995, "Python" -> 1991)
    // adding value to map; creates a new map and reassigns the variable
    languages += ("Scala" -> 2004)
    
    println(languages)
    println("Java is developed in " + languages("Java"))  // getting value from the map

    try
      // accessing a non-existing element throws an exception
      println(languages("Ada"))
    catch
      case _: Exception => println("Not found: Ada")

    // more robust ways to get value from map
    println("getOrElse: " + languages.getOrElse("Ada", 1900))
    println("get: " + languages.get("Ada"))  // returns Option

    // immutable map with default value (can also specify default compute function)
    val lang2 = languages.withDefaultValue(1890)
    println(lang2("Ada"))  // 1890

    // mutable map, can be 'val'
    val m = scala.collection.mutable.Map[String, Integer]()  // empty map with specified types
    m("lucky number") = 3  // can update without reassignment
    // add multiple pairs
    m ++= Map("stars" -> 7, "cents" -> 89)
    println(m)

    // delete a key from mutable map
    m -= "cents"
    println(m)

    // iteration
    for (k, v) <- m do
      println(s"key: '$k', value: $v")

    // inverted map
    val inverted = for (k, v) <- m yield (v, k)
    println(inverted)

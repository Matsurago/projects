package com.learning
package collections

import scala.collection.immutable.HashSet


object SetDemo:
  def main(args: Array[String]): Unit =
    var movies = Set("Matrix", "Leon", "Taxi")  // creates an immutable set
    movies += "Hobbit"   // creates a new set with an additional element, and reassignes the variable 'movies'
    println(movies)
    println("Movies contain 'Taxi': " + movies.contains("Taxi"))

    val mutableSetExample = scala.collection.mutable.Set(1, 2, 10)   // can be val, no reassignment below
    mutableSetExample += 15   // actally adds an element to the current set
    println(mutableSetExample)

    val concreteSet = HashSet('a', 'c')  // specifying concrete set implementation
    println(concreteSet)

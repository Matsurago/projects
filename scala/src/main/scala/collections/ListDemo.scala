package sc.study.collections


// Lists are always immutable!
object ListDemo extends App {

  val firstList = List(1, 2, 3)
  //firstList(1) = 3    // not allowed
  println(firstList)

  val secondList = List(4, 5)
  val longerList = firstList ::: secondList    // concatenation of two lists
  println(longerList)

  val evenLongerList = -1 :: 0 :: longerList  // cons (can add element only at the beginning of a list)
  // General Rule: if the method name ends in a colon, the method is invoked on the right operand (as 'cons')
  println(evenLongerList)

  val empty = Nil  // Nil is an empty list
  println(empty)

  val anotherList = 'a' :: 'b' :: 'c' :: Nil   // another way to construct a list
  val anotherList2 = anotherList :+ 'd'  // append: expensive operation, rarely used! Instead, use ListBuffer, and then call toList()
  println(anotherList2)

  val numbers = List(1, 3, 10, 89, 100, -1, -2, -10, 0, 1, 0, 1, 1, 120)
  println(numbers)
  println("Length: " + numbers.length)
  println("First element is " + numbers.head + ", second is " + numbers(1) + ", last one is " + numbers.last)  // accessing elements
  println("number of elements less than one: " + numbers.count(x => x < 1))  // count + predicate
  println("exists > 100: " + numbers.exists(x => x > 100) + "; exists > 150: " + numbers.exists(x => x > 150)) // exists
  println("for all x < 200: " + numbers.forall(x => x < 200) + "; for all x < 100: " + numbers.forall(x => x < 100))  // P holds for all
  println(numbers.drop(3))  // list without first three elements
  println(numbers.dropRight(3))  // list without last three elements
  println("odd: " + numbers.filter(x => x % 2 == 1))  // filter odd numbers

  numbers.init // all elements, but last
  numbers.tail // all elements, but first
  numbers.isEmpty

  println(numbers.map(x => x + 1)) // map
  println(numbers.reverse)  // reverses a list !

  val sortedNumbers = numbers.sortWith((x, y) => x > y)
  println("Sorted (desc.): " + sortedNumbers)

  println(numbers.mkString(" * ")) // toString(); the parameter is a separator

}

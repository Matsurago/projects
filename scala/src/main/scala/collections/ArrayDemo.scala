package com.learning
package collections

import scala.collection.mutable.ArrayBuffer

object ArrayDemo:
  @main
  def main(): Unit =
    // array of 5 ints initialized to zero
    val arr = new Array[Int](5)
    arr(3) = 3    // assignment to an array element: use () instead of []
    arr.foreach(x => print(s"$x "))   // foreach
    println()

    // array of existing values
    val arr2 = Array("cat", "plot", "message", "Internet", "the U.S.")
    // if lambda is x => f(x), argument's name can be omitted
    arr2.foreach(print)
    println()

    // foreach loop
    print("Classic foreach loop: ")
    for x <- arr2 do
      print(x + " ")
    println()

    // variable length array
    val a = ArrayBuffer[Int]()
    a += 3  // add an element
    a ++= Array(5, 7, 9)  // add multiple elements
    println(a)

    // find indices of elements that satisfy some predicate
    val is = for
      i <- a.indices
      if a(i) % 3 == 0
    yield i
    println(is)  // 0, 3

    // remove elements that didn't satisfy the predicate
    for i <- is.indices do
      a(i) = a(is(i))
    a.dropRightInPlace(a.length - is.length)
    println(a)  // 3, 9

    // sort
    a ++= Array(5, 7, 2)
    val b = a.sortWith(_ > _)  // descending
    println(b)

    // binary search
    val c = b.sorted  // asc
    println(c.search(7))  // Found(3)
    println(c.search(8))  // InsertionPoint(4)

    // to string with a separator
    val s = a.mkString(",")
    println(s)

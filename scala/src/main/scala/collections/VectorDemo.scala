package com.learning
package collections

// Vectors are like lists but with more efficient random element access/update
// They are implemented as a shallow tree of arrays, each array at most 32 element long
// The tree is at most 5 layers deep:
//    layer 1: 32 elements
//    layer 2: 32 children of 32 elements each
//    ...
//    layer 5: 2^(5*5) = max 2^25 elements total
object VectorDemo:
  def main(args: Array[String]): Unit =
    // create
    val v = Vector("fireball", "water cannon")
    // add front
    val v1 = "shock" +: v
    // add back
    val v2 = v :+ "lantern"

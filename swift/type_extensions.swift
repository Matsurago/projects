// all classes and structs, including built-in ones, can be extended:
extension Int {
    
    func next() -> Int {
        return self + 1;
    }
    
}

print(3.next())  // 4

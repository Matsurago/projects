// type alias
typealias short = UInt16
let minValue = short.min

// FEATURE: declaring a tuple
let pos = (23.92392, "NW")
// FEATURE: tuple decomposition
let (coord, direction) = pos
print("Coordinate: \(coord), direction: \(direction)")
// decomposition of only needed values
let (mycoord, _) = pos
// accessing a value in tuple without decomposition
print("Coordinate: \(pos.0)")
// declaring a tuple with named values
let item = (price: 100, label: "clock")
// accessing a named value in tuple
print("The \(item.label) costs \(item.price)$")

// FEATURE: optional binding
let possibleNumber = "9823"
if let value = Int(possibleNumber) { // conversion returns Int?
  print("Value = \(value)")  // value has type Int
} else {
  print("\(possibleNumber) is not an integer.")
}
// multiple binding: true if all values are not nil
if let fst = Int("3"), let snd = Int("7") {
  print("Not nil.")
}
// declaring an implicitly unwrapped optional
let name: String! = "John" // can be used as non-optional thereafter

// declaring a function that can throw an exception
func communicate() throws {
  // do something
}
// a function that may throw must be called with try
try communicate()
// 'do' statement creates a new scope
do {
  try communicate()
} catch {
  // process an exception
}

// assertions work only in debug builds
let age = 10
assert(age >= 0, "A person's age must be positive.")
// FEATURE: preconditions retain in release
precondition(age >= 0, "A person's age must be positive.")

// use fatal errors to terminate an app
func unimplementedStub() {
  fatalError("Unimplemented!")
}

// nil-coalescing operator
var myColor: String?
let color = myColor ?? "Green"
print(color)

// allowing integer overflow
var small: Int8 = 120
small &+= 10  // -126

// 3 types: arrays, sets, and dictionaries

// ARRAYS

// create a mutable array of Ints
var arr  = [Int]()   // empty
var arr2 = ["cat", "mouse"]   // from array literal
print("size = \(arr2.count)")

// create an array of default values
var a2 = Array(repeating: 0, count: 3)
print(a2)

// add an element to the array
var a3 = [1, 2, 3]
a3.append(4)
print(a3)
// add multiple elements to an array
a3 += [5, 6]
print(a3)

// change a value in the array
a3[0] = 100
print(a3)
// change multiple values in the array
a3[1...3] = [7, 8]  // number of elements can be different
print(a3)
// insert a value at the specific position of the array
a3.insert(99, at: 1)
print(a3)
// remove a value at the specific position
a3.remove(at: 0)
print(a3)
// remove the last value
a3.removeLast()
print(a3)

// iterate over an array having indexes
for (index, value) in a3.enumerated() {
  print("\(value) at position \(index)")
}

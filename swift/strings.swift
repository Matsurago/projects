import Foundation

// convert from String to Int
let x = Int("123")  // returns Optional

// iterate over characters in a String
for c in "a string" {
  print(c, terminator: " ")
}
print()

// length of a String
let len = "some string".count
print("length = \(len)")

// test if string starts/ends with a substring
let s2 = "The cat is cute"
print(s2.hasSuffix("cute")) // true
print(s2.hasPrefix("The")) // true

// take a substring
// a) entire string
print(s2[s2.startIndex..<s2.endIndex])
// b) starting from the word 'cat' (or from start if no 'cat' is found)
let startPos = s2.range(of: "cat")?.lowerBound ?? s2.startIndex
print(s2[startPos...])
// c) up to word 'is'
let endPos = s2.range(of: " is")?.lowerBound ?? s2.endIndex
print(s2[...endPos])

// to include a unicode character using its code:
let str = "Love \u{1f496}"
print(str)

// a character type is used to store only one character
let char: Character = "!"
// Unicode *combining characters* contribute to the previous character:
let char2: Character = "\u{e9}\u{20dd}"  // 0x20dd adds an enclosing circle
print(char2)
// Unicode regional indicators also count as one symbol
let char3: Character = "\u{1f1fa}\u{1f1f8}" // 'U' + 'S' = 'US'
print(char3) // US flag

// declaring a class
class User {
    let firstName: String
    let lastName: String

    init(_ firstName: String, _ lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}

let user1 = User("John", "Newton")
let user2 = user1  // points to the same object
print(user1 === user2)  // true
let user3 = User("John", "Newton")
print(user1 === user3)  // false


// unlike classes, structs have default all-args constructor
struct UserStruct {
    let firstName: String
    let lastName: String
}

let userS1 = UserStruct(firstName: "John", lastName: "Newton")
let userS2 = UserStruct(firstName: "John", lastName: "Newton")
// structs are value types, so operator === cannot be applied to struct objects

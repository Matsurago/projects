// for-in loop and closed range
for index in 1...5 {
  print("\(index) ", terminator:"")
}
print("")

// for-in loop and half-open range + condition
for index in 1..<5 where index % 2 == 1 {
  print("\(index) ", terminator:"")
}
print("")

// iterate over an array having indexes
let arr = ["cat", "dog", "mouse"]
for (index, value) in arr.enumerated() {
  print("\(value) at position \(index)")
}

// Ex.1: switch + ranges
fileprivate func describe(_ statusCode: Int) -> String {
    switch statusCode {
        case 100, 101:
            return "\(statusCode) Informational"
        case 200:
            return "\(statusCode) Success"
        case 300...307:
            return "\(statusCode) Redirection"
        case let errorCode where errorCode >= 400:
            return "\(errorCode) Client or Server error"
        default:
            return "Unknown"
    }
}
print(describe(404))

// Ex.2: switch + pattern matching 
fileprivate func describeInterval(_ pair: (Double, Double)) -> String {
    switch pair {
        case let (a, b) where b < a:
            return "Malformed interval"
        case (0, _), (_, 0):
            return "Ray from origin"
        case (_, _):
            return "Valid interval"
    }
}
print(describeInterval((-3, 0)))

// Ex.3: if + pattern matching
let num_value = 31
if case 20...50 = num_value, num_value % 2 == 1 {
    print("match")
}

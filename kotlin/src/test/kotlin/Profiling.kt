import kotlin.system.measureTimeMillis

// Measure performance of initializing a list of N values
// using different methods.
// This just calls `System.currentTimeMillis()` so that results vary.
// Use JMH for precise measurements.
fun main() {
    val n = 1_000_000
    val x: List<Int>
    val y: List<Int>
    val z: List<Int>

    val xTime = measureTimeMillis {
        x = mutableListOf()
        for (i in 1..n) {
            x += i
        }
    }

    val yTime = measureTimeMillis {
        y = generateSequence(0) { it + 1 }
            .take(n)
            .toList()
    }

    val zTime = measureTimeMillis {
        z = List(n) { it + 1 }
    }

    println("x (size = ${x.size}), time: $xTime")
    println("y (size = ${y.size}), time: $yTime")
    println("z (size = ${z.size}), time: $zTime")
}

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse

class Strings {

    @Test
    fun stringComparison() {
        val s1 = ('a'..'f').joinToString("")
        val s2 = "abc" + "fed".reversed()

        assertTrue(s1 == s2)           // equals() in Java
        assertFalse(s1 === s2)   // == in Java (referential equality)
    }

    @Test
    fun rawStrings() {
        val json = """
            {
                "name": "Oleg",
                "age": 21
            }
        """.trimIndent()

        val lines = json.split("\n")
        assertEquals(4, lines.size)
    }

    @Test
    fun regex() {
        val s = "4232"

        val res = if (s.matches("\\d+".toRegex()))
            s.toInt()
        else
            -1

        assertEquals(4232, res)
    }

}

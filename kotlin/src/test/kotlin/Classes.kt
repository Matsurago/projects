import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertNotEquals
import kotlin.test.assertNull

class Month {
    // every property declared in class must be initialized
    // even if it's var
    var month: Int = 0
        // custom getter
        get() = field + 1

        // custom setter
        set(value) {
            if (value < 1 || value > 12) {
                throw IllegalArgumentException("not a valid month: $value")
            }
            field = value - 1
        }

    // property that can be initialized later (exception if accessed prior init)
    lateinit var note: String

    // computed property
    val season: String
        get() = when(month) {
            in 0..1, 11 -> "winter"
            in 2..4 -> "spring"
            in 5..7 -> "summer"
            in 8..10 -> "autumn"
            else -> "unknown"
        }

    // lazy init property (for computation intensive code that may not be always needed)
    val year: Int by lazy {
        LocalDate.now().year
    }

    // for static methods
    companion object {
        fun description(): String {
            return "Month of the Year"
        }
    }
}

// 1) can define fields in class header that have no custom getter/setters
// 2) must mark class open to support inheritance
open class Animal(
    name: String,  // has custom getter/setter, no field
    val age: Int,  // defines an immutable field
    var status: String,   // mutable field
    val id: Int = 0       // field initialized with a default argument
) {
    val name = name
        get() = field.replaceFirstChar { it.uppercase() }

    // secondary constructor
    constructor(age: Int) :
            this("unknown animal", age, "unknown status", 0)

    // init block: always runs
    // useful for validation
    init {
        require(name.isNotEmpty())
        require(age >= 0) { "age must be positive" }
    }

    // overridable functions must also be marked as 'open'
    open fun sound(): String = "unknown"
}

// a subclass: must call superclass constructor
class Cat(name: String, age: Int) : Animal(name, age, ""), Pet {
    override fun sound() = "meow!"
}

// singleton class
object IdSeq {
    var id = 0
        private set

    fun inc() { ++id }
}

// data class: has equals, hashCode, toString, copy
// also supports destructuring
data class Point(val x: Int, val y: Int) {
    // support +
    operator fun plus(other: Point): Point {
        return Point(x + other.x, y + other.y)
    }
}

// enum
enum class Day(val n: Int) {
    Monday(1), Tuesday(2), Wednesday(3), Thursday(4),
    Friday(5), Saturday(6), Sunday(7);
}

// thin wrapper - no performance hindrance
@JvmInline
value class UserId(val id: Int)

// pattern: enum-like class but bound value can vary between instances
sealed class StudentStatus {
    data object NotEnrolled : StudentStatus()
    data object Graduated : StudentStatus()
    data class Enrolled(val courseId: Int) : StudentStatus()
}

interface Pet {
    // interface can have properties
    val name: String
}

// generics
// out: covariance      List<Animal> := List<Cat>   read-only
// in:  contravariance  List<Cat> := List<Animal>   write-only, cannot store in property
class MayBe<out T>(val value: T?) where T : Any? {  // constraints on T

    fun <R> map(f: (T) -> R): MayBe<R> =
        if (value != null)
            MayBe(f(value))
        else
            MayBe(null)

    // reified function must be inlined
    inline fun <reified R> takeIfTypeMatches(): R? {
        return if (value is R) value else null
    }
}


class ClassTest {

    @Test
    fun testMonth() {
        val m = Month()
        m.month = 11
        m.note = "last month in the year"

        assertEquals("winter", m.season)
        assertEquals(20, m.year / 100)

        assertEquals("Month of the Year", Month.description())
    }

    @Test
    fun testAnimal() {
        // can use named arguments
        val jack = Animal(
            name = "jack",
            age = 3,
            status = "sleeping"
        )

        assertEquals("Jack", jack.name)
        assertEquals(0, jack.id)

        // via secondary constructor
        val animal = Animal(2)

        assertEquals(2, animal.age)
        assertEquals("unknown status", animal.status)
    }

    @Test
    @Suppress("USELESS_IS_CHECK")
    fun testSubclass() {
        val cat = Cat("Barsik", 5)

        assertTrue(cat is Animal)
        assertEquals("Barsik", cat.name)
        assertEquals("meow!", cat.sound())

        val animal = Animal(10)
        // smart cast: null if failed
        val name = (animal as? Cat)?.name
        assertNull(name)
    }

    @Test
    fun testAnonymousClass() {
        // anonymous class
        val a = object : Animal(4) {
            override fun sound() = "woof!"
        }

        assertEquals("woof!", a.sound())
    }

    @Test
    fun testSingleton() {
        IdSeq.inc()
        assertEquals(1, IdSeq.id)
    }

    @Test
    fun testDataClass() {
        val a = Point(1, 2)

        // copy specifying only changed arguments
        val b = a.copy(x = 3)

        val c = a + b
        // destructuring
        val (xc, yc) = c

        assertEquals(4, xc)
        assertEquals(4, yc)
    }

    @Test
    fun testEnum() {
        assertEquals(Day.Sunday, Day.valueOf("Sunday"))

        // since 1.9
        // entries: better performance, easier usage
        assertEquals(Day.Thursday, Day.entries.first { it.n > 3 })
    }

    @Test
    fun testValueClass() {
        val johnId = UserId(1200)
        assertEquals(1200, johnId.id)
    }

    @Test
    @Suppress("KotlinConstantConditions")
    fun testEnumLikeClass() {
        val status: StudentStatus = StudentStatus.Enrolled(courseId = 100)
        val message = when (status) {
            is StudentStatus.Graduated -> "Alumni"
            is StudentStatus.NotEnrolled -> "Prospect student"
            is StudentStatus.Enrolled -> "Student, course ${status.courseId}"
        }

        assertEquals("Student, course 100", message)
    }

    @Test
    fun testGenerics() {
        val a = MayBe(4)
        val b = a.map { it.toString() }
        assertEquals("4", b.value)

        val c: MayBe<Number> = a  // covariance
        assertEquals(4, c.value)

        // accessing reified function
        val d: String? = b.takeIfTypeMatches()
        val e: Int? = b.takeIfTypeMatches()
        assertEquals("4", d)
        assertEquals(null, e)
    }
}


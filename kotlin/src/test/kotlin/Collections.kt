import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class CollectionsTest {

    @Test
    fun lists() {
        val names = mutableListOf("Oleg", "John", "Sabina", "Eric")

        // add
        names += "Jessica"
        names += listOf("Zahar", "Ahmed", "Sergio", "Svetlana", "Patrick")

        // remove
        names -= "Patrick"
        names -= listOf("Oleg", "John")

        // membership
        assertTrue("Jessica" in names)
        assertTrue("Patrick" !in names)

        // element access
        assertEquals("Sabina", names[0])
    }

    @Test
    fun listInitializer() {
        val squares = List(5) { i ->
            (i + 1) * (i + 1)
        }

        assertEquals(listOf(1, 4, 9, 16, 25), squares)
    }

    @Test
    fun listRandomElement() {
        val nums = listOf(3, 7, 9)
        val randomElement = nums.random()

        assertTrue(randomElement % 2 == 1)
    }

    @Test
    fun maps() {
        val prices = mutableMapOf(
            "coffee" to 4300,
            "tea" to 2800,
        )

        // add to map
        prices += "sugar" to 400

        assertEquals(400, prices["sugar"])
    }

    @Test
    fun mapCreateWithAssociate() {
        val prices = "coffee:4300,tea:2800,sugar:400".split(",")
            .map { entry -> entry.split(":") }
            .associate { (item, price) ->        // destructuring in lambda
                item to price.toInt()
            }

        assertEquals(2800, prices["tea"])
    }

    @Test
    fun mapCreateWithZip() {
        val item = listOf("coffee", "tea", "sugar")
        val price = listOf(4300, 2800, 400)

        val prices = item.zip(price).toMap()

        assertEquals(2800, prices["tea"])
    }

    @Test
    fun lazyCollections() {
        // Sequence is a lazy collection in Kotlin
        val seq = generateSequence(4L) { it * it }
            .filter { it > 100 }
            .take(3)
            .toList()  // convert lazy collection to normal

        assertEquals(listOf(256L, 65536L, 4294967296L), seq)
    }
}

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.io.File
import java.lang.Exception
import java.time.LocalDate

class BasicSyntaxTest {

    @Test
    fun stringInterpolation() {
        val a = 2
        val b = 3
        val result = "$a + $b = ${a+b}"

        assertEquals("2 + 3 = 5", result)
    }

    @Test
    fun ifExpression() {
        val res = if (currentYear() % 1000 == 0) {
            "millennium"
        } else {
            "normal year"
        }

        assertEquals("normal year", res)
    }

    @Test
    fun whenExpression() {
        // can init variable in 'when' scope
        val msg = when (val code = (currentYear() / 100) * 20 + 1) {
            201 -> "Created"
            in 200..299 -> "OK"
            in 300..399 -> "Redirect"
            401, 403 -> "No Access"
            in 400..499 -> "Client Error"
            in 500..599 -> "Server Error"
            else -> "Unknown: code $code"
        }

        assertEquals("No Access", msg)

        // can use 'when' without a variable, then it simply replaces if
        val input = "email: test@test.com"
        val type = when {
            input.contains("<>|$") -> "Incorrect input"
            input.startsWith("tel:") -> "Telephone"
            input.startsWith("email:") -> "Email"
            else -> "Unknown"
        }

        assertEquals("Email", type)
    }

    @Test
    fun tryExpression() {
        val res = try {
            File("demand.txt").readText()
        } catch(e : Exception) {
            "file not found"
        }

        assertEquals("file not found", res)
    }

    @Test
    fun valueDestructuring() {
        val row = "1000,mike,full_access"
        // can ignore some values with _
        val (_, username, accessType) = row.split(",")

        assertEquals("mike", username)
        assertEquals("full_access", accessType)
    }

    @Test
    fun forInLoop() {
        val items = listOf(1, 3, 5)
        var sum = 0

        for (item in items) {
            sum += item
        }

        assertEquals(9, sum)
    }

    @Test
    fun ranges() {
        val x = currentYear() / 1000

        assertTrue(x in 1..5)
        assertTrue(x !in 3..8)

        // every 2nd element
        for (i in 1..5 step 2) {
            assertTrue(i % 2 == 1)
        }

        // not including the last element
        val range1 = 1 until 5   // since 1.9 can use ..<
        // reverse order
        val range2 = 4 downTo 1

        assertEquals(range1 step 1, range2.reversed())
    }

    private fun currentYear() : Int = LocalDate.now().year
}

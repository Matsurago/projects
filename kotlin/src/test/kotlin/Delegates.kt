import java.io.File
import kotlin.reflect.KProperty
import kotlin.test.Test
import kotlin.test.assertEquals

// property delegate
class Person : Named {
    override var name: String by FileStorageDelegate()
    var dept: String by FileStorageDelegate()
}

class FileStorageDelegate {
    operator fun getValue(instance: Any, property: KProperty<*>): String {
        return getStoredValue(property.name)
    }

    operator fun setValue(instance: Any, property: KProperty<*>, value: String) {
        storeValue(property.name, value)
    }

    private fun storeValue(name: String, value: String) {
        var found = false
        val entries = readEntries()
            .map { (k, v) ->
                if (k == name) {
                    found = true
                    k to value
                } else {
                    k to v
                }
            }.toMutableList()

        if (!found) {
            entries += name to value
        }

        writeEntries(entries)
    }

    private fun getStoredValue(name: String): String {
        return readEntries()
            .firstOrNull { (k, _) -> k == name }
            ?.second ?: ""
    }

    private fun readEntries() : List<Pair<String, String>> {
        return readDB().split(",")
            .filter { it.isNotEmpty() }
            .map { it.split("=") }
            .filter { it.size == 2 }
            .map { (k, v) -> k to v }
    }

    private fun writeEntries(entries: List<Pair<String, String>>) {
        val data = entries.joinToString(",") { (k, v) -> "$k=$v" }
        writeDB(data)
    }

    private fun readDB(): String {
        return File(DB_FILE)
            .takeIf { it.exists() }
            ?.readText() ?: ""
    }

    private fun writeDB(data: String) {
        File(DB_FILE).writeText(data)
    }

    companion object {
        const val DB_FILE = "data.txt"
    }
}

// interface implementation delegate
interface Named {
    val name: String
}

data class PersonHolder(val person: Person) : Named by person


class DelegateTest {
    @Test
    fun testDelegatedProperty() {
        val person = Person()

        person.name = "Vanya"
        assertEquals("Vanya", person.name)

        person.dept = "SRE"
        person.name = "Svetlana"
        assertEquals("Svetlana", person.name)
        assertEquals("SRE", person.dept)
    }

    @Test
    fun testDelegatedInterfaceImplementation() {
        val person = Person()
        person.name = "Stepan"

        val holder = PersonHolder(person)

        assertEquals("Stepan", holder.name)
    }
}

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertTrue

// 1) top-level function
// 2) if a function has only one expression, curly braces and return can be omitted
// 3) can specify default arguments
fun add(a: Int, b: Int, c: Int = 0): Int = a + b + c

// Nothing return type: function never returns
fun notYet() : Nothing {
    throw Exception("Not implemented")
}

// 1) Function that accepts another function
// 2) inline keyword: paste the function in all places where it's used
//    removes overhead of instantiating a lambda
inline fun <T> find(xs: Iterable<T>, predicate: (T) -> Boolean) : T? {
    for (x in xs) {
        if (predicate(x)) {
            return x
        }
    }
    return null
}

// a vararg function
fun sum(vararg xs: Int) : Int {
    var res = 0
    for (x in xs) {   // can iterate over vararg params
        res += x
    }
    return res
}

// extension functions: add functionality to existing types
fun Int.increment(): Int = this + 1

// generic extension
fun <T> T.hashLastDigit() = hashCode() % 10

// extension properties
val String.numVowels
    get() = this.count { it.lowercase() in "aeiou" }

// infix functions: can be defined as an extension function or in a class
// usage: 4 sub 3
infix fun Int.sub(other: Int) = this - other

// reified function - must be inlined
internal inline fun <reified T> List<*>.filterByType(): List<T> {
    val result = mutableListOf<T>()
    for (item in this) {
        if (item is T) {
            result.add(item)
        }
    }
    return result
}


class FunctionsTest {

    @Test
    fun testAdd() {
        // can call with named arguments
        val x = add(a = 2, b = 3)

        assertEquals(5, x)
    }

    @Test
    fun testReturnNothing() {
        assertThrows<Exception> { notYet() }
    }

    @Test
    fun lambdaCall() {
        val xs = listOf(1, 1, 2, 3, 5, 8, 11)

        // () can be omitted when lambda is last parameter of function
        val res = find(xs) {
            it > 3 && it % 2 == 0
        }

        assertEquals(8, res)
    }

    @Test
    fun varargCall() {
        // sum is a vararg function
        val a = sum(3, 4, 8)
        assertEquals(15, a)

        // use spread operator to call a vararg function on an array
        // other type of collection must be converted to array first
        val values = listOf(1, 2, 3)
        val b = sum(*values.toIntArray())
        assertEquals(6, b)
    }

    @Test
    fun testExtensions() {
        assertEquals(3, 2.increment())
        assertTrue("something".hashLastDigit() < 10)
        assertEquals(3, "something".numVowels)

        assertEquals(5, 7 sub 2)
    }

    @Test
    fun testReified() {
        val objects = listOf("test", 4, true, 2, "abcd", 3.14f)
        val numbers = objects.filterByType<Int>()

        assertEquals(listOf(4, 2), numbers)
    }

}

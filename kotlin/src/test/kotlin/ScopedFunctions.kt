import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.File

class ScopedFunctionsTest {

    @Test
    fun testApply() {
        // everything within lambda is applied to the 'file' variable, which is returned.
        // useful in variable init to avoid var
        val file = File(".").apply {
            setReadable(true)    // file.setReadable(true)
            setExecutable(true)
            // returns file - even if last expression has a different type
        }

        assertTrue(file.canRead())
        assertTrue(file.canExecute())
    }

    @Test
    fun testWith() {
        // similar to apply() that method calls are performed on the scoped variable
        // unlike apply() it returns value of the last expression in lambda
        val name = "John Wallas"
        val isShortSurname = with(name) {
            substring(5).length <= 8  // Boolean
        }

        assertTrue(isShortSurname)
    }

    @Test
    fun testLet() {
        // allows performing additional manipulations on an expression,
        // which is passed as 'it' to lambda
        val x = listOf(3, 2, 1).first().let {
            it * it
            // returns last expression in this lambda
        }

        assertEquals(9, x)

        // can also be used to do something on a nullable variable if it is not null
        val s: String? = "test"
        val result = s?.let {
            "$it success"
        }

        assertEquals("test success", result)
    }

    @Test
    fun testRun() {
        // allows replacing method nesting with chaining
        // cf.: result = Math.abs(Integer.valueOf("-1234"))
        val result = "-1234"
            .run(Integer::valueOf)
            .run(Math::abs)
            .run {
                // like apply(), all methods are called on this instance
                // like let(), returns value of the last expression
                // unlike let(), doesn't pass instance as 'it' to lambda
                toShort()
            }

        assertEquals(1234, result)
    }

    @Test
    fun testAlso() {
        // used to do a side effect in a method chain
        // like let(), passes the instance as 'it' to lambda
        // like apply(), returns the instance itself (not the last expression)
        var originalLength = 0
        val result = "tel:000-0000-000"
            .also { originalLength = it.length }
            .run { substring(4) }

        assertEquals(16, originalLength)
        assertEquals("000-0000-000", result)
    }

    @Test
    fun testTakeIf() {
        // returns a value if condition is satisfied
        // otherwise returns null
        val result = "some string"   // Int?
            .run { length }
            .takeIf { it > 20 }

        assertNull(result)
    }

    @Test
    fun testRepeat() {
        val nums = mutableListOf<Int>()

        repeat(3) {
            nums += 1
        }
        
        assertEquals(3, nums.size)
    }
}
